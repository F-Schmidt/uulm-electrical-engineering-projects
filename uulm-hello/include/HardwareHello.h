//
// Created by Falko Schmidt on 05.09.22.
//

#ifndef UULM_SGUBERRY_PIO_TEMPLATE_HARDWAREHELLO_H
#define UULM_SGUBERRY_PIO_TEMPLATE_HARDWAREHELLO_H


#include "AbstractHello.h"

/**
 * This class implements 'Hello World' using the
 * hardware-level approach. This means, that an
 * LED will be turned on and off.
 *
 * @author Falko Schmidt
 * @date 05. September 2022
 */
class HardwareHello : public AbstractHello {

public:

    /**
     * Initializes the LED-pin to be an output with
     * an initial value of LOW.
     * @return always true, because there are no detectable errors.
     */
    bool begin() override;

    /**
     * Turns the LED on and off in a specific cycle.
     * This function will be implemented using an
     * endless loop.
     */
    [[noreturn]] void sayHello() override;
};


#endif //UULM_SGUBERRY_PIO_TEMPLATE_HARDWAREHELLO_H
