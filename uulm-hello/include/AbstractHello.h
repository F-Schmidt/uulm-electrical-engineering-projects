//
// Created by Falko Schmidt on 05.09.22.
//

#ifndef UULM_SGUBERRY_PIO_TEMPLATE_ABSTRACTHELLO_H
#define UULM_SGUBERRY_PIO_TEMPLATE_ABSTRACTHELLO_H

/**
 * This is the abstract base class for the two implementations of "Hello World".
 * A base class names functions - in this case we know, that all classes that
 * inherit AbstractHello implement begin() and sayHello().
 *
 * @author Falko Schmidt
 * @date 05. September 2022
 */
class AbstractHello {

public:
    /**
     * Start all interfaces and/or set all pins
     * that you need in your sayHello()-method.
     * @return true, if everything was inited correctly, false otherwise.
     */
    virtual bool begin()    = 0;

    /**
     * Say hello using your own implementation. Further information can
     * be found in the subclasses 'HardwareHello' and 'SoftwareHello'.
     */
    virtual void sayHello() = 0;

};


#endif //UULM_SGUBERRY_PIO_TEMPLATE_ABSTRACTHELLO_H
