// ******************************************************************************************
// !!!   D O   N O T   C H A N G E   T H I S   F I L E   !!!
// ******************************************************************************************
// Created for Seminar NWT - Project SGUberry
// Falko Schmidt | Ulm University | 2022
// ******************************************************************************************
#include <Arduino.h>
#include "HardwareHello.h"
#include "AbstractHello.h"
#include "SoftwareHello.h"

// ------------------------------------------------------------------------------------------
// setup():
// This function will only run ONCE after power on of the microcontroller.
// ------------------------------------------------------------------------------------------
void setup() {
    // First, we will create our instances of
    // SoftwareHello and HardwareHello.
    AbstractHello *softwareHello = new SoftwareHello();
    AbstractHello *hardwareHello = new HardwareHello();

    // Now we will start the software hello to get our
    // message via serial interface.
    if(softwareHello->begin()) {
        softwareHello->sayHello();
    }

    // Finally, we will run HardwareHello to blink
    // an LED forever.
    if(hardwareHello->begin()) {
        hardwareHello->sayHello();
    }
}


// ------------------------------------------------------------------------------------------
// loop():
// After execution of the setup() function, the loop-function will run forever.
// ------------------------------------------------------------------------------------------
void loop() {
  // For this program we do not need this loop.
  // We will activate and run everything in the
  // setup function.
}
