# uulm-arduino-isp

This repository implements a simple In-Circuit-Serial-Programmer (ICSP) that can be used to flash ATtiny25/45/85 microcontrollers.

Developed by Falko Schmidt, University of Ulm

## Needed material
Make sure to gather these components before starting the project!

| Quantity | Name / Number of Component | Description |
|---|---|---|
| 1 | 150080RS75000 | red LED, 0805 from Kingbright  |
| 1 | 150080YS75000 | yellow LED, 0805 from Kingbright  |
| 1 | 150080VS75000 | green LED, 0805 from Kingbright |
| 1 | - | ZIF-Socket for DIP-14 ICs. |
| 1 | K103K15X7RF5TL2 | 10 uF MLCC. Basically any one with form factor 0805 will work here. You only need this when using the programmer shield with an Arduino MEGA.|

You also need 3 resistors for current regulation of the LEDs. I used 510 ohms for the yellow and green LED and 820 ohms for the red LED, which works fine.
Make sure you also have everything needed to solder the components and filament for 3D-printing.

If you print the case (read below for details), then make sure to buy pin headers where the long side pin is at lease 12mm long. I used these and they work perfect: https://www.amazon.de/gp/product/B083S1RP25/

## Instructions -- How to build it

1. Start ordering the PCB, because this will take the longest time. If you want to directly order the version as seen below, you can simply navigate to the [manufacturing file directory](./pcb-design/manufacturing), download the ZIP file and upload it to a pcb manufacturer, for example [JLCPCB](https://jlcpcb.com).
If you want to edit the pcb, then you can find the KiCAD-Files (KiCAD version 6 and later) in [`pcb-design`](./pcb-design).

2. Print the case for Arduino UNO. The files are located in the [mechanical directory](./mechanical). Originally they are from https://www.thingiverse.com/thing:628929 but I changed the dimensions of the button to work with my 3D-printer.

3. Solder the PCB.

4. Flash the programmer. The software can either be found as an example in the Arduino IDE installation: `11.ArduinoISP -> ArduinoISP`. Flash it to your chosen programmer board (UNO / MEGA).

5. Flash ATtiny25/45/85 devices. Install a core that fits your needs. Add the URL to the "Additional Boards Manager URLs" in "Preferences". I chose this one: http://drazzy.com/package_drazzy.com_index.json which works fine for me, but it lacks some implementations, so there might be better cores out there!
To upload you must choose the Programmer `Arduino as ISP` (NOT ArduinoISP)! If you use an Arduino Leonardo, then you must choose `Arduino as ISP (ATmega32U4)`.
After that upload by navigating to `Sketch -> Upload using programmer`.

6. You should see the LEDs flash for a short period of time. Afther that your microcontroller should work like a charm. If you want to be safe, that the microcontroller was flashed correctly, then you can also read the content of the ic after writing to it. This can be set in "Preferences" of the Arduino IDE.


## Images

![](./pcb-design/Programmer_ATtiny_Top.png "Top side of ATtiny25/45/85 Programmer PCB") 
![](./pcb-design/Programmer_ATtiny_Bottom.png "Bottom side of ATtiny25/45/85 Programmer PCB")

## ... and the final product:

![](./ATtiny_Programmer_Final.jpg "Final ATtiny programmer on an Arduino UNO from Sparkfun") 



