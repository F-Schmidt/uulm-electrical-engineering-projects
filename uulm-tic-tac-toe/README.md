# uulm-tic-tac-toe

![Tic Tac Toe (Top)](./pcb-design/Tic_Tac_Toe_Top.png)

This repository presents the implementation of a Tic Tac Toe game utilizing red-green LEDs. The game allows two players to engage in a match, employing small jumpers to activate the LEDs, adding an interactive and visually engaging dimension to the classic game experience.

Falko Schmidt, Ulm University
