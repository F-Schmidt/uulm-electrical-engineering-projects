# air-quality-pcb

![Air Quality PCB](./pcb-design/UULM_Weatherstation_Top.png)

This repository contains a simple Arduino HAT with sensors for air temperature and humidity.

Falko Schmidt, Ulm University