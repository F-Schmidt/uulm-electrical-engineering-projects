#include "RandomGenerator.h"

#define         LED_RED_PIN         (PA1)
#define         LED_BLUE_PIN        (PA5)
#define         LED_GREEN_PIN       (PB0)
#define         LED_YELLOW_PIN      (PA6)

#define         BUTTON_RED_PIN      (PA2)
#define         BUTTON_BLUE_PIN     (PA4)
#define         BUTTON_GREEN_PIN    (PB1)
#define         BUTTON_YELLOW_PIN   (PA7)

#define         BUZZER_PIN          (PA3)

#define         SIMON_SAYS_ROUNDS   (20)


enum Color {
  RED     = 0,
  BLUE    = 1,
  GREEN   = 2,
  YELLOW  = 3
};

const uint8_t LED_PINS[4]    = {LED_RED_PIN,     LED_BLUE_PIN,     LED_GREEN_PIN,    LED_YELLOW_PIN};
const uint8_t BUTTON_PINS[4] = {BUTTON_RED_PIN,  BUTTON_BLUE_PIN,  BUTTON_GREEN_PIN, BUTTON_YELLOW_PIN};
const unsigned long TONE_FREQUENCIES[4] = {1500, 2500, 4000, 8000};

uint8_t simon_says_fields[SIMON_SAYS_ROUNDS] = {0};

volatile uint8_t currentIndex = 0;
volatile uint8_t player_fields[SIMON_SAYS_ROUNDS] = {0};

RandomGenerator randomGenerator(42, 0, 3);


void setPlayerField(enum Color color) {
  player_fields[currentIndex++] = color;
}

inline void onButtonRedPressed() {
  setPlayerField(Color::RED);
}

inline void onButtonBluePressed() {
  setPlayerField(Color::BLUE);
}

inline void onButtonGreenPressed() {
  setPlayerField(Color::GREEN);
}

inline void onButtonYellowPressed() {
  setPlayerField(Color::YELLOW);
}

void setup() {
  // Set up LED pins and deactivate all LEDs
  for(uint8_t index = 0; index < 4; index++) {
    pinMode(LED_PINS[index], OUTPUT);
    digitalWrite(LED_PINS[index], LOW);
  }

  // Set button pins and enable pullup resistors
  for(uint8_t index = 0; index < 4; index++) {
    pinMode(BUTTON_PINS[index], INPUT_PULLUP);
  }

  // Set up buzzer pin to be OUTPUT
  pinMode(BUZZER_PIN, OUTPUT);

  // attach interrupts to buttons
  attachInterrupt(BUTTON_RED_PIN,     onButtonRedPressed,     FALLING);
  attachInterrupt(BUTTON_BLUE_PIN,    onButtonBluePressed,    FALLING);
  attachInterrupt(BUTTON_GREEN_PIN,   onButtonGreenPressed,   FALLING);
  attachInterrupt(BUTTON_YELLOW_PIN,  onButtonYellowPressed,  FALLING);

  // Create Simon Says sequence
  for(uint8_t index = 0; index < SIMON_SAYS_ROUNDS; index++) {
    simon_says_fields[index] = index % 4;//randomGenerator.getRandom();
  }
}

void loop() {
  // Show sequence
  for(uint8_t round = 0; round <= SIMON_SAYS_ROUNDS/*currentIndex*/; round++) {
    tone(BUZZER_PIN, TONE_FREQUENCIES[simon_says_fields[round]]);
    digitalWrite(LED_PINS[simon_says_fields[round]], HIGH);
    delay(100);
    noTone(BUZZER_PIN);
    digitalWrite(LED_PINS[simon_says_fields[round]], LOW);
    delay(100);
  }
}
