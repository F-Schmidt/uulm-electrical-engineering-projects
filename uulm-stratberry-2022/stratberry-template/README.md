# uulm-sguberry-kicad-template
This GitLab-repository contains a template that must be used to create modules for use with the SGUberry datalogger, that will be sent into the stratosphere with a maximum altitude of approximately 33.000 meters.

# General information
- This template includes the outside edges of the PCB as well as the exact locations for the connectors on the top and bottom of the PCB.
- Components may only be placed within the red square, which is located in the 'User.9' layer
- Please change the text 'Team member 1 & 2 names' to either your name or the name of your group (whichever you prefer). Please don't touch another line of text.
- The measurement lines are in the 'User.2' layer.
- You can hide the 'F.Fab', 'B.Fab' and 'User.1' layers to hide unnecessary information during PCB development.


# 3D-Rendering
The image below shows a rendering of the initial template version. This will of course change if you add the sensors you need and additional details.
![Template View in KiCAD](SGUberry_Module_Template_PCB.png)

# Credits
All files in this repository were created by Falko Schmidt, University of Ulm, 2022.
