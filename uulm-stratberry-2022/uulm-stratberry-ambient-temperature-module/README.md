# uulm-stratberry-ambient-temperature-module

This repository implements a PCB that can measure the temperature in the stratosphere using up to 4 epoxy thermistors.

Developed by Falko Schmidt, University of Ulm
