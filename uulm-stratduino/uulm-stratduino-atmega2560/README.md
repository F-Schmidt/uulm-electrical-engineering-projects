# uulm-stratduino-atmega2560

This repository includes all files that belong to the Statduino project of the University of Ulm, 2022.
Developed by Falko Schmidt

# What is a weather ballon?
A weather ballon consists of three main components: Up top there is a huge ballon filled with a gas which is lighter than air. We use helium.
About 10 meters below there is a parachute. This is needed to safely bring the probe back to earth.
The probe is located about 5 meters below the parachute and has a weight of approximately 2kg.

The main goal of a weather ballon is - who guessed it? ;) - to record data about the weather, like temperature, pressure, gas concentrations etc.

# Why is this project so amazing?
We use this project to build some electrical components with students. They can choose between some different topics. Either they build a pcb that can measure temperature and humidity or a PCB that will be used to store all data on SD-cards.
Each student creats an own PCB containing an own circuit. All these PCBs are stacked together and will be sent up to a height of approximately 35-40km.
In this amazing height we can achieve pictures like this:

 TODO INSERT IMAGE

 # What is this repository about?
 The main PCB is done by is. This is to prevent any mechanical and electrical issues - we plan the connectors used in the stack and tell each group which pins to use. This PCB is based around the ATMega2560 and can therefore be easily programmed using the Arduino IDE for the board 'Arduino MEGA'. The only difference is that we do not provide the USB<->UART converter on the PCB to minimize the power needed. Also the form factor is more convenient.
