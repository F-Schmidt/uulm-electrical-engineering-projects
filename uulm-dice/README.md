# uulm-attiny-dice

![AtTiny Dice](./pcb-design/AtTiny_Dice_Top.png)

This repository implements a simple pseudo-random dice using an AtTiny25/45/85.

Falko Schmidt, Ulm University
