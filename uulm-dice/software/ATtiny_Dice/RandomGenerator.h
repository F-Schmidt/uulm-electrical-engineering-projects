//
// Created by Falko Schmidt on 11.02.22.
//

#ifndef ARDUINORANDOMCUBE_RANDOMGENERATOR_H
#define ARDUINORANDOMCUBE_RANDOMGENERATOR_H


#include <stdint.h>

class RandomGenerator {

private:
    uint32_t min;
    uint32_t increment;
    uint32_t factor;
    uint32_t modulo;
    uint32_t seed;

public:
    explicit RandomGenerator(uint32_t seed, uint32_t min, uint32_t max);
    uint8_t getRandom();

};


#endif //ARDUINORANDOMCUBE_RANDOMGENERATOR_H
