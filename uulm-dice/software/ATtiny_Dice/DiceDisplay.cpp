//
// Created by Falko Schmidt on 11.02.22.
//

#include <Arduino.h>
#include "DiceDisplay.h"

#define         LED_ON          (leds_active_low ? LOW : HIGH)
#define         LED_OFF         (leds_active_low ? HIGH : LOW)

DiceDisplay::DiceDisplay(const uint8_t pin_tl_br, const uint8_t pin_tr_bl, const uint8_t pin_mid_row,
                         const uint8_t pin_mid_point, const bool leds_active_low) {
    this->pin_tl_br = pin_tl_br;
    this->pin_tr_bl = pin_tr_bl;
    this->pin_mid_row = pin_mid_row;
    this->pin_mid_point = pin_mid_point;
    this->leds_active_low = leds_active_low;
    pinMode(this->pin_tl_br, OUTPUT);
    pinMode(this->pin_tr_bl, OUTPUT);
    pinMode(this->pin_mid_row,OUTPUT);
    pinMode(this->pin_mid_point,OUTPUT);
}


void DiceDisplay::display(const uint8_t val) const {
    switch (val) {
        case 1:
            /**
             * Dice-Display for one:
             *
             *         -------------
             *        |             |
             *        |      o      |
             *        |             |
             *         -------------
             */
            digitalWrite(pin_tl_br,     LED_OFF);
            digitalWrite(pin_tr_bl,     LED_OFF);
            digitalWrite(pin_mid_row,   LED_OFF);
            digitalWrite(pin_mid_point, LED_ON);
            break;
        case 2:
            /**
             * Dice-Display for two:
             *
             *         -------------
             *        |          o  |
             *        |             |
             *        |  o          |
             *         -------------
             */
            digitalWrite(pin_tl_br,     LED_OFF);
            digitalWrite(pin_tr_bl,     LED_ON);
            digitalWrite(pin_mid_row,   LED_OFF);
            digitalWrite(pin_mid_point, LED_OFF);
            break;
        case 3:
            /**
             * Dice-Display for three:
             *
             *         -------------
             *        |          o  |
             *        |      o      |
             *        |  o          |
             *         -------------
             */
            digitalWrite(pin_tl_br,     LED_OFF);
            digitalWrite(pin_tr_bl,     LED_ON);
            digitalWrite(pin_mid_row,   LED_OFF);
            digitalWrite(pin_mid_point, LED_ON);
            break;
        case 4:
            /**
             * Dice-Display for four:
             *
             *         -------------
             *        |  o       o  |
             *        |             |
             *        |  o       o  |
             *         -------------
             */
            digitalWrite(pin_tl_br,     LED_ON);
            digitalWrite(pin_tr_bl,     LED_ON);
            digitalWrite(pin_mid_row,   LED_OFF);
            digitalWrite(pin_mid_point, LED_OFF);
            break;
        case 5:
            /**
             * Dice-Display for five:
             *
             *         -------------
             *        |  o       o  |
             *        |      o      |
             *        |  o       o  |
             *         -------------
             */
            digitalWrite(pin_tl_br,     LED_ON);
            digitalWrite(pin_tr_bl,     LED_ON);
            digitalWrite(pin_mid_row,   LED_OFF);
            digitalWrite(pin_mid_point, LED_ON);
            break;
        default:
            /**
             * Dice-Display for six:
             *
             *         -------------
             *        |  o       o  |
             *        |  o       o  |
             *        |  o       o  |
             *         -------------
             */
            digitalWrite(pin_tl_br,     LED_ON);
            digitalWrite(pin_tr_bl,     LED_ON);
            digitalWrite(pin_mid_row,   LED_ON);
            digitalWrite(pin_mid_point, LED_OFF);
            break;
    }
}

void DiceDisplay::clear() {
    digitalWrite(this->pin_tl_br,     LED_OFF);
    digitalWrite(this->pin_tr_bl,     LED_OFF);
    digitalWrite(this->pin_mid_row,   LED_OFF);
    digitalWrite(this->pin_mid_point, LED_OFF);
}
