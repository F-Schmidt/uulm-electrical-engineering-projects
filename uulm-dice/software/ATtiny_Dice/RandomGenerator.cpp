//
// Created by Falko Schmidt on 11.02.22.
//

#include "RandomGenerator.h"


RandomGenerator::RandomGenerator(const uint32_t seed, const uint32_t min, const uint32_t max) {
    this->seed      = seed;
    this->min       = min;
    this->modulo    = max;
    this->factor    = 1103515245;   // BSD formula
    this->increment = 12345;        // BSD formula
}


uint8_t RandomGenerator::getRandom() {
    this->seed = (this->factor * this->seed + this->increment);
    return ((this->seed % this->modulo) + this->min);
}