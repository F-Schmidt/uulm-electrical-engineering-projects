//
// Created by Falko Schmidt on 11.02.22.
//

#ifndef ARDUINORANDOMCUBE_DICEDISPLAY_H
#define ARDUINORANDOMCUBE_DICEDISPLAY_H


#include <stdint.h>

class DiceDisplay {

private:
    uint8_t pin_tl_br;
    uint8_t pin_tr_bl;
    uint8_t pin_mid_row;
    uint8_t pin_mid_point;
    bool leds_active_low;

public:
    DiceDisplay(uint8_t pinTlBr, uint8_t pinTrBl, uint8_t pinMidRow, uint8_t pinMidPoint, bool leds_active_low);
    void display(uint8_t val) const;
    void clear();
};


#endif //ARDUINORANDOMCUBE_DICEDISPLAY_H
