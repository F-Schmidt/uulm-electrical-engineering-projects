// ==============================================================================================================
// Pseudo-Random Dice for ATtiny25/45/85 microcontroller
// Created by Falko Schmidt, University of Ulm, GERMANY
// ==============================================================================================================
#include <Arduino.h>            // General header for Arduino devices / Atmel microcontrollers
#include "RandomGenerator.h"    // Linear congruential generator implementation used in this example
#include "DiceDisplay.h"        // The display controller for a dice display using 7 LEDs
#include "EEPROMHandler.h"      // This class handles store and read operations for the eeprom

// ==============================================================================================================
// Pin definitions for dice display
// PIN_LED_TL_BR     = The pin, which top left (TL) and bottom right (BR) LEDs are connected to
// PIN_LED_TR_BL     = The pin, which top right (TR) and bottom left (BL) LEDs are connected to
// PIN_LED_MID_ROW   = The pin, which the LEDs left and right of the mid row are connected to
// PIN_LED_MID_POINT = The pin, which the single middle LED is connected to
// ==============================================================================================================
#define         PIN_LED_TL_BR             (1)
#define         PIN_LED_TR_BL             (2)
#define         PIN_LED_MID_ROW           (0)
#define         PIN_LED_MID_POINT         (4)

// ==============================================================================================================
// Definitions for the button that will be pressed to roll the dice
// PIN_BUTTON_INPUT  = The pin which the button is connected to
// BUTTON_PRESSED    = The state which the button has, when pressed. (Either HIGH or LOW)
// ==============================================================================================================
#define         BUTTON_PRESSED            (LOW)
#define         PIN_BUTTON_INPUT          (3)

// ==============================================================================================================
// Definitions for delays
// DELAY_CYCLE       = The delay time between two executions of the loop-function
// ROLL_DISPLAY_TIME = The time that the rolled value is displayed
// ==============================================================================================================
#define         DELAY_CYCLE               (50)
#define         ROLL_DISPLAY_TIME         (4000)

// ==============================================================================================================
// Initialization of all objects needed.
// eepromHandler    -> instance of class handling load and save operations on EEPROM
// randomGenerator  -> the linear congruential generator used for pseudo-random values
// diceDisplay      -> instance of class controlling dice display LEDs
//
// NOTE: EEPROMHandler must be initialized before randomGenerator, because the randomGenerator uses a 
//       seed read from EEPROM
// ==============================================================================================================
EEPROMHandler eepromHandler;
RandomGenerator randomGenerator(eepromHandler.load(0), 1, 6);
DiceDisplay diceDisplay(PIN_LED_TL_BR, PIN_LED_TR_BL, PIN_LED_MID_ROW, PIN_LED_MID_POINT, false);

// ==============================================================================================================
// Variables needed for program execution
// ==============================================================================================================
uint8_t shuffleEffectValue = 1;   // The value shown when button is pressed. Start value is 1
uint8_t delayCounter = 0;         // Delay counter which is used when a number was rolled
bool buttonWasPressed = false;    // boolean indicating if button was pressed before. This indicates a roll
bool isFirstRoll = true;          // boolean indicating if the current roll is the first. Used for new seed value

// ==============================================================================================================
// Small inline function for better readability. This function checks if the button is currently pressed
// ==============================================================================================================
inline bool isButtonPressed() {
  return digitalRead(PIN_BUTTON_INPUT) == BUTTON_PRESSED;
}

// ==============================================================================================================
// This function will be executed once at the beginning of the program execution.
// In this case only the pin, where the external push button is connected to, hast to be defined to be an
// interrupt with internal pullup.
// ==============================================================================================================
void setup() {
  // Define button pin to be an input with internal pullup
  pinMode(PIN_BUTTON_INPUT, INPUT_PULLUP);
}

// ==============================================================================================================
// This function will be repeated (forever) after execution of the setup function.
// The status of the button is checked. If pressed, a shuffle effect will be displayed. If released the
// rolled value will be displayed for a certain time.
// ==============================================================================================================
void loop() {
  if(isButtonPressed()) {
    // ----------------------------------------------------------------------------------------------------------
    // Button PRESSED -> shuffle effect displayed
    // ----------------------------------------------------------------------------------------------------------
    
    // Reset values
    delayCounter = 0;
    buttonWasPressed = true;

    // Check shuffle display value
    if(shuffleEffectValue == 6) {
      shuffleEffectValue = 1;
    }

    // Shuffle effect. We will display the numbers 1...6
    diceDisplay.display(shuffleEffectValue++);
    
  } else {
    // ----------------------------------------------------------------------------------------------------------
    // Button not pressed. We need to work on several cases here.
    // If the button was already pressed, then we need to roll a value and display it.
    // ----------------------------------------------------------------------------------------------------------

    // If it is the first roll, then we need to save a seed for the next startup
    if(isFirstRoll && buttonWasPressed) {
      isFirstRoll = false;
      eepromHandler.save((uint64_t)millis(), 0);
    }

    // Reset shuffle effect
    shuffleEffectValue = 1;

    if(delayCounter == (ROLL_DISPLAY_TIME / DELAY_CYCLE)) {
      delayCounter = 0;
      diceDisplay.clear();
    } else {
      if(buttonWasPressed) {
        // Show random number
        diceDisplay.display(randomGenerator.getRandom());
        buttonWasPressed = false;
      }
      delayCounter++;
    }
  }

  // Wait 100 milliseconds
  delay(DELAY_CYCLE);
}

