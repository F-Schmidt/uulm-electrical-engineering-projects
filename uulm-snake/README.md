# snake-pcb

![Snake PCB](./pcb-design/Snake.png)

An Arduino UNO/MEGA HAT that implements the typical Snake game.

Falko Schmidt, Ulm University