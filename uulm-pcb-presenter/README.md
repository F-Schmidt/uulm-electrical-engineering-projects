# PCB-Presenter

This repository implements a HAT for the Arduino Mega / Due which can be used to present Arduino Uno PCBs. This pcb contains three switches which can be used to dynamically load up to eight different programs and run them.

Falko Schmidt, Uni Ulm