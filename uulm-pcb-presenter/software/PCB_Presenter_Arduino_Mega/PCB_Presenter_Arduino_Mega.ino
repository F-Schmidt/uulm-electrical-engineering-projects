/*
Created for use with arduino mega or due.
Falko Schmidt, 2021
*/
#include "Arduino.h"
#include <Arduino_FreeRTOS.h>

#define         SW0         28   
#define         SW1         38   
#define         SW2         48

#define         POWER_LED   17
#define         ERROR_LED   20


uint8_t switchState;

TaskHandle_t BUTTON_CHECK_TASK;
TaskHandle_t PROGRAM_TASK;


// Declare function 'reset_arduino' to be at position zero in the program code.
// On calling this function, the program counter will be set to zero and the arduino 
// will restart.
void(* reset_arduino) (void) = 0;

// Define function, that will be called every 100ms to check, if the button 
// positions changed.
static void switch_change_check(void) {
  for(;;) {
    Serial.println("CHECK");
    if(switchState != read_switch_state()) {
      reset_arduino();
    }
    vTaskDelay(100/portTICK_PERIOD_MS);
  }
}


uint8_t read_switch_state(void) {
  uint8_t state = 0;
  state |= digitalRead(SW0);
  state |= digitalRead(SW1) << 1;
  state |= digitalRead(SW2) << 2;
  return state;
}


static void show_switch_error() {
  uint8_t count;
  while(1) {
    for(count = 0; count < 3; count++) {
      digitalWrite(ERROR_LED, LOW);
      vTaskDelay(100/portTICK_PERIOD_MS);
      digitalWrite(ERROR_LED, HIGH);
      vTaskDelay(100/portTICK_PERIOD_MS);
    }
      vTaskDelay(750/portTICK_PERIOD_MS);
    //delay(750);
  }
}



void setup(void) {
  Serial.begin(9600);
  Serial.println("RESETTET!");
  // ================================================================================
  // Initialize all switches to be inputs with activated internal pullup resistor.
  // ================================================================================
  pinMode(SW0, INPUT);
  pinMode(SW1, INPUT);
  pinMode(SW2, INPUT);
  
  switchState = read_switch_state();

  // ================================================================================
  // Connect all input switch pins to interrupt service routine
  // ================================================================================
  //attachInterrupt(digitalPinToInterrupt(SW0), switch_isr_on_change, CHANGE);
  //attachInterrupt(digitalPinToInterrupt(SW1), switch_isr_on_change, CHANGE);
  //attachInterrupt(digitalPinToInterrupt(SW2), switch_isr_on_change, CHANGE);

  // ================================================================================
  // Enable power LED
  // ================================================================================
  pinMode(POWER_LED, OUTPUT);
  digitalWrite(POWER_LED, LOW);

  // ================================================================================
  // Set error LED mode
  // ================================================================================
  pinMode(ERROR_LED, OUTPUT);
  digitalWrite(ERROR_LED, HIGH);

  // ================================================================================
  // Load program according to selected state
  // ================================================================================
  switch(read_switch_state()) {
    case 0x00: //TODO call program setup and void methods
    case 0x01:
    case 0x02:
    case 0x03:
    case 0x04:
    case 0x05:
    case 0x06:
    case 0x07:
    default: ;
  }
  /*
  xtaskCreate(function_name_of_task, task_name          ,stack, params, priority, taskhandle */
  xTaskCreate(show_switch_error, "Show error indication", 128, NULL, 1, &BUTTON_CHECK_TASK);
  xTaskCreate(switch_change_check, "Switch change check", 128, NULL, 2, &PROGRAM_TASK);
}

void loop(void) {
}
