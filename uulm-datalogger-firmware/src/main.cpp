#include "pins_arduino.h"
#include <Arduino.h>
#include "loggers/stratberry-proto/StratberryPrototypeDatalogger.h"
#include "loggers/mkr1310-proto/MKR1310PrototypeDatalogger.h"
#include "loggers/tempberry-proto/TempberryPrototypeDatalogger.h"
#include "loggers/sguberry-2022/SGUberry2022Datalogger.h"

// minicom -c on -D /dev/cu.usbmodem14301

/*
 * TODO
 *  delete sd dir and merge it to handlers with different 'tactics' of logging: sequential, new file, etc...
 *  add generic uart, i2c, spi, onewire device
 *  add generic sensor devices (temperature,...)
 *  rework debuggers vs handlers (make the difference clear!) and maybe add xml/json/csv support
 *  test openLog
 *  implement generic OneSensorModule which takes any sensor, samples it etc. This is practical for stemma
 *      -> maybe implement wrappers for the sensors using third libraries (for example Sensirion)
 *  add custom implementations for ALL sensors (one by one...)
 *  add support for nameable to add a reference to a function that returns a string. this allows access for
 *      fields like CS or similar
 *  documentation for handlers and modules needed for Seminar NWT!
 *  rename general dir to a better name...
 *  update all readme
 *  add images of all dataloggers and their names in a table for better documentation
 *  remove 'event modules' and rework structure!
 *  document abstractdatalogger and abstractmodule and check if they are abstract or generic
 *  rework platform.ini for generic RP2040 and MKR1310 etc board envs
 *  reduce dependencies as far as possible
 *
 */

void setup() {

#if defined(STRATBERRY_PROTOTYPE_RP2040)
    auto datalogger = StratberryPrototypeDatalogger();
#elif defined(MKR1310_LOGGER_PROTOTYPE)
    auto datalogger = MKR1310PrototypeDatalogger();
#elif defined(SGUBERRY_2022)
    auto datalogger = SGUberry2022Datalogger();
#elif defined(ARDUINO_UNO_MIKROBUS_PROTOTYPE)
#error NOT YET DEFINED!
#elif defined(ARDUINO_EDUCATION_MIKROBUS_PROTOTYPE)
#error NOT YET DEFINED!
#elif defined(TEMPBERRY_PROTOTYPE)
    auto datalogger = TempberryPrototypeDatalogger();
#elif defined(STMBERRY)
    pinMode(PA7, OUTPUT);
    while(1) {
        digitalWrite(PA7, HIGH);
        delay(1000);
        digitalWrite(PA7, LOW);
        delay(1000);
    }
#else
#error No datalogger type defined. Please define a type in file platform.ini!
#endif

    // Try to start the datalogger
    datalogger.begin();

    // Logging cycle
    datalogger.run();

}


void loop() {
    // No work to do here. The datalogger will be started in
    // the setup function and from there it will run forever.
}
