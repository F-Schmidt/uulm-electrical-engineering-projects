//
// Created by Falko Schmidt on 14.08.22.
//

#include "status-indicators/StratberryPrototypeDataLoggerStatusIndicator.h"

#define                 RUNNING_INIT_LED_PIN            (25u)
#define                 INIT_SUCCESS_LED_PIN            (24u)
#define                 INIT_FAILURE_LED_PIN            (23u)

#define                 SAMPLING_LED_PIN                (22u)
#define                 SAMPLING_FAILURE_LED_PIN        (21u)

#define                 WRITING_DATA_LED_PIN            (20u)
#define                 SD_CARD_FAILURE_LED_PIN         (19u)


StratberryPrototypeDataLoggerStatusIndicator::StratberryPrototypeDataLoggerStatusIndicator()
        : AbstractStatusIndicator("Stratberry-Prototype Datalogger Status Indicator",
                                  "PCB HAT for Stratberry Prototype Datalogger.") {
    this->initRunningLED = new LED(RUNNING_INIT_LED_PIN);
    this->initSuccessLED = new LED(INIT_SUCCESS_LED_PIN);
    this->initFailureLED = new LED(INIT_FAILURE_LED_PIN);
    this->samplingRunningLED = new LED(SAMPLING_LED_PIN);
    this->samplingFailureLED = new LED(SAMPLING_FAILURE_LED_PIN);
    this->writingDataLED = new LED(WRITING_DATA_LED_PIN);
    this->sdCardFailureLED = new LED(SD_CARD_FAILURE_LED_PIN);
}


void StratberryPrototypeDataLoggerStatusIndicator::onStart(const AbstractStatusIndicator::ProgramState state) {
    switch (state) {
        case AbstractStatusIndicator::INITIALIZATION:
            this->initRunningLED->on();
            return;
        case AbstractStatusIndicator::SAMPLING:
            this->samplingRunningLED->on();
            return;
        case AbstractStatusIndicator::WRITING_DATA:
            this->writingDataLED->on();
            return;
        default:
            return;
    }
}


void StratberryPrototypeDataLoggerStatusIndicator::onFinish(const AbstractStatusIndicator::ProgramState state) {
    switch (state) {
        case AbstractStatusIndicator::INITIALIZATION:
            this->initRunningLED->off();
            return;
        case AbstractStatusIndicator::SAMPLING:
            this->samplingRunningLED->off();
            return;
        case AbstractStatusIndicator::WRITING_DATA:
            this->writingDataLED->off();
            return;
        default:
            return;
    }
}


void StratberryPrototypeDataLoggerStatusIndicator::clear() {
    this->initRunningLED->off();
    this->initSuccessLED->off();
    this->initFailureLED->off();
    this->samplingRunningLED->off();
    this->samplingFailureLED->off();
    this->writingDataLED->off();
    this->sdCardFailureLED->off();
}


bool StratberryPrototypeDataLoggerStatusIndicator::begin() {
    this->initRunningLED->begin();
    this->initSuccessLED->begin();
    this->initFailureLED->begin();
    this->samplingRunningLED->begin();
    this->samplingFailureLED->begin();
    this->writingDataLED->begin();
    this->sdCardFailureLED->begin();
    return true;
}
