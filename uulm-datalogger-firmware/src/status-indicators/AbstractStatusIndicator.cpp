//
// Created by Falko Schmidt on 13.08.22.
//

#include "status-indicators/AbstractStatusIndicator.h"

AbstractStatusIndicator::AbstractStatusIndicator(const std::string &name, const std::string &description)
    : Nameable(name), Describable(description) {
    // Nothing to do here
}
