//
// Created by Falko Alrik Schmidt on 27.09.22.
//

#include "Arduino.h"
#include "status-indicators/SGUberryHelloWorldStatusIndicator.h"

SGUberryHelloWorldStatusIndicator::SGUberryHelloWorldStatusIndicator(uint8_t ledPin)
        : AbstractStatusIndicator("SGUberry Hello World Status Indicator",
"This status indicator activates an LED during sampling (Similar to the Hello World example).")  {
    this->ledPin = ledPin;
}

void SGUberryHelloWorldStatusIndicator::onStart(AbstractStatusIndicator::ProgramState state) {
    if(state == SAMPLING) {
        digitalWrite(this->ledPin, HIGH);
    }
}

void SGUberryHelloWorldStatusIndicator::onFinish(AbstractStatusIndicator::ProgramState state) {
    if(state == SAMPLING) {
        this->clear();
    }
}

void SGUberryHelloWorldStatusIndicator::clear() {
    digitalWrite(this->ledPin, LOW);
}

bool SGUberryHelloWorldStatusIndicator::begin() {
    pinMode(this->ledPin, OUTPUT);
    digitalWrite(this->ledPin, LOW);
    return true;
}
