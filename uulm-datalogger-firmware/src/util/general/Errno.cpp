//
// Created by Falko Alrik Schmidt on 05.11.22.
//

#include "util/general/Errno.h"

Errno::Errno() {
    this->errorNum = ERRNO_NO_ERROR;
}

void Errno::setErrno(uint32_t errorNum) {
    this->errorNum = errorNum;
}

uint32_t Errno::getErrno() const {
    return this->errorNum;
}

bool Errno::hasError() const {
    return this->errorNum != ERRNO_NO_ERROR;
}

void Errno::clearError() {
    this->errorNum = ERRNO_NO_ERROR;
}

void Errno::printErrorStr(Stream &stream) const {
    stream.println(this->getErrorStr().c_str());
}
