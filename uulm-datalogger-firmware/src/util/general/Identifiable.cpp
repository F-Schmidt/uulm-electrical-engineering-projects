//
// Created by Falko Schmidt on 02.09.22.
//

#include "util/general/Identifiable.h"

template<typename T>
Identifiable<T>::Identifiable(T id) {
    this->id = id;
}

template<typename T>
T Identifiable<T>::getId() const {
    return this->id;
}
