//
// Created by Falko Schmidt on 30.07.22.
//

#include "util/general/Describable.h"

#include <utility>

Describable::Describable(std::string description) : description(std::move(description)) {}

const std::string &Describable::getDescription() const {
    return description;
}

Describable::Describable(const std::function<std::string()>& f) {
    this->description = f();
}
