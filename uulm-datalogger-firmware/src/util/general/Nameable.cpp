//
// Created by Falko Schmidt on 30.07.22.
//
#include "util/general/Nameable.h"

#include <utility>

Nameable::Nameable(std::string name) : name(std::move(name)) {
    // Nothing to do here
}


const std::string &Nameable::getName() const {
    return this->name;
}
