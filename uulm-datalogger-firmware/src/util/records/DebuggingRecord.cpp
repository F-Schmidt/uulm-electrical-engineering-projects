//
// Created by Falko Schmidt on 01.09.22.
//

#include "util/records/DebuggingRecord.h"

/**
 * Default constructor.
 * @param type the type of this debugging record.
 * @param message the debugging message
 * @param theModule the module which is related to this record or null,
 * if the record is related to the datalogger.
 */
DebuggingRecord::DebuggingRecord(DebuggingType type, const std::string &message, AbstractModule *theModule)
        : GenericRecord(type, message) {
    this->theModule = theModule;
}

//TODO support handlers, status indicators,...
AbstractModule *DebuggingRecord::getModule() const {
    return this->theModule;
}
