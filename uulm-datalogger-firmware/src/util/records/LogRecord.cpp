//
// Created by Falko Schmidt on 16.06.22.
//

#include "util/records/LogRecord.h"

/*
LogRecord::LogRecord(LogRecord::LogType type, const std::string &message) {
    this->type = type;
    this->message = message;
}

LogRecord::LogType LogRecord::getType() const {
    return type;
}

const std::string &LogRecord::getMessage() const {
    return message;
}*/


LogRecord::LogRecord(LogType type, const std::string &message) : GenericRecord(type, message) {
    // Nothing more to do here.
}

