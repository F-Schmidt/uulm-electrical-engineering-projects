//
// Created by Falko Alrik Schmidt on 20.11.22.
//
#include "util/Unit.h"

std::string Unit::of(const char* prefix, const char* unit) {
    std::stringstream ss;
    ss << prefix << unit;
    return ss.str();
}

std::string Unit::frac(const char* numerator, const char* denominator) {
    std::stringstream ss;
    ss << numerator << '/' << denominator;
    return ss.str();
}
