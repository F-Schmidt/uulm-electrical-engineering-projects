//
// Created by Falko Alrik Schmidt on 20.11.22.
//

#include "util/datafields/BooleanDatafield.h"


std::string BooleanDatafield::getFunctionResult() {

    bool result = this->func();

    switch (this->outputType) {
        case BooleanDatafield::STRING:
            return result ? "true" : "false";
        default: /* BooleanDatafield::NUMBER: */
            return result ? "1" : "0";
    }
}

BooleanDatafield::BooleanDatafield(const std::string &header, std::function<bool()>& func,
                                   BooleanDatafield::OutputType outputType) : GenericDatafield(header), func(func),
                                                                              outputType(outputType) {}



