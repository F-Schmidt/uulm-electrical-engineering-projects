//
// Created by Falko Alrik Schmidt on 20.11.22.
//

#include <sstream>
#include "util/datafields/FloatingPointDatafield.h"


template<typename T>
FloatingPointDatafield<T>::FloatingPointDatafield(const std::string &header, const std::function<T()> &func,
                                                  int precision)
        : GenericDatafield(header), func(func), precision(precision) {
    // Nothing more to do here.
}

template<typename T>
std::string FloatingPointDatafield<T>::getFunctionResult() {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(this->precision) << this->func();
    return ss.str();
}

template class FloatingPointDatafield<float>;
template class FloatingPointDatafield<double>;
