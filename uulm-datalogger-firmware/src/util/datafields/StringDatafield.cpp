//
// Created by Falko Alrik Schmidt on 20.11.22.
//

#include "util/datafields/StringDatafield.h"

StringDatafield::StringDatafield(const std::string &header, std::function<std::string()> &func)
        : GenericDatafield(header), func(func) {
    // Nothing more to do here.
}

std::string StringDatafield::getFunctionResult() {
    // The function of this object type already returns a string,
    // so we just have to call it.
    return this->func();
}
