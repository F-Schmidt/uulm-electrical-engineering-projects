//
// Created by Falko Alrik Schmidt on 20.11.22.
//
#include <Arduino.h>
#include <sstream>
#include "util/datafields/NumericDatafield.h"


template<typename T>
std::string NumericDatafield<T>::getFunctionResult() {
    std::stringstream ss;
    ss << this->func();
    return ss.str();
}

template<typename T>
NumericDatafield<T>::NumericDatafield(const std::string &header, const std::function<T()> &func):GenericDatafield(
        header), func(func) {
//TODO THIS FOR ALL; NO POINTERS; DIRECT FUNCTIONS!
        }


template class NumericDatafield<char>;

template class NumericDatafield<uint8_t>;
template class NumericDatafield<uint16_t>;
template class NumericDatafield<uint32_t>;
template class NumericDatafield<uint64_t>;

template class NumericDatafield<int8_t>;
template class NumericDatafield<int16_t>;
template class NumericDatafield<int32_t>;
template class NumericDatafield<int64_t>;
