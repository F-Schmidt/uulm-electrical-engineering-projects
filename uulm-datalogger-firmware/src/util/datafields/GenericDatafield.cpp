//
// Created by Falko Alrik Schmidt on 20.11.22.
//
#include "util/datafields/GenericDatafield.h"

#include <utility>

GenericDatafield::GenericDatafield(std::string header) : header(std::move(header)) {
    // Nothing more to do here.
}

const std::string &GenericDatafield::getHeader() const {
    return header;
}
