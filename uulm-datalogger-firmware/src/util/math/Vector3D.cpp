//
// Created by Falko Schmidt on 28.05.22.
//

#include "util/math/Vector3D.h"

template<typename T>
Vector3D<T>::Vector3D(T x, T y, T z) : Vector2D<T>(x, y) {
    this->z = z;
}

template<typename T>
T Vector3D<T>::getZ() const {
    return z;
}

template<typename T>
void Vector3D<T>::setZ(T z) {
    this->z = z;
}

template class Vector3D<float>;
