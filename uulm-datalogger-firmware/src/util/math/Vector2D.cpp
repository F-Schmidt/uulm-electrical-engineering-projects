//
// Created by Falko Schmidt on 28.05.22.
//

#include "util/math/Vector2D.h"

template<typename T>
Vector2D<T>::Vector2D(T x, T y) {
    this->x = x;
    this->y = y;
}

template<typename T>
T Vector2D<T>::getX() const {
    return x;
}

template<typename T>
T Vector2D<T>::getY() const {
    return y;
}

template<typename T>
void Vector2D<T>::setX(T x) {
    this->x = x;
}

template<typename T>
void Vector2D<T>::setY(T y) {
    this->y = y;
}


template<typename T>
Vector2D<T> Vector2D<T>::operator+(Vector2D<T> &add) {
    Vector2D<T> temp(0, 0);
    temp.x = this->x + add.x;
    temp.y = this->y + add.y;
    return temp;
}


template class Vector2D<float>;
