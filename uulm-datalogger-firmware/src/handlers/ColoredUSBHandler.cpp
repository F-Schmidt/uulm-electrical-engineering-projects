//
// Created by Falko Schmidt on 04.06.22.
//
#include <Arduino.h>
#include "handlers/ColoredUSBHandler.h"
#include "util/ANSIColor.h"

ColoredUSBHandler::ColoredUSBHandler(char columnSeparator) : columnSeparator(columnSeparator) {}


void ColoredUSBHandler::writeRecord(const LogRecord &log) {
    if(log.getType() != HEADER && log.getType() != DATA_ENTITY) {
        // If our type is neither the header line, nor an entity with sampled values,
        // then we will ignore it.
        return;
    }

    // Init color. We will start with color RED
    enum ANSI::TextColor color = ANSI::TextColor::RED;
    Serial.print(ansiToString(color).c_str());
    color = ANSI::next(color);

    for (char i : log.getMessage()) {
        if(i == this->columnSeparator) {
            // If current char is a column separator, we will change
            // color for next column.
            Serial.print(i);
            Serial.print(ansiToString(color).c_str());
            color = ANSI::next(color);
        } else {
            Serial.print(i);
        }
    }

    Serial.println(ANSI::ansiToString(ANSI::TextColor::RESET).c_str());
}