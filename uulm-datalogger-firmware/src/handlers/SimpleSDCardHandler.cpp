//
// Created by Falko Schmidt on 04.06.22.
//

#include "handlers/SimpleSDCardHandler.h"
#include "SD.h"

SimpleSDCardHandler::SimpleSDCardHandler(uint8_t csPin, const std::string &dataFileName)
        : AbstractHandler("SD-Card Handler") {
    this->csPin = csPin;
    this->dataFileName = dataFileName;
}

bool SimpleSDCardHandler::begin() {
    return SD.begin(this->csPin);
}

void SimpleSDCardHandler::writeRecord(const LogRecord &log) {
    if(log.getType() == DATA_ENTITY || log.getType() == HEADER) {

        // Open file in WRITE mode with APPEND and CREATE
        File f = SD.open(this->dataFileName.c_str(), FILE_WRITE);

        // Only write to file, if we were able to open it
        if(f) {
            f.write(log.getMessage().c_str());
            f.write('\n');
            f.close();
        }
    }
}
