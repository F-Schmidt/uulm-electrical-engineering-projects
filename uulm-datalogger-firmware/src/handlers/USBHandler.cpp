//
// Created by Falko Schmidt on 04.06.22.
//

#include <Arduino.h>
#include "handlers/USBHandler.h"

//void USBHandler::writeData(const std::string& data) {
//    Serial.print(data.c_str());/
//}

bool USBHandler::begin() {
    //TODO CHANGE TO CENTRAL INTERFACE CONTROL
    // SERIAL WILL BE STARTED FOR NOW
    Serial.begin(115200);
    delay(1500);
    return true;
}

void USBHandler::writeRecord(const LogRecord &log) {
    // Our USB Handler will accept every type of LogRecord and
    // just output
    Serial.print(log.getMessage().c_str());
    Serial.print("\r\n");
}

USBHandler::USBHandler(const std::string &name) : AbstractHandler(name) {}

