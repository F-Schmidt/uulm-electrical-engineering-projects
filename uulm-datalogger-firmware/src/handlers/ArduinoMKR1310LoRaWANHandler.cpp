//
// Created by Falko Alrik Schmidt on 11.12.22.
//

#include "handlers/ArduinoMKR1310LoRaWANHandler.h"
#include "LoRa.h"

void ArduinoMKR1310LoRaWANHandler::writeRecord(const LogRecord &log) {
    LoRa.beginPacket();
    LoRa.print(log.getMessage().c_str());
    LoRa.endPacket(true);
}

bool ArduinoMKR1310LoRaWANHandler::begin() {
    return LoRa.begin(868E6);
}

ArduinoMKR1310LoRaWANHandler::ArduinoMKR1310LoRaWANHandler()
        : AbstractHandler("Arduino MKR1310 LoRaWAN-Handler") {}
