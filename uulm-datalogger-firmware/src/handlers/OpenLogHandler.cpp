//
// Created by Falko Alrik Schmidt on 22.10.22.
//

#include <sstream>
#include "handlers/OpenLogHandler.h"


OpenLogHandler::OpenLogHandler(UART *uart, int reset, unsigned long baud)
        : AbstractHandler("OpenLog-Handler") {
    this->uart = uart;
    this->resetPin = reset;
    this->baud = baud;
    this->isInCommandMode = false;
}

bool OpenLogHandler::begin() {

    // Start serial connection
    this->uart->begin(this->baud);

    // Setup reset pin and (hardware-)reset module
    pinMode(this->resetPin, OUTPUT);
    this->reset();

    // Wait for OpenLog-module to respond with '<'.
    // This indicates that the module is recording to file.
    // This function uses a five-second timeout to not hang up
    // the datalogger.
    unsigned long currentMillis = millis();
    bool hasResponded = false;
    while (currentMillis + 5000 > millis()) {
        if(this->uart->available()) {
            if(this->uart->read() == '<') {
                hasResponded = true;
                break;
            }
        }
    }

    return hasResponded;
}





void OpenLogHandler::writeRecord(const LogRecord &log) {
    if(log.getType() == HEADER || log.getType() == DATA_ENTITY) {
        // D A T A
        this->uart->println(log.getMessage().c_str());
    }
}


void OpenLogHandler::enterCommandMode() {

    if(this->isInCommandMode) {
        return;
    }

    this->sendEscapeSequence();

    //TODO time limit!!!
    while(true) {
        if(this->uart->available()) {
            if(this->uart->read() == '>') {
                this->isInCommandMode = true;
                return;
            }
        }
    }

}


bool OpenLogHandler::isCommandMode() const {
    return this->isInCommandMode;
}


void OpenLogHandler::sendEscapeSequence() {
    for(uint8_t i = 0; i < OPENLOG_DEFAULT_AMOUNT_ESCAPE_CHAR; i++) {
        this->uart->write(OPENLOG_DEFAULT_ESCAPE_CHAR);
    }
}

void OpenLogHandler::reset() const {
    digitalWrite(this->resetPin, LOW);
    delay(100);
    digitalWrite(this->resetPin, HIGH);
}

void OpenLogHandler::softReset() {
    if(!this->isInCommandMode) {
        this->enterCommandMode();
    }

    // Send command with line break
    this->uart->println("reset");
}

void OpenLogHandler::sendCommand(const char *cmd) {
    this->enterCommandMode();

    // Send command with line break
    this->uart->println(cmd);
}

void OpenLogHandler::initSD() {
    this->sendCommand("init");
}

void OpenLogHandler::syncSD() {
    this->sendCommand("sync");
}

std::string OpenLogHandler::readSDInformation() {
    this->sendCommand("disk");

    // Read response
    std::stringstream ss;
    while (this->uart->available()) {
        ss << this->uart->read();
    }

    return ss.str();
}

std::string OpenLogHandler::getAvailableCommands() {
    this->sendCommand("?");

    // Read response
    std::stringstream ss;
    while (this->uart->available()) {
        ss << this->uart->read();
    }

    return ss.str();
}
