//
// Created by Falko Schmidt on 18.08.22.
//

#include "modules/internal/arduino-mkr1310/ArduinoMKR1310CoreTemperatureModule.h"

ArduinoMKR1310CoreTemperatureModule::ArduinoMKR1310CoreTemperatureModule()
    : MCUCoreTemperatureModule("Arduino MKR1310 (SAMD21 Cortex-M0+) Core Temperature Module") {
    // Currently this project uses an external library that will be used to read the kernel temperature.
    // To use this library, we need to instantiate an object of type 'TemperatureZero'.
    this->mkr1310TemperatureReader = new TemperatureZero();
}

bool ArduinoMKR1310CoreTemperatureModule::begin() {
    this->mkr1310TemperatureReader->init();
    return true;
}

bool ArduinoMKR1310CoreTemperatureModule::sample() {
    this->mkr1310TemperatureReader->wakeup();
    this->coreTemperature = this->mkr1310TemperatureReader->readInternalTemperature();
    this->mkr1310TemperatureReader->disable();
    return true;
}

