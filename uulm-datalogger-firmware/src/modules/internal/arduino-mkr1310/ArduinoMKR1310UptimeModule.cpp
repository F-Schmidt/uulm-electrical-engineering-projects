//
// Created by Falko Schmidt on 17.08.22.
//

#include "Arduino.h"
#include "modules/internal/arduino-mkr1310/ArduinoMKR1310UptimeModule.h"
#include "util/Unit.h"


bool ArduinoMKR1310UptimeModule::sample() {
    this->uptimeMicros = micros();
    return true;
}

ArduinoMKR1310UptimeModule::ArduinoMKR1310UptimeModule() : MCUUptimeModule("Arduino MKR Uptime Module",
                          "This module reads the uptime of Arduino MKR MCU in microseconds.") {
    // Nothing to do here
}

bool ArduinoMKR1310UptimeModule::begin() {
    // The method 'getUptimeMicros' is defined in super class, so we have to explicitly tell the compiler
    // which type is our generic. We will use THIS class, so call:
    //  this->addDatafield<ArduinoMKR1310UptimeModule>(...);
    this->addDatafield<ArduinoMKR1310UptimeModule>("", Unit::of(Unit::MICRO, Unit::SECOND),
                       &ArduinoMKR1310UptimeModule::getUptimeMicros, this);
    return true;
}
