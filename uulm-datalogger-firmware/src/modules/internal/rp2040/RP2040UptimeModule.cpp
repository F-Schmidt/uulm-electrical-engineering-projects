//
// Created by Falko Schmidt on 26.05.22.
//

#include "timer.h"
#include "modules/internal/rp2040/RP2040UptimeModule.h"
#include "util/Unit.h"

RP2040UptimeModule::RP2040UptimeModule() : MCUUptimeModule("RP2040 Uptime Module",
                   "This module reads the uptime of RP2040 MCU in microseconds.") {
    // Nothing more to do here
}

bool RP2040UptimeModule::begin() {
    // This method call must specify the generic parameter T to be 'RP2040UptimeModule', because
    // otherwise it could also be MCUUptimeModule due to the fact, that the method 'getUptimeMicros'
    // is defined in the super class of 'this'.
    this->addDatafield<RP2040UptimeModule>("RP2040 MCU Uptime",
                                           Unit::of(Unit::MICRO, Unit::SECOND),
                                           &RP2040UptimeModule::getUptimeMicros,
                                           this);
    return true;
}

bool RP2040UptimeModule::sample() {
    this->uptimeMicros = time_us_64();
    return true;
}
