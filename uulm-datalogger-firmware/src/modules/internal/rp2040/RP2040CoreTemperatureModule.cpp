//
// Created by Falko Schmidt on 25.05.22.
//

#include <adc.h>
#include "modules/internal/rp2040/RP2040CoreTemperatureModule.h"
#include "util/Unit.h"


#define                 RP2040_INTERNAL_TEMPERATURE_SENSOR_ANALOG_PIN                   (0x04u)


RP2040CoreTemperatureModule::RP2040CoreTemperatureModule()
    : MCUCoreTemperatureModule("RP2040 (Dual ARM Cortex-M0+) Core Temperature Module") {
}


bool RP2040CoreTemperatureModule::begin() {
    // Start ADC circuit of RP2040-MCU
    adc_init();

    this->addDatafield<RP2040CoreTemperatureModule>("RP2040 MCU Core Temperature",
                       Unit::CELSIUS,
                       &RP2040CoreTemperatureModule::getCoreTemperature,
                       this,
                       3);

    return true;
}


bool RP2040CoreTemperatureModule::sample() {

    adc_set_temp_sensor_enabled(true);

    // Select the analog input that is connected to the internal temperature sensor.
    // For this, we will use the Pico SDK functions.
    adc_select_input(RP2040_INTERNAL_TEMPERATURE_SENSOR_ANALOG_PIN);

    // Refer to RP2040 datasheet, chapter 4.9.5. Temperature Sensor:
    // Calculate the voltage.
    double voltage = (((double)adc_read()) / 4096.0) * 3.3;

    // Update the temperature using the formula from datasheet
    this->coreTemperature = 27 - (voltage - 0.706) / 0.001721;

    adc_set_temp_sensor_enabled(false);

    // No errors can be detected, so we will just return true.
    return true;
}

