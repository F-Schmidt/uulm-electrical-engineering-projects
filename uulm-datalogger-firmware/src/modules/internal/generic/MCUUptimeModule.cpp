//
// Created by Falko Schmidt on 17.08.22.
//

#include <sstream>
#include "modules/internal/generic/MCUUptimeModule.h"
#include "util/Unit.h"

MCUUptimeModule::MCUUptimeModule(const std::string &name, const std::string &description)
    : InternalModule(name, description) {
    this->uptimeMicros = 0;
}

uint64_t MCUUptimeModule::getUptimeMicros() const {
    return this->uptimeMicros;
}

bool MCUUptimeModule::begin() {
    this->addDatafield("MCU Uptime", Unit::of(Unit::MICRO, Unit::SECOND),
                       &MCUUptimeModule::getUptimeMicros, this);
    return true;
}