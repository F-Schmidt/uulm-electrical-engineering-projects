//
// Created by Falko Schmidt on 18.08.22.
//

#include <sstream>
#include <iomanip>
#include "modules/internal/generic/MCUCoreTemperatureModule.h"
#include "util/Unit.h"

MCUCoreTemperatureModule::MCUCoreTemperatureModule(const std::string &name, const std::string &description)
        : InternalModule(name, description) {
    this->coreTemperature = 0.0;
}

double MCUCoreTemperatureModule::getCoreTemperature() const {
    return coreTemperature;
}

bool MCUCoreTemperatureModule::begin() {
    this->addDatafield("MCU Core Temperature",
                       Unit::CELSIUS,
                       &MCUCoreTemperatureModule::getCoreTemperature,
                       this,
                       3);
    return true;
}

