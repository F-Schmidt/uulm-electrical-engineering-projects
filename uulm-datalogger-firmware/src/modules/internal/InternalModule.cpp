//
// Created by Falko Schmidt on 25.05.22.
//

#include "modules/internal/InternalModule.h"

InternalModule::InternalModule(const std::string &name, const std::string &description)
        : AbstractModule(name, description, AbstractModule::INTERNAL) {
    // Nothing to do here
}

bool InternalModule::isEnabled() {
    return AbstractModule::isEnabled();
}

std::string InternalModule::getErrorStr() const {
    return AbstractModule::getErrorStr();
}
