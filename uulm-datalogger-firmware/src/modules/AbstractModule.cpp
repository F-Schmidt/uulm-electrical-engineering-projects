//
// Created by Falko Schmidt on 25.05.22.
//

#include <sstream>
#include "modules/AbstractModule.h"
#include "util/datafields/GenericDatafield.h"
#include "util/datafields/FloatingPointDatafield.h"
#include "util/datafields/BooleanDatafield.h"
#include "util/datafields/StringDatafield.h"
#include "util/datafields/NumericDatafield.h"

AbstractModule::AbstractModule(const std::string &name, const std::string &description,
                               const AbstractModule::ModuleType type)
                               : Nameable(name), Describable(description) {
    this->type = type;
    this->datafields = std::vector<GenericDatafield*>();
}

AbstractModule::ModuleType AbstractModule::getType() const {
    return this->type;
}

const std::vector<GenericDatafield *> &AbstractModule::getDatafields() const {
    return datafields;
}


bool AbstractModule::isEnabled() {
    // If not otherwise defined, the module is enabled.
    return true;
}


std::string AbstractModule::getErrorStr() const {
    return "This module does not support errno.";
}

bool AbstractModule::supportsErrno() const {
    return false;
}

