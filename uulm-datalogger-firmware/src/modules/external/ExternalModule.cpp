//
// Created by Falko Schmidt on 25.05.22.
//

#include "modules/external/ExternalModule.h"

ExternalModule::ExternalModule(const std::string &name, const std::string &description)
        : AbstractModule(name, description, AbstractModule::EXTERNAL) {
    // Nothing more to do here
}
