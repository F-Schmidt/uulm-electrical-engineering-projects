//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#include "modules/external/tempberry/TempberryBME680Module.h"
#include "util/Unit.h"

TempberryBME680Module::TempberryBME680Module(uint8_t enableSwitchPin, int8_t bme680ssPin)
        : TempberryModule("Tempberry BME680 module", "This module controls a sensor of type BME680",
                          enableSwitchPin) {
    this->bme680 = Adafruit_BME680(bme680ssPin);
    this->altitude = 0.0;
    this->humidity = 0.0;
    this->temperature = 0.0;
    this->gasResistance = 0.0;
}

bool TempberryBME680Module::begin() {
    if(!this->bme680.begin()) {
        return false;
    }

    // Set up oversampling and filter initialization
    bme680.setTemperatureOversampling(BME680_OS_8X);
    bme680.setHumidityOversampling(BME680_OS_2X);
    bme680.setPressureOversampling(BME680_OS_4X);
    bme680.setIIRFilterSize(BME680_FILTER_SIZE_3);
    bme680.setGasHeater(320, 150); // 320*C for 150 ms


    this->addDatafield("Temperature (BME680)", Unit::CELSIUS,
                       &TempberryBME680Module::getTemperature, this, 2);
    this->addDatafield("Humidity (BME680)", Unit::PERCENT,
                       &TempberryBME680Module::getHumidity, this, 2);
    this->addDatafield("Gas Resistance (BME680)", Unit::of(Unit::KILO, "Ohm"),
                       &TempberryBME680Module::getGasResistance, this, 2);
    this->addDatafield("Altitude (BME680)", Unit::METER,
                       &TempberryBME680Module::getAltitude, this, 2);
    return true;
}

bool TempberryBME680Module::sample() {
    if(!this->bme680.performReading()) {
        return false;
    }
    this->temperature = this->bme680.temperature;
    this->humidity = this->bme680.humidity;
    this->gasResistance = this->bme680.gas_resistance;
    this->altitude = this->bme680.readAltitude(1013.25);
    return true;
}

float TempberryBME680Module::getTemperature() const {
    return temperature;
}

float TempberryBME680Module::getHumidity() const {
    return humidity;
}

float TempberryBME680Module::getGasResistance() const {
    return gasResistance;
}

float TempberryBME680Module::getAltitude() const {
    return altitude;
}
