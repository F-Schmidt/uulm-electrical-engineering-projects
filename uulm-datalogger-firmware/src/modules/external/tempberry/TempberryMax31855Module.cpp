//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#include "TempberryMax31855Module.h"
#include "util/Unit.h"

TempberryMax31855Module::TempberryMax31855Module(uint8_t enableSwitchPin, int8_t max31855ssPin)
        : TempberryModule("Tempberry MAX31855 module", "This module controlls a sensor of type Max31855",
                          enableSwitchPin) {
    this->thermocouple = new Adafruit_MAX31855(max31855ssPin);
    this->internalTemperature = 0.0;
    this->temperature = 0.0;
}

bool TempberryMax31855Module::begin() {
    if(!this->thermocouple->begin()) {
        return false;
    }
    // Add datafields
    this->addDatafield("Max31855 internal temperature", Unit::CELSIUS,
                       &TempberryMax31855Module::getInternalTemperature, this, 2);
    this->addDatafield("Temperature (Max31855)", Unit::CELSIUS,
                       &TempberryMax31855Module::getTemperature, this, 2);
    return true;
}

bool TempberryMax31855Module::sample() {
    this->temperature = this->thermocouple->readCelsius();
    this->internalTemperature = this->thermocouple->readInternal();
    return true;
}

double TempberryMax31855Module::getInternalTemperature() const {
    return internalTemperature;
}

double TempberryMax31855Module::getTemperature() const {
    return temperature;
}
