//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#include "modules/external/tempberry/TempberryDHT11Module.h"
#include "util/Unit.h"

TempberryDHT11Module::TempberryDHT11Module(uint8_t enableSwitchPin, uint8_t dht11Pin)
        : TempberryModule("Tempberry DHT11 module","This module controls a sensor of type DHT11",
                          enableSwitchPin) {
    this->dht11 = new DHT(dht11Pin, DHT11);
    this->temperature = 0.0;
    this->humidity = 0.0;
}

bool TempberryDHT11Module::begin() {
    this->dht11->begin();
    this->addDatafield("DHT11 Temperature", Unit::CELSIUS,
                       &TempberryDHT11Module::getTemperature, this, 2);
    this->addDatafield("DHT11 Humidity", Unit::PERCENT,
                       &TempberryDHT11Module::getHumidity, this, 2);
    return true;
}

bool TempberryDHT11Module::sample() {
    float measuredTemperature = this->dht11->readTemperature();
    float measuredHumidity = this->dht11->readHumidity();

    if(isnan(measuredTemperature) || isnan(measuredHumidity)) {
        // Error in measurement
        return false;
    }

    // No error -> set the values
    this->temperature = measuredTemperature;
    this->humidity = measuredHumidity;

    return true;
}

float TempberryDHT11Module::getHumidity() const {
    return humidity;
}

float TempberryDHT11Module::getTemperature() const {
    return temperature;
}


