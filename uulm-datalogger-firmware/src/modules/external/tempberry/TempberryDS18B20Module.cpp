//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#include "modules/external/tempberry/TempberryDS18B20Module.h"
#include "util/Unit.h"


TempberryDS18B20Module::TempberryDS18B20Module(uint8_t enableSwitchPin, uint8_t oneWirePin)
        : TempberryModule("Tempberry DS18B20 module",
                          "This module can be used to control a temperature sensor of type DS18B20.",
                          enableSwitchPin) {
    this->oneWire = OneWire(oneWirePin);
    this->sensors = DallasTemperature(&this->oneWire);
    this->temperature = 0.0;
}

bool TempberryDS18B20Module::begin() {
    this->sensors.begin();
    this->addDatafield("DS18B20 temperature", Unit::CELSIUS, &TempberryDS18B20Module::getTemperature,
                       this, 2);
    return true;
}

bool TempberryDS18B20Module::sample() {
    this->temperature = this->sensors.getTempCByIndex(0);
    return true;
}

float TempberryDS18B20Module::getTemperature() const {
    return this->temperature;
}
