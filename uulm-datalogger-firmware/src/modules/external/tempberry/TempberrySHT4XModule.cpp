//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#include "modules/external/tempberry/TempberrySHT4XModule.h"
#include "util/Unit.h"

TempberrySHT4XModule::TempberrySHT4XModule(uint8_t enableSwitchPin)
        : TempberryModule("Tempberry SHT4X module", "This module controls a sensor of type SHT4X",
                          enableSwitchPin) {
    this->temperature = 0.0;
    this->humidity = 0.0;
    this->sht4x = SHTSensor(SHTSensor::SHT4X);
}

bool TempberrySHT4XModule::begin() {
    if(!this->sht4x.init()) {
        return false;
    }
    this->addDatafield("Temperature (SHT4X)", Unit::CELSIUS,
                       &TempberrySHT4XModule::getTemperature, this, 2);
    this->addDatafield("Humidity (SHT4X)", Unit::PERCENT,
                       &TempberrySHT4XModule::getHumidity, this, 2);
    return true;
}

bool TempberrySHT4XModule::sample() {
    if(!sht4x.readSample()) {
        return false;
    }
    this->humidity = this->sht4x.getHumidity();
    this->temperature = this->sht4x.getTemperature();
    return true;
}

float TempberrySHT4XModule::getTemperature() const {
    return temperature;
}

float TempberrySHT4XModule::getHumidity() const {
    return humidity;
}
