//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#include "modules/external/tempberry/TempberryMCP3201Module.h"
#include "util/Unit.h"

#define             TEMPBERRY_MCP3201_VREF              (3300)      // 3.3V
#define             MCP3204_RESOLUTION                  (1 << 12)   // 12 bit resolution (4096)

TempberryMCP3201Module::TempberryMCP3201Module(uint8_t enableSwitchPin, uint8_t mcp3201ssPin)
        : TempberryModule("Tempberry MCP3201 module",
          "This module calculates the temperature using a voltage divider and a 12 bit ADC.",
          enableSwitchPin) {
    this->adc = new MCP3201(TEMPBERRY_MCP3201_VREF, mcp3201ssPin);
    this->temperature = 0.0;
}

bool TempberryMCP3201Module::begin() {
    this->addDatafield("Temperature (MCP3201)", Unit::CELSIUS,
                       &TempberryMCP3201Module::getTemperature, this, 2);
    return true;
}

bool TempberryMCP3201Module::sample() {
    uint16_t rawValue = this->adc->read(MCP3201::Channel::SINGLE_0);

    if(rawValue == 0) {
        // Prevent divide by zero error
        this->temperature = -234.1;
    } else {
        // Calculate resistor value in kiloohms from raw analog value using a voltage divider
        double resistorKOhms = 10000 / ((double) (MCP3204_RESOLUTION) / rawValue - 1) / 1000;

        // Calculate the exact temperature value. This function was created using MATLAB with
        // the POLYFIT functionality.
        this->temperature = 15.13 * pow(resistorKOhms, -0.432) + 305.9 * pow(resistorKOhms, -0.08183) - 234.1;
    }

    return true;
}

double TempberryMCP3201Module::getTemperature() const {
    return temperature;
}