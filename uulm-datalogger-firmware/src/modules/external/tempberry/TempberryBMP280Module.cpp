//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#include <Serial.h>
#include "modules/external/tempberry/TempberryBMP280Module.h"
#include "util/Unit.h"

TempberryBMP280Module::TempberryBMP280Module(uint8_t enableSwitchPin, int8_t bmp280ssPin)
        : TempberryModule("Tempberry BMP280 module", "This module controls a sensor of type BMP280",
                          enableSwitchPin) {
    this->bmp280 = Adafruit_BMP280(bmp280ssPin);
    this->temperature = 0.0;
    this->pressure = 0.0;
    this->altitude = 0.0;
}

bool TempberryBMP280Module::begin() {
    if(!this->bmp280.begin()) {
        return false;
    }

    /* Default settings from datasheet. */
    this->bmp280.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                    Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                    Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                    Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                    Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */

    this->addDatafield("Temperature (BMP280)", Unit::CELSIUS,
                       &TempberryBMP280Module::getTemperature, this, 2);
    this->addDatafield("Pressure (BMP280)", "Pa",
                       &TempberryBMP280Module::getPressure, this, 2);
    this->addDatafield("Altitude (BMP280)", Unit::METER,
                       &TempberryBMP280Module::getAltitude, this, 2);
    return true;
}

bool TempberryBMP280Module::sample() {
    this->temperature = this->bmp280.readTemperature();
    this->pressure = this->bmp280.readPressure();
    this->altitude = this->bmp280.readAltitude();
    return true;
}

float TempberryBMP280Module::getTemperature() const {
    return temperature;
}

float TempberryBMP280Module::getPressure() const {
    return pressure;
}

float TempberryBMP280Module::getAltitude() const {
    return altitude;
}
