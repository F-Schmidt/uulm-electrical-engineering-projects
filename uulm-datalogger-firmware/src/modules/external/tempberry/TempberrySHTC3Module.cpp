//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#include "modules/external/tempberry/TempberrySHTC3Module.h"
#include "util/Unit.h"

TempberrySHTC3Module::TempberrySHTC3Module(uint8_t enableSwitchPin)
        : TempberryModule("Tempberry SHTC3 module", "This module controls a sensor of type SHTC3",
                          enableSwitchPin) {
    this->temperature = 0.0;
    this->humidity = 0.0;
    this->shtc3 = SHTSensor(SHTSensor::SHTC3);
}

bool TempberrySHTC3Module::begin() {
    if(!this->shtc3.init()) {
        return false;
    }

    this->addDatafield("Temperature SHTC3", Unit::CELSIUS,
                       &TempberrySHTC3Module::getTemperature, this, 2);
    this->addDatafield("Humidity SHTC3", Unit::PERCENT,
                       &TempberrySHTC3Module::getHumidity, this, 2);
    return true;
}

bool TempberrySHTC3Module::sample() {
    if(!shtc3.readSample()) {
        return false;
    }
    this->humidity = this->shtc3.getHumidity();
    this->temperature = this->shtc3.getTemperature();
    return true;
}

float TempberrySHTC3Module::getHumidity() const {
    return humidity;
}

float TempberrySHTC3Module::getTemperature() const {
    return temperature;
}
