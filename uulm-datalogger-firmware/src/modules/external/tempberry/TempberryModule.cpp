//
// Created by Falko Schmidt on 02.09.22.
//

#include "Arduino.h"
#include "modules/external/tempberry/TempberryModule.h"


bool TempberryModule::isEnabled() {
    // Setup pin
    pinMode(this->enableSwitchPin, INPUT_PULLUP);

    // Let the pin settle
    delay(10);

    // Switch is in state 'ON'.
    return digitalRead(this->enableSwitchPin) == HIGH;
}

TempberryModule::TempberryModule(const std::string &name, const std::string &description, uint8_t enableSwitchPin)
    : ExternalModule(name, description) {
    this->enableSwitchPin = enableSwitchPin;
}
