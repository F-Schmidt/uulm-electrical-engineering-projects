//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_TEMPBERRYMAX31855MODULE_H
#define UULM_DATALOGGER_FIRMWARE_TEMPBERRYMAX31855MODULE_H


#include "modules/external/tempberry/TempberryModule.h"
#include "../../../../.pio/libdeps/tempberry_prototype/Adafruit MAX31855 library/Adafruit_MAX31855.h"

class TempberryMax31855Module : public TempberryModule {

private:
    Adafruit_MAX31855* thermocouple;
    double internalTemperature;
    double temperature;

public:
    explicit TempberryMax31855Module(uint8_t enableSwitchPin, int8_t max31855ssPin);

    bool begin() override;
    bool sample() override;

    double getInternalTemperature() const;

    double getTemperature() const;

};


#endif //UULM_DATALOGGER_FIRMWARE_TEMPBERRYMAX31855MODULE_H
