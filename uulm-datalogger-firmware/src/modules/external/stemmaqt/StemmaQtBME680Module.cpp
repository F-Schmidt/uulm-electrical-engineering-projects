//
// Created by Falko Schmidt on 20.07.22.
//

#include "modules/external/stemmaqt/StemmaQtBME680Module.h"

#define SEALEVELPRESSURE_HPA (1013.25)

StemmaQtBME680Module::StemmaQtBME680Module()
    : ExternalModule("BME680 Module,", "Measures temperature and humidity") {
    this->bme680 = new Adafruit_BME680();
}


bool StemmaQtBME680Module::begin() {
    if(!this->bme680->begin()) {
        Serial.println("Could not find a valid BME680 sensor, check wiring!");
        return false;
    }
    this->addDatafield("Temperature (BME680) [°C]", __CALLBACK__(StemmaQtBME680Module::getTemperature));
    this->addDatafield("Humidity (BME680) [%]", __CALLBACK__(StemmaQtBME680Module::getHumidity));
    this->addDatafield("Pressure (BME680) [hPa]", __CALLBACK__(StemmaQtBME680Module::getPressure));
    this->addDatafield("Gas Resistance (BME680) [kOhms]", __CALLBACK__(StemmaQtBME680Module::getGasResistance));
    this->addDatafield("Altitude (BME680) [m]", __CALLBACK__(StemmaQtBME680Module::getAltitude));

    // Set up oversampling and filter initialization
    this->bme680->setTemperatureOversampling(BME680_OS_8X);
    this->bme680->setHumidityOversampling(BME680_OS_2X);
    this->bme680->setPressureOversampling(BME680_OS_4X);
    this->bme680->setIIRFilterSize(BME680_FILTER_SIZE_3);
    this->bme680->setGasHeater(320, 150); // 320*C for 150 ms
    return true;
}

bool StemmaQtBME680Module::sample() {
    this->humidity = this->bme680->readHumidity();
    this->temperature = this->bme680->readTemperature();
    this->pressure = this->bme680->readPressure();
    this->gasResistance = this->bme680->readGas();
    this->altitude = this->bme680->readAltitude(SEALEVELPRESSURE_HPA);
    return true;
}

std::string StemmaQtBME680Module::getTemperature() {
    return std::to_string(this->temperature);
}

std::string StemmaQtBME680Module::getHumidity() {
    return std::to_string(this->humidity);
}

std::string StemmaQtBME680Module::getPressure() {
    return std::to_string(this->pressure / 100.0);
}

std::string StemmaQtBME680Module::getGasResistance() {
    return std::to_string(this->gasResistance / 1000.0);
}

std::string StemmaQtBME680Module::getAltitude() {
    return std::to_string(this->altitude);
}


