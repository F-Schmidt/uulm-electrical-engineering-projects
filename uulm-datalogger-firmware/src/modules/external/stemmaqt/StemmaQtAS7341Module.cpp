//
// Created by Falko Schmidt on 20.07.22.
//

#include "modules/external/stemmaqt/StemmaQtAS7341Module.h"

StemmaQtAS7341Module::StemmaQtAS7341Module()
        : ExternalModule("AS7341 Light Module", "Measures the light strength on multiple wave lengths") {
    this->as7341 = new Adafruit_AS7341();
}


bool StemmaQtAS7341Module::begin() {
    if(!this->as7341->begin()) {
        Serial.println("Failed to find AS7341 sensor!");
        return false;
    }
    this->addDatafield("", __CALLBACK__(StemmaQtAS7341Module::getLightAt415nm));
    this->addDatafield("Light at 445nm (AS7341)", __CALLBACK__(StemmaQtAS7341Module::getLightAt445nm));
    this->addDatafield("Light at 480nm (AS7341)", __CALLBACK__(StemmaQtAS7341Module::getLightAt480nm));
    this->addDatafield("Light at 515nm (AS7341)", __CALLBACK__(StemmaQtAS7341Module::getLightAt515nm));
    this->addDatafield("Light at 555nm (AS7341)", __CALLBACK__(StemmaQtAS7341Module::getLightAt555nm));
    this->addDatafield("Light at 590nm (AS7341)", __CALLBACK__(StemmaQtAS7341Module::getLightAt590nm));
    this->addDatafield("Light at 630nm (AS7341)", __CALLBACK__(StemmaQtAS7341Module::getLightAt630nm));
    this->addDatafield("Light at 680nm (AS7341)", __CALLBACK__(StemmaQtAS7341Module::getLightAt680nm));
    this->addDatafield("Light Clear (AS7341)", __CALLBACK__(StemmaQtAS7341Module::getLightClear));
    this->addDatafield("Light NIR (AS7341)", __CALLBACK__(StemmaQtAS7341Module::getLightNIR));

    this->as7341->setATIME(100);
    this->as7341->setASTEP(999);
    this->as7341->setGain(AS7341_GAIN_256X);
    return true;
}

bool StemmaQtAS7341Module::sample() {
    return this->as7341->readAllChannels(this->readings);
}

std::string StemmaQtAS7341Module::getLightAt415nm() {
    return std::to_string(this->readings[0]);
}

std::string StemmaQtAS7341Module::getLightAt445nm() {
    return std::to_string(this->readings[1]);
}

std::string StemmaQtAS7341Module::getLightAt480nm() {
    return std::to_string(this->readings[2]);
}

std::string StemmaQtAS7341Module::getLightAt515nm() {
    return std::to_string(this->readings[3]);
}

std::string StemmaQtAS7341Module::getLightAt555nm() {
    return std::to_string(this->readings[6]);
}

std::string StemmaQtAS7341Module::getLightAt590nm() {
    return std::to_string(this->readings[7]);
}

std::string StemmaQtAS7341Module::getLightAt630nm() {
    return std::to_string(this->readings[8]);
}

std::string StemmaQtAS7341Module::getLightAt680nm() {
    return std::to_string(this->readings[9]);
}

std::string StemmaQtAS7341Module::getLightClear() {
    return std::to_string(this->readings[10]);
}

std::string StemmaQtAS7341Module::getLightNIR() {
    return std::to_string(this->readings[11]);
}
