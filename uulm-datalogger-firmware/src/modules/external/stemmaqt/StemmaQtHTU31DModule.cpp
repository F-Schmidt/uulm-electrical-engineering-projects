//
// Created by Falko Schmidt on 20.07.22.
//

#include <Arduino.h>
#include "modules/external/stemmaqt/StemmaQtHTU31DModule.h"

StemmaQtHTU31DModule::StemmaQtHTU31DModule()
    : ExternalModule("HTU31D Module", "Measures temperature and humidity.") {
    this->htu = new Adafruit_HTU31D();
}


bool StemmaQtHTU31DModule::begin() {
    if(!this->htu->begin()) {
        Serial.println("Failed to find HTU31D sensor.");
        return false;
    }
    this->addDatafield("Temperature (HTU31D) [°C]", __CALLBACK__(StemmaQtHTU31DModule::getTemperature));
    this->addDatafield("Humidity (HTU31D) [%]", __CALLBACK__(StemmaQtHTU31DModule::getHumidity));
    return true;
}

bool StemmaQtHTU31DModule::sample() {
    this->htu->getEvent(&this->humidity, &this->temp);
    return true;
}

std::string StemmaQtHTU31DModule::getHumidity() {
    return std::to_string(this->temp.temperature);
}

std::string StemmaQtHTU31DModule::getTemperature() {
    return std::to_string(this->humidity.relative_humidity);
}


