//
// Created by Falko Schmidt on 02.07.22.
//

#include "modules/external/stemmaqt/StemmaQtMCP9808Module.h"

StemmaQtMCP9808Module::StemmaQtMCP9808Module()
    : ExternalModule("StemmaQtMCP9808Module",
                     "This module measures the temperature using the MCP9808 sensor.") {
    this->mcp9808 = new Adafruit_MCP9808();
}


bool StemmaQtMCP9808Module::begin() {
    Wire.begin();

    if(!this->mcp9808->begin()) {
        Serial.println("Failed to begin MCP9808 sensor!");
        return false;
    }

    this->addDatafield("Temperature MCP9808 [°C]", __CALLBACK__(StemmaQtMCP9808Module::getTemperatureC));
    this->addDatafield("Temperature MCP9808 [°F]", __CALLBACK__(StemmaQtMCP9808Module::getTemperatureF));

    // Set resolution to finest
    this->mcp9808->setResolution(3);

    // We will wake up on begin, because this function needs some time
    // (about 260 ms) which we can't wait while sampling all modules.
    this->mcp9808->wake();

    return true;
}

bool StemmaQtMCP9808Module::sample() {
    this->temperatureC = this->mcp9808->readTempC();
    this->temperatureF = this->mcp9808->readTempF();
    return true;
}

std::string StemmaQtMCP9808Module::getTemperatureC() const {
    return std::to_string(this->temperatureC);
}

std::string StemmaQtMCP9808Module::getTemperatureF() const {
    return std::to_string(this->temperatureF);
}
