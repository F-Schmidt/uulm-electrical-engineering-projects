//
// Created by Falko Schmidt on 20.07.22.
//

#include <Arduino.h>
#include "modules/external/stemmaqt/StemmaQtLTR390Module.h"

StemmaQtLTR390Module::StemmaQtLTR390Module()
    : ExternalModule("LTR390 Light Module", "Measures UV and light") {
    this->ltr = new Adafruit_LTR390();
    this->uvs = 0;
    this->als = 0;
}


bool StemmaQtLTR390Module::begin() {
    if(!this->ltr->begin(&Wire)) {
        Serial.println("Failed to find LTR390 sensor!");
        return false;
    }
    this->ltr->setGain(LTR390_GAIN_3);
    this->ltr->setResolution(LTR390_RESOLUTION_20BIT);
    this->addDatafield("Ambient Light [Lux]", __CALLBACK__(StemmaQtLTR390Module::getLux));
    this->addDatafield("UV Index", __CALLBACK__(StemmaQtLTR390Module::getUVIndex));
    return true;
}

bool StemmaQtLTR390Module::sample() {
    this->ltr->setMode(LTR390_MODE_UVS);
    this->uvs = this->ltr->readUVS();
    this->ltr->setMode(LTR390_MODE_ALS);
    this->als = this->ltr->readALS();
    return true;
}

double StemmaQtLTR390Module::calculateLux() {
    return (0.6 * this->als) / (this->getGainFactor() * this->getResolutionFactor());
}

uint8_t StemmaQtLTR390Module::getGainFactor() {
    switch(this->ltr->getGain()) {
        case LTR390_GAIN_1: return 1;
        case LTR390_GAIN_3: return 3;
        case LTR390_GAIN_6: return 6;
        case LTR390_GAIN_9: return 9;
        default: return 18;
    }
}

double StemmaQtLTR390Module::getResolutionFactor() {
    switch(this->ltr->getResolution()) {
        case LTR390_RESOLUTION_13BIT: return 0.03125;
        case LTR390_RESOLUTION_16BIT: return 0.25;
        case LTR390_RESOLUTION_17BIT: return 0.5;
        case LTR390_RESOLUTION_18BIT: return 1.0;
        case LTR390_RESOLUTION_19BIT: return 2.0;
        default: return 4.0;
    }
}

double StemmaQtLTR390Module::calculateUVIndex() {
    return this->uvs / (this->getGainFactor() / 18.0) * pow(2, this->getResolutionFactor()) / (pow(2, 20)) * 2300;
}

std::string StemmaQtLTR390Module::getLux() {
    return std::to_string(this->calculateLux());
}

std::string StemmaQtLTR390Module::getUVIndex() {
    return std::to_string(this->calculateUVIndex());
}
