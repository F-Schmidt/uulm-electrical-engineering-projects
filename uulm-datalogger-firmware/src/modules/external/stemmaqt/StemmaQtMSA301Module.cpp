//
// Created by Falko Schmidt on 20.07.22.
//

#include "modules/external/stemmaqt/StemmaQtMSA301Module.h"

StemmaQtMSA301Module::StemmaQtMSA301Module() : ExternalModule("MSA301 module", "Measures acceleration."){
    this->msa301 = new Adafruit_MSA301();
    this->acceleration = new Vector3D<float>(0, 0, 0);
}


bool StemmaQtMSA301Module::begin() {
    if(this->msa301->begin()) {
        this->addDatafield("Acceleration (X) [m/s^2]", __CALLBACK__(StemmaQtMSA301Module::getAccelerationX));
        this->addDatafield("Acceleration (Y) [m/s^2]", __CALLBACK__(StemmaQtMSA301Module::getAccelerationY));
        this->addDatafield("Acceleration (Z) [m/s^2]", __CALLBACK__(StemmaQtMSA301Module::getAccelerationZ));
        return true;
    }
    return false;
}

bool StemmaQtMSA301Module::sample() {
    sensors_event_t event;
    this->msa301->getEvent(&event);
    this->acceleration->setX(event.acceleration.x);
    this->acceleration->setY(event.acceleration.y);
    this->acceleration->setZ(event.acceleration.z);
    return true;
}

std::string StemmaQtMSA301Module::getAccelerationX() {
    return std::to_string(this->acceleration->getX());
}

std::string StemmaQtMSA301Module::getAccelerationY() {
    return std::to_string(this->acceleration->getY());
}

std::string StemmaQtMSA301Module::getAccelerationZ() {
    return std::to_string(this->acceleration->getZ());
}

