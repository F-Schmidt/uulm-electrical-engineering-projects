//
// Created by Falko Schmidt on 28.05.22.
//

#include "modules/external/stemmaqt/StemmaQtTLV493DModule.h"


StemmaQtTLV493DModule::StemmaQtTLV493DModule()
        : ExternalModule("Stemma QT TLV493D 3D Magnetometer module",
                         "This module measures the magnetic field strength in all three dimensions.") {
    this->magnetometer = new Tlv493d();
    this->magnetStrength = new Vector3D<float>(0, 0, 0);
}


bool StemmaQtTLV493DModule::begin() {
    this->magnetometer->begin();
    this->addDatafield("Magnetic field X (TLV493D) [uT]", __CALLBACK__(StemmaQtTLV493DModule::getMagnetFieldStrengthX));
    this->addDatafield("Magnetic field Y (TLV493D) [uT]", __CALLBACK__(StemmaQtTLV493DModule::getMagnetFieldStrengthY));
    this->addDatafield("Magnetic field Z (TLV493D) [uT]", __CALLBACK__(StemmaQtTLV493DModule::getMagnetFieldStrengthZ));
    return true;
}

bool StemmaQtTLV493DModule::sample() {
    // Update data by reading magnetometer
    if(this->magnetometer->updateData() != TLV493D_NO_ERROR) {
        // Failed to fetch data, we will display false
        return false;
    }

    // Update vector
    this->magnetStrength->setX(this->magnetometer->getX());
    this->magnetStrength->setY(this->magnetometer->getY());
    this->magnetStrength->setZ(this->magnetometer->getZ());

    // Sampling successful
    return true;
}

std::string StemmaQtTLV493DModule::getMagnetFieldStrengthX() {
    return std::to_string(this->magnetStrength->getX());
}

std::string StemmaQtTLV493DModule::getMagnetFieldStrengthY() {
    return std::to_string(this->magnetStrength->getY());
}

std::string StemmaQtTLV493DModule::getMagnetFieldStrengthZ() {
    return std::to_string(this->magnetStrength->getZ());
}
