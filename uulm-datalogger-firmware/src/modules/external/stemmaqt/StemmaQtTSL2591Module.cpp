//
// Created by Falko Schmidt on 08.06.22.
//

#include "modules/external/stemmaqt/StemmaQtTSL2591Module.h"

StemmaQtTSL2591Module::StemmaQtTSL2591Module() :
        ExternalModule("StemmaQtTSL2591Module", "Module to access the TSL2591 digital light sensor") {
    this->tsl2591 = new Adafruit_TSL2591();
}


bool StemmaQtTSL2591Module::begin() {
    if(!this->tsl2591->begin()) {
        Serial.println("Failed to begin tsl2591 sensor!");
        return false;
    }
    //TODO DATAFIELDS
    return true;
}


bool StemmaQtTSL2591Module::sample() {
    this->lum = this->tsl2591->getFullLuminosity();
    this->ir = this->lum >> 16;
    this->full = this->lum & 0xFFFF;
    this->lux = this->tsl2591->calculateLux(full, ir);
    return true;
}
