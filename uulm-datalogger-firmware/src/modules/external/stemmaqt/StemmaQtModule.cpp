//
// Created by Falko Schmidt on 28.05.22.
//

#include "modules/external/stemmaqt/StemmaQtModule.h"
#include "modules/external/stemmaqt/StemmaQtTLV493DModule.h"
#include "modules/external/stemmaqt/StemmaQtLIS3MDLModule.h"
#include "modules/external/stemmaqt/StemmaQtTSL2591Module.h"
#include "modules/external/stemmaqt/StemmaQtMCP9808Module.h"
#include "modules/external/stemmaqt/StemmaQtSCD41Module.h"
#include "modules/external/stemmaqt/StemmaQtPMSA003IModule.h"
#include "modules/external/stemmaqt/StemmaQtHTU31DModule.h"
#include "modules/external/stemmaqt/StemmaQtLTR390Module.h"
#include "modules/external/stemmaqt/StemmaQtBME680Module.h"
#include "modules/external/stemmaqt/StemmaQtAS7341Module.h"
#include "modules/external/stemmaqt/StemmaQtMSA301Module.h"

AbstractModule *StemmaQtModule::fromSensor(StemmaQtModule::StemmaQtSensor sensor) {
    switch (sensor) {
        case LIS3MDL:   return new StemmaQtLIS3MDLModule();
        case TLV493D:   return new StemmaQtTLV493DModule();
        case TSL2591:   return new StemmaQtTSL2591Module();
        case MCP9808:   return new StemmaQtMCP9808Module();
        case SCD41:     return new StemmaQtSCD41Module();
        case PMSA003I:  return new StemmaQtPMSA003IModule();
        case LTR390:    return new StemmaQtLTR390Module();
        case HTU31D:    return new StemmaQtHTU31DModule();
        case BME680:    return new StemmaQtBME680Module();
        case AS7341:    return new StemmaQtAS7341Module();
        case MSA301:    return new StemmaQtMSA301Module();
        default:        return nullptr;
    }
}
