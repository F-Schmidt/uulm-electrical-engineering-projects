//
// Created by Falko Schmidt on 20.07.22.
//

#include "modules/external/stemmaqt/StemmaQtPMSA003IModule.h"

StemmaQtPMSA003IModule::StemmaQtPMSA003IModule()
    : ExternalModule("PMSA003I Air Quality Sensor Module", "This module measures the concentration of different particles in the air.") {
    // Nothing to do here
    this->pmsa003i = new Adafruit_PM25AQI();
    this->measurement = new PM25_AQI_Data();
}


bool StemmaQtPMSA003IModule::begin() {
    if(this->pmsa003i->begin_I2C()) {
        this->addDatafield("Particles > 0.3um / 0.1L air", __CALLBACK__(StemmaQtPMSA003IModule::getParticles03um));
        this->addDatafield("Particles > 0.5um / 0.1L air", __CALLBACK__(StemmaQtPMSA003IModule::getParticles05um));
        this->addDatafield("Particles > 1.0um / 0.1L air", __CALLBACK__(StemmaQtPMSA003IModule::getParticles10um));
        this->addDatafield("Particles > 2.5um / 0.1L air", __CALLBACK__(StemmaQtPMSA003IModule::getParticles25um));
        this->addDatafield("Particles > 5.0um / 0.1L air", __CALLBACK__(StemmaQtPMSA003IModule::getParticles50um));
        this->addDatafield("Particles > 10um / 0.1L air", __CALLBACK__(StemmaQtPMSA003IModule::getParticles100um));
        return true;
    }
    return false;
}

bool StemmaQtPMSA003IModule::sample() {
    return this->pmsa003i->read(this->measurement);
}

std::string StemmaQtPMSA003IModule::getParticles03um() {
    return std::to_string(this->measurement->particles_03um);
}

std::string StemmaQtPMSA003IModule::getParticles05um() {
    return std::to_string(this->measurement->particles_05um);
}

std::string StemmaQtPMSA003IModule::getParticles10um() {
    return std::to_string(this->measurement->particles_10um);
}

std::string StemmaQtPMSA003IModule::getParticles25um() {
    return std::to_string(this->measurement->particles_25um);
}

std::string StemmaQtPMSA003IModule::getParticles50um() {
    return std::to_string(this->measurement->particles_50um);
}

std::string StemmaQtPMSA003IModule::getParticles100um() {
    return std::to_string(this->measurement->particles_100um);
}
