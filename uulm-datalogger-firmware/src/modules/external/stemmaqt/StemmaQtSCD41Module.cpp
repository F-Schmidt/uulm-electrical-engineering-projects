//
// Created by Falko Schmidt on 02.07.22.
//
#include <Arduino.h>
#include <sstream>
#include "modules/external/stemmaqt/StemmaQtSCD41Module.h"

StemmaQtSCD41Module::StemmaQtSCD41Module()
    : ExternalModule("StemmaQtSCD41Module",
                     "This module uses the SCD41 sensor to measure temperature, humidity and CO2-concentration.") {
    this->scd4x = new SensirionI2CScd4x();
    this->co2 = 0;
    this->temperature = 0.0;
    this->humidity = 0.0;
}


bool StemmaQtSCD41Module::begin() {
    Wire.begin();

    this->scd4x->begin(Wire);

    uint16_t error;

    error = this->scd4x->stopPeriodicMeasurement();
    if(error) {
        Serial.println("Failed to stop measurement. Failed to begin SCD4X sensor.");
        return false;
    }

    // Explicitly start the first measurement
    error = this->scd4x->startPeriodicMeasurement();
    if(error) {
        Serial.println("Failed to start measurement!");
        return false;
    }

    this->addDatafield("CO2 Concentration [ppm]", __CALLBACK__(StemmaQtSCD41Module::getCO2Concentration));
    this->addDatafield("Temperature [°C]", __CALLBACK__(StemmaQtSCD41Module::getTemperature));
    this->addDatafield("Humidity [%RH]", __CALLBACK__(StemmaQtSCD41Module::getHumidity));

    return true;
}

bool StemmaQtSCD41Module::sample() {
    uint16_t error = this->scd4x->readMeasurement(this->co2, this->temperature, this->humidity);
    if(error) {
        return false;
    } else {
        return true;
    }
}

std::string StemmaQtSCD41Module::getCO2Concentration() const {
    std::ostringstream os;
    os << this->co2;
    return os.str();
}

std::string StemmaQtSCD41Module::getTemperature() const {
    return std::to_string(this->temperature);
}

std::string StemmaQtSCD41Module::getHumidity() const {
    return std::to_string(this->humidity);
}
