//
// Created by Falko Schmidt on 28.05.22.
//

#include "modules/external/stemmaqt/StemmaQtLIS3MDLModule.h"

StemmaQtLIS3MDLModule::StemmaQtLIS3MDLModule()
        : ExternalModule("Stemma QT LIS3MDL Magnetometer Module",
                         "This module measures the magnetic field strength in all three dimensions.") {
    this->magnetometer = new Adafruit_LIS3MDL();
    this->magneticStrength = new Vector3D<float>(0, 0, 0);
}


bool StemmaQtLIS3MDLModule::begin() {
    if(!this->magnetometer->begin_I2C()) {
        return false;
    }
    this->magnetometer->setPerformanceMode(LIS3MDL_HIGHMODE);
    this->magnetometer->setOperationMode(LIS3MDL_SINGLEMODE);
    this->addDatafield("Magnetic field X (LIS3MDL) [uT]", __CALLBACK__(StemmaQtLIS3MDLModule::getMagnetFieldStrengthX));
    this->addDatafield("Magnetic field Y (LIS3MDL) [uT]", __CALLBACK__(StemmaQtLIS3MDLModule::getMagnetFieldStrengthY));
    this->addDatafield("Magnetic field Z (LIS3MDL) [uT]", __CALLBACK__(StemmaQtLIS3MDLModule::getMagnetFieldStrengthZ));

    return true;
}

bool StemmaQtLIS3MDLModule::sample() {
    this->magnetometer->read();
    this->magneticStrength->setX(this->magnetometer->x_gauss);
    this->magneticStrength->setY(this->magnetometer->y_gauss);
    this->magneticStrength->setZ(this->magnetometer->z_gauss);
    return true;
}

std::string StemmaQtLIS3MDLModule::getMagnetFieldStrengthX() {
    return std::to_string(this->magneticStrength->getX());
}

std::string StemmaQtLIS3MDLModule::getMagnetFieldStrengthY() {
    return std::to_string(this->magneticStrength->getY());
}

std::string StemmaQtLIS3MDLModule::getMagnetFieldStrengthZ() {
    return std::to_string(this->magneticStrength->getZ());
}
