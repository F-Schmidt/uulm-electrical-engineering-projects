//
// Created by Falko Alrik Schmidt on 03.02.23.
//

#include "modules/external/generic/GenericBME680Module.h"
#include "util/Unit.h"

// Constants
#define         GENERIC_BME680_MODULE_SEA_LEVEL_PRESSURE_HPA                    (1013.25f)

// Errno values for begin()-function
#define         GENERIC_BME680_MODULE_ERRNO_NO_SENSOR_FOUND                     (1u)
#define         GENERIC_BME680_MODULE_ERRNO_TEMPERATURE_OVERSAMPLING            (2u)
#define         GENERIC_BME680_MODULE_ERRNO_HUMIDITY_OVERSAMPLING               (3u)
#define         GENERIC_BME680_MODULE_ERRNO_PRESSURE_OVERSAMPLING               (4u)
#define         GENERIC_BME680_MODULE_ERRNO_IIR_FILTER_CONFIG                   (5u)
#define         GENERIC_BME680_MODULE_ERRNO_GAS_HEATER_CONFIG                   (6u)

// Errno values for sample()-function
#define         GENERIC_BME680_MODULE_ERRNO_GENERIC_SAMPLING_ERROR              (7u)

/**
 * Default constructor.
 * @param ssPin The slave select pin of the SPI bus, that the BME680 sensor is connected to.
 * @param name The name of this module.
 * @param description The description of this module.
 */
GenericBME680Module::GenericBME680Module(int8_t ssPin, const std::string &name, const std::string &description)
        : ExternalModule(name, description) {
    this->bme = new Adafruit_BME680(ssPin);
    this->temperature   = 0.0;
    this->humidity      = 0.0;
    this->pressure      = 0;
    this->gasResistance = 0;
    this->altitude      = 0.0;
}


/**
 * Inits the connection to the sensor and configures it.
 * @return true on success, false otherwise.
 */
bool GenericBME680Module::begin() {

    if(!this->bme->begin()) {
        this->setErrno(GENERIC_BME680_MODULE_ERRNO_NO_SENSOR_FOUND);
        return false;
    }

    if(!this->bme->setTemperatureOversampling(BME680_OS_8X)) {
        this->setErrno(GENERIC_BME680_MODULE_ERRNO_TEMPERATURE_OVERSAMPLING);
        return false;
    }

    if(!this->bme->setHumidityOversampling(BME680_OS_2X)) {
        this->setErrno(GENERIC_BME680_MODULE_ERRNO_HUMIDITY_OVERSAMPLING);
        return false;
    }

    if(!this->bme->setPressureOversampling(BME680_OS_4X)) {
        this->setErrno(GENERIC_BME680_MODULE_ERRNO_PRESSURE_OVERSAMPLING);
        return false;
    }

    if(!this->bme->setIIRFilterSize(BME680_FILTER_SIZE_3)) {
        this->setErrno(GENERIC_BME680_MODULE_ERRNO_IIR_FILTER_CONFIG);
        return false;
    }

    // Configure gas heater with temperature 320°C for 150ms
    if(!this->bme->setGasHeater(320, 150)) {
        this->setErrno(GENERIC_BME680_MODULE_ERRNO_GAS_HEATER_CONFIG);
        return false;
    }

    // Add data fields
    this->addDatafield("Temperature (BME680)",
                       Unit::CELSIUS,
                       &GenericBME680Module::getTemperature,
                       this,
                       3);

    this->addDatafield("Humidity (BME680)",
                       Unit::PERCENT,
                       &GenericBME680Module::getHumidity,
                       this,
                       3);

    this->addDatafield("Pressure (BME680)",
                       Unit::PASCAL,
                       &GenericBME680Module::getPressure,
                       this);

    this->addDatafield("Gas Resistance (BME680)",
                       Unit::of(Unit::KILO, "Ohm"),
                       &GenericBME680Module::getGasResistance,
                       this);

    this->addDatafield("Altitude (BME680)",
                       Unit::METER,
                       &GenericBME680Module::getAltitude,
                       this,
                       3);

    return true;
}


/**
 * Samples the sensor. All values will be read from the sensor and stored
 * into member variables.
 * @return
 */
bool GenericBME680Module::sample() {
    if(!this->bme->performReading()) {
        this->setErrno(GENERIC_BME680_MODULE_ERRNO_GENERIC_SAMPLING_ERROR);
        return false;
    }

    // Assign all values to this module.
    this->humidity      = this->bme->humidity;
    this->temperature   = this->bme->temperature;
    this->pressure      = this->bme->pressure;
    this->gasResistance = this->bme->gas_resistance;
    this->altitude      = this->bme->readAltitude(GENERIC_BME680_MODULE_SEA_LEVEL_PRESSURE_HPA);

    // Yay, no error!
    return true;
}


/**
 * This method indicates, that this module fully supports the Errno interface.
 * @return true.
 */
bool GenericBME680Module::supportsErrno() const {
    return true;
}


/**
 * Returns a string with description of the current error state of this module.
 * @return a string with a short error description.
 */
std::string GenericBME680Module::getErrorStr() const {
    switch (this->getErrno()) {
        case GENERIC_BME680_MODULE_ERRNO_NO_SENSOR_FOUND:
            return "Failed to find sensor of type 'BME680' (SPI).";
        case GENERIC_BME680_MODULE_ERRNO_TEMPERATURE_OVERSAMPLING:
            return "Failed to set temperature oversampling rate.";
        case GENERIC_BME680_MODULE_ERRNO_HUMIDITY_OVERSAMPLING:
            return "Failed to set humidity oversampling rate.";
        case GENERIC_BME680_MODULE_ERRNO_PRESSURE_OVERSAMPLING:
            return "Failed to set pressure oversampling rate.";
        case GENERIC_BME680_MODULE_ERRNO_IIR_FILTER_CONFIG:
            return "Failed to configure IIR filter.";
        case GENERIC_BME680_MODULE_ERRNO_GAS_HEATER_CONFIG:
            return "Failed to configure gas heater.";
        case GENERIC_BME680_MODULE_ERRNO_GENERIC_SAMPLING_ERROR:
            return "Failed to sample BME680 sensor.";
        default:
            return "Unknown error";
    }
}


/**
 * Returns the currently measured temperature.
 * @return the temperature read by BME680 sensor.
 */
float GenericBME680Module::getTemperature() const {
    return temperature;
}


/**
 * Returns the currently measured humidity.
 * @return the humidity read by BME680 sensor.
 */
float GenericBME680Module::getHumidity() const {
    return humidity;
}


/**
 * Returns the currently measured air pressure.
 * @return the air pressured read by BME680 sensor. Unit is PASCAL.
 */
uint32_t GenericBME680Module::getPressure() const {
    return pressure;
}


/**
 * Returns the currently measured gas resistance.
 * @return the gas resistance read by BME680 sensor. Unit is kOhm.
 */
uint32_t GenericBME680Module::getGasResistance() const {
    return gasResistance;
}


/**
 * Returns the current altitude.
 * @return the altitude calculated from the air pressure read by BME680 sensor.
 */
float GenericBME680Module::getAltitude() const {
    return altitude;
}
