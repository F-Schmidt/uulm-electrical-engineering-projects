//
// Created by Falko Alrik Schmidt on 22.01.23.
//

#include "modules/external/generic/GenericRTCModule.h"

GenericRTCModule::GenericRTCModule() : ExternalModule("RTC Module",
                                                      "This module stores and manages the current time.") {
}
