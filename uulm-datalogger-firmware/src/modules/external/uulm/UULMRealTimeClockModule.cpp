//
// Created by Falko Schmidt on 28.05.22.
//

#include "modules/external/uulm/UULMRealTimeClockModule.h"

#define                 ADJUST_HOUR_BUTTON_PIN                  (21)
#define                 ADJUST_MINUTE_BUTTON_PIN                (20)

UULMRealTimeClockModule::UULMRealTimeClockModule()
        : ExternalModule("DS3231 RTC Module", "This module stores and manages the current time.") {
    this->ds3231 = new RTC_DS3231();
    this->now = new DateTime(0, 0, 0, 0, 0, 0);
    this->temperature = 0.0;
}


bool UULMRealTimeClockModule::begin() {

    Wire.begin();

    if(this->ds3231->begin()) {
        this->addDatafield("Current Time [UTC]",
                           __CALLBACK__(UULMRealTimeClockModule::getTimestamp));
        this->addDatafield("DS3231 Internal Temperature [°C]",
                           __CALLBACK__(UULMRealTimeClockModule::getDS3231Temperature));
        return true;
    }
    return false;

    if(!this->ds3231->begin()) {
        printf("Failed to begin DS3231 clock module!");
        return false;
    }

    //attachInterrupt<>(ADJUST_HOUR_BUTTON_PIN,
    //                      std::bind(&UULMRealTimeClockModule::updateHours, this),
    //                      FALLING, nullptr);

    /*
    pinMode(ADJUST_HOUR_BUTTON_PIN, INPUT_PULLUP);
    pinMode(ADJUST_MINUTE_BUTTON_PIN, INPUT_PULLUP);

     */

    //attachInterrupt(digitalPinToInterrupt(ADJUST_HOUR_BUTTON_PIN),
    //                std::bind(&UULMRealTimeClockModule::updateHours, this),
    //                FALLING, nullptr);

    return true;
}


bool UULMRealTimeClockModule::sample() {
    this->now = new DateTime(this->ds3231->now());
    this->temperature = this->ds3231->getTemperature();
    return true;
}


std::string UULMRealTimeClockModule::getTimestamp() {

    // These fields can be used in the format of a date time:
    // hh   - the hour with a leading zero (00 to 23)
    // mm   - the minute with a leading zero (00 to 59)
    // ss   - the whole second with a leading zero where applicable (00 to 59)
    // YYYY - the year as four-digit number
    // YY   - the year as two-digit number (00-99)
    // MM   - the month as number with a leading zero (01-12)
    // MMM  - the abbreviated English month name ('Jan' to 'Dec')
    // DD   - the day as number with a leading zero (01 to 31)
    // DDD  - the abbreviated English day name ('Mon' to 'Sun')

    char format[] = "DD.MM.YYYY-hh:mm:ss";
    return this->now->toString(format);
}

void UULMRealTimeClockModule::updateHours() {
    DateTime time = this->ds3231->now();

    uint8_t adjustedHour = time.hour() + 1;
    if(time.hour() >= 24) {
        adjustedHour = 0;
    }

    DateTime adjusted(time.year(), time.month(), time.day(),
                      adjustedHour, time.minute(), time.second());
    this->ds3231->adjust(adjusted);
}


void UULMRealTimeClockModule::updateMinutes() {
    DateTime time = this->ds3231->now();

    uint8_t adjustedMinute = time.minute() + 1;
    if(time.hour() >= 60) {
        adjustedMinute = 0;
    }

    DateTime adjusted(time.year(), time.month(), time.day(),
                      time.hour(), adjustedMinute, time.second());
    this->ds3231->adjust(adjusted);
}


std::string UULMRealTimeClockModule::getDS3231Temperature() const {
    return std::to_string(this->temperature);
}

