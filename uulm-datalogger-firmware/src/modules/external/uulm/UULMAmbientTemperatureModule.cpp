//
// Created by Falko Schmidt on 28.05.22.
//

#include "modules/external/uulm/UULMAmbientTemperatureModule.h"

#define                 MCP3204_VREF                        (3300)
#define                 SPI_SS                              (5)
#define                 MCP3204_RESOLUTION                  (1 << 12)

#define                 THERMISTOR_00_PIN                   (22)
#define                 THERMISTOR_01_PIN                   (23)
#define                 THERMISTOR_02_PIN                   (24)
#define                 THERMISTOR_03_PIN                   (25)

UULMAmbientTemperatureModule::UULMAmbientTemperatureModule()
        : ExternalModule("Ambient Temperature Module",
                         "This module measures the outdoor temperature in the stratosphere using 10k Epoxy Resistors") {
    this->adc = new MCP3204(MCP3204_VREF, SPI_SS);
}


bool UULMAmbientTemperatureModule::begin() {
    // Start interface to communicate to ADC
    SPI.begin();

    // Set thermistor pins to be outputs
    pinMode(THERMISTOR_00_PIN, OUTPUT);
    pinMode(THERMISTOR_01_PIN, OUTPUT);
    pinMode(THERMISTOR_02_PIN, OUTPUT);
    pinMode(THERMISTOR_03_PIN, OUTPUT);

    // Drive all pins low to prevent any current flow
    digitalWrite(THERMISTOR_00_PIN, LOW);
    digitalWrite(THERMISTOR_01_PIN, LOW);
    digitalWrite(THERMISTOR_02_PIN, LOW);
    digitalWrite(THERMISTOR_03_PIN, LOW);

    this->addDatafield("Outdoor Temp (Thermistor #0) [°C]",
                       __CALLBACK__(UULMAmbientTemperatureModule::getTemperatureThermistor0));
    this->addDatafield("Outdoor Temp (Thermistor #1) [°C]",
                       __CALLBACK__(UULMAmbientTemperatureModule::getTemperatureThermistor1));
    this->addDatafield("Outdoor Temp (Thermistor #2) [°C]",
                       __CALLBACK__(UULMAmbientTemperatureModule::getTemperatureThermistor2));
    this->addDatafield("Outdoor Temp (Thermistor #3) [°C]",
                       __CALLBACK__(UULMAmbientTemperatureModule::getTemperatureThermistor3));

    return true;
}

bool UULMAmbientTemperatureModule::sample() {
    // Sample all thermistors
    this->adcValuesRaw[0] = this->sampleThermistor(THERMISTOR_00_PIN, MCP3204::Channel::SINGLE_0);
    this->adcValuesRaw[1] = this->sampleThermistor(THERMISTOR_01_PIN, MCP3204::Channel::SINGLE_1);
    this->adcValuesRaw[2] = this->sampleThermistor(THERMISTOR_02_PIN, MCP3204::Channel::SINGLE_2);
    this->adcValuesRaw[3] = this->sampleThermistor(THERMISTOR_03_PIN, MCP3204::Channel::SINGLE_3);
    return true;
}

double UULMAmbientTemperatureModule::getTemperatureThermistor(uint8_t channel) {
    if(channel > 3) {
        // Error, we will return 0
        return 0;
    }
    uint16_t rawValue = this->adcValuesRaw[channel];

    if(rawValue == 0) {
        // Prevent divide by zero error
        return -234.1;
    }
    // Calculate resistor value in kiloohms from raw analog value using a voltage divider
    double resistorKOhms = 10000 / ((double)(MCP3204_RESOLUTION) / rawValue - 1) / 1000;

    // Calculate the exact temperature value. This function was created using MATLAB with
    // the POLYFIT functionality.
    return (15.13 * pow(resistorKOhms, -0.432) + 305.9 * pow(resistorKOhms, -0.08183) - 234.1);
}

std::string UULMAmbientTemperatureModule::getTemperatureThermistor0() {
    return std::to_string(this->getTemperatureThermistor(0));
}

std::string UULMAmbientTemperatureModule::getTemperatureThermistor1() {
    return std::to_string(this->getTemperatureThermistor(1));
}

std::string UULMAmbientTemperatureModule::getTemperatureThermistor2() {
    return std::to_string(this->getTemperatureThermistor(2));
}

std::string UULMAmbientTemperatureModule::getTemperatureThermistor3() {
    return std::to_string(this->getTemperatureThermistor(3));
}

uint16_t UULMAmbientTemperatureModule::sampleThermistor(uint8_t pin, MCP3204::Channel channel) {
    // Activate thermistor by driving pin high
    digitalWrite(pin, HIGH);

    // Read value
    uint16_t analogValue = this->adc->read(channel);

    // Deactivate thermistor by driving pin low
    digitalWrite(pin, LOW);

    // The read value
    return analogValue;
}
