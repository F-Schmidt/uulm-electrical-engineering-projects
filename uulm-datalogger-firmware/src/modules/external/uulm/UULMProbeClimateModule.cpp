//
// Created by Falko Schmidt on 28.05.22.
//

#include "modules/external/uulm/UULMProbeClimateModule.h"


#define                 PROBE_UULM_CLIMATE_MODULE_BME280_CS     (6)
#define                 SEALEVEL_PRESSURE_HPA                   (1013.25)

UULMProbeClimateModule::UULMProbeClimateModule()
        : ExternalModule("Probe Climate Module",
                         "This module measures temperature, RH% and pressure in the probe.") {
    this->bme280 = new Adafruit_BME280(PROBE_UULM_CLIMATE_MODULE_BME280_CS);
    this->sht4x = new SHTSensor(SHTSensor::SHT4X);
    this->altitudeBme280 = 0.0;
    this->pressureBme280 = 0.0;
    this->temperatureBme280 = 0.0;
    this->humidityBme280 = 0.0;
}


bool UULMProbeClimateModule::begin() {

    bool hasError = false;

    // Start i2c interface
    Wire.begin();

    // Start spi interface
    SPI.begin();

    // Start communication with BME280 pressure sensor
    if(this->bme280->begin()) {
        // Sensor found, add datafields
        this->addDatafield("Altitude (BME280) [m]",
                           __CALLBACK__(UULMProbeClimateModule::getAltitudeBME280));
        this->addDatafield("Pressure (BME280) [hPa]",
                           __CALLBACK__(UULMProbeClimateModule::getPressureBME280));
        this->addDatafield("Temperature (BME280) [°C]",
                           __CALLBACK__(UULMProbeClimateModule::getTemperatureBME280));
        this->addDatafield("Humidity (BME280) [%]",
                           __CALLBACK__(UULMProbeClimateModule::getHumidityBME280));
    } else {
        // No sensor found, error
        hasError = true;
    }


    // Start communication with SHT4x sensor
    if(this->sht4x->init()) {
        // Success, SHT4x sensor is available
        this->addDatafield("Temperature (SHT4X) [°C]",
                           __CALLBACK__(UULMProbeClimateModule::getTemperatureSHT4X));
        this->addDatafield("Humidity (SHT4X) [%]",
                           __CALLBACK__(UULMProbeClimateModule::getHumiditySHT4X));

        // Set accuracy to the highest possible value
        if(!this->sht4x->setAccuracy(SHTSensor::SHT_ACCURACY_HIGH)) {
            // Failed to set accuracy
            hasError = true;
        }
    } else {
        // No sensor found, error
        hasError = true;
    }

    return !hasError;
}


bool UULMProbeClimateModule::sample() {
    this->altitudeBme280 = this->bme280->readAltitude(SEALEVEL_PRESSURE_HPA);
    this->humidityBme280 = this->bme280->readHumidity();
    this->pressureBme280 = this->bme280->readPressure();
    this->temperatureBme280 = this->bme280->readTemperature();
    return this->sht4x->readSample();
}


std::string UULMProbeClimateModule::getHumidityBME280() const {
    return std::to_string(this->humidityBme280);
}

std::string UULMProbeClimateModule::getTemperatureBME280() const {
    return std::to_string(this->temperatureBme280);
}

std::string UULMProbeClimateModule::getPressureBME280() const {
    return std::to_string(this->pressureBme280);
}

std::string UULMProbeClimateModule::getAltitudeBME280() const {
    return std::to_string(this->altitudeBme280);
}

std::string UULMProbeClimateModule::getHumiditySHT4X() {
    return std::to_string(this->sht4x->getHumidity());
}

std::string UULMProbeClimateModule::getTemperatureSHT4X() {
    return std::to_string(this->sht4x->getTemperature());
}
