//
// Created by Falko Alrik Schmidt on 15.03.23.
//

#include "modules/external/sgu-seminar-2022/SGUberryGasMeasurementModule.h"
#include "util/Unit.h"

SGUberryGasMeasurementModule::SGUberryGasMeasurementModule(uint8_t pinMQ4)
    : ExternalModule("SGUberry gas measurement unit (CO2 / Methane)",
                         "This module uses the SCD4x to measure CO2 and MQ-4 to measure methane.") {
    this->scd4x = new SensirionI2CScd4x();
    this->pinMQ4 = pinMQ4;
    this->co2PPM = 0;
    this->methanePPM = 0;
}

bool SGUberryGasMeasurementModule::begin() {

    pinMode(this->pinMQ4, INPUT);

    this->scd4x->begin(Wire);
    this->scd4x->startPeriodicMeasurement();

    this->addDatafield("Temperature (SCD4x)", Unit::CELSIUS,
                       &SGUberryGasMeasurementModule::getTemperature,
                       this);

    this->addDatafield("Humidity (SCD4x)", Unit::PERCENT,
                       &SGUberryGasMeasurementModule::getHumidity,
                       this);

    this->addDatafield("CO2 (SCD4x)", "PPM",
                       &SGUberryGasMeasurementModule::getCO2,
                       this);

    this->addDatafield("Methane", "PPM",
                       &SGUberryGasMeasurementModule::getMethane,
                       this);

    return true;
}

bool SGUberryGasMeasurementModule::sample() {
    this->scd4x->readMeasurement(this->co2PPM, this->temperature, this->humidity);

    //Calculate methane PPM
    int a3 = analogRead(pinMQ4); // get raw reading from sensor
    double v_o = a3 * 3.3 / 4096.0; // convert reading to volts
    double R_S = (3.3-v_o) * 1000.0 / v_o; // apply formula for getting RS
    double PPM = pow(R_S/945.0, -2.95) * 1000.0; //apply formula for getting PPM
    this->methanePPM = PPM;

    return true;
}

float SGUberryGasMeasurementModule::getTemperature() const {
    return this->temperature;
}

float SGUberryGasMeasurementModule::getHumidity() const {
    return this->humidity;
}

float SGUberryGasMeasurementModule::getCO2() const {
    return this->co2PPM;
}

double SGUberryGasMeasurementModule::getMethane() const {
    return this->methanePPM;
}




