//
// Created by Falko Alrik Schmidt on 02.02.23.
//

#include "modules/external/sgu-seminar-2022/SGUberryAltitudeModule.h"

SGUberryAltitudeModule::SGUberryAltitudeModule(int8_t ssPin)
        : GenericBME680Module(ssPin, "SGUberry altitude module") {
    // Nothing to do here.
}
