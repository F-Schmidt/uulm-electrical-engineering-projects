//
// Created by Falko Alrik Schmidt on 13.02.23.
//

#include "modules/external/sgu-seminar-2022/SGUberryRadiationModule.h"

#define             SGUBERRY_RADIATION_MODULE_SOFTWARE_INTERRUPT_PIN                    (23u)
#define             SGUBERRY_RADIATION_MODULE_RESET_PIN                                 (18u)

SGUberryRadiationModule::SGUberryRadiationModule() : ExternalModule("SGUberry radiation module",
        "This module measures the amount of ionized particles using multiple geiger counters.") {
    this->lastClickMicros = 0;
    this->swIrqCount = 0;
    this->portExpander = new Adafruit_MCP23X17();
    this->lastMeasurementMillis = 0;
    this->isPortExpanderWorking = false;
}


bool SGUberryRadiationModule::begin() {

    pinMode(SGUBERRY_RADIATION_MODULE_SOFTWARE_INTERRUPT_PIN, INPUT_PULLDOWN);

    // Software interrupts
    attachInterruptParam(SGUBERRY_RADIATION_MODULE_SOFTWARE_INTERRUPT_PIN,
                         SGUberryRadiationModule::irq_forwarder_function,
                         RISING,
                         this);

    // Hardware counter
    if(!this->portExpander->begin_I2C(0x26)) {
        this->isPortExpanderWorking = false;

        pinMode(SGUBERRY_RADIATION_MODULE_RESET_PIN, OUTPUT);
        digitalWrite(SGUBERRY_RADIATION_MODULE_RESET_PIN, HIGH);

        // Add hardware tick counter
        this->addDatafield("Geigercounter (HW-INT)", "CPM",
                           &SGUberryRadiationModule::getHardwareCPM,
                           this);

        // Add field for overall ticks
        this->addDatafield("Total clicks (HW)", "Clicks",
                           &SGUberryRadiationModule::getTotalHardwareClicks,
                           this);

    } else {
        this->isPortExpanderWorking = true;
    }


    // Add software tick counter
    this->addDatafield("Geigercounter (SW-INT)", "CPM",
                       &SGUberryRadiationModule::getSoftwareCPM,
                       this);

    // Add field for overall ticks
    this->addDatafield("Total clicks (SW)", "Clicks",
                       &SGUberryRadiationModule::getTotalSoftwareClicks,
                       this);

    return true;
}


bool SGUberryRadiationModule::sample() {

    // Get time difference
    unsigned long timeDifference = millis() - this->lastMeasurementMillis;

    // Calculate the factor for a minute
    double factor = 1000.0 / timeDifference * 60;

    this->softwareCPM = this->momentarySwCount * factor;

    if(this->isPortExpanderWorking) {
        this->momentaryHwCount = this->readHardwareCounters();
        this->totalHardwareClicks += this->momentaryHwCount;
        this->hardwareCPM = this->momentaryHwCount * factor;
        resetHardwareCounter();
    } else {
        this->momentaryHwCount = 0;
        this->totalHardwareClicks = 0;
        this->hardwareCPM = 0;
    }

    // Reset counters and update last measurement time
    this->lastMeasurementMillis = millis();
    this->momentarySwCount = 0;
    return true;
}


void SGUberryRadiationModule::irq_forwarder_function(void* pointer) {
    noInterrupts();
    static_cast<SGUberryRadiationModule*>(pointer)->countInterrupt();
    interrupts();
}


void SGUberryRadiationModule::countInterrupt() {
    if(micros() - this->lastClickMicros > 1500) {
        // Only add tick, if last tick was at least 1500us earlier.
        // This is a debounce method without using delay(...).
        this->totalSoftwareClicks++;
        this->momentarySwCount++;
        this->lastClickMicros = micros();
    }
}

double SGUberryRadiationModule::getSoftwareCPM() const {
    return this->softwareCPM;
}

double SGUberryRadiationModule::getHardwareCPM() const {
    return this->hardwareCPM;
}

unsigned long SGUberryRadiationModule::getTotalSoftwareClicks() const {
    return totalSoftwareClicks;
}

unsigned long SGUberryRadiationModule::getTotalHardwareClicks() const {
    return totalHardwareClicks;
}

uint16_t SGUberryRadiationModule::readHardwareCounters() {

    uint8_t a = this->portExpander->readGPIOA();
    a = reverseByte(a);

    uint8_t b = this->portExpander->readGPIOB();
    b = reverseByte(b);

    return a + (b << 8);
}


uint8_t SGUberryRadiationModule::reverseByte(uint8_t b) {
    b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
    b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
    b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
    return b;
}

void SGUberryRadiationModule::resetHardwareCounter() {
    digitalWrite(SGUBERRY_RADIATION_MODULE_RESET_PIN, LOW);
    delay(100);
    digitalWrite(SGUBERRY_RADIATION_MODULE_RESET_PIN, HIGH);
}
