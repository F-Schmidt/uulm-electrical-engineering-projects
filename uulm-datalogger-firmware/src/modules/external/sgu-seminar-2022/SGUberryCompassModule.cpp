//
// Created by Falko Alrik Schmidt on 14.01.23.
//

#include <Arduino.h>
#include "modules/external/sgu-seminar-2022/SGUberryCompassModule.h"
#include "util/Unit.h"

#define         DECLINATION_ANGLE_GERMANY           (-16.90)

SGUberryCompassModule::SGUberryCompassModule() : ExternalModule("SGUberry compass module",
"This module measures the magnetic field strength in all three dimensions using the sensor 'LIS3MDL'.") {
    this->magnetometer = new Adafruit_LIS3MDL();
    this->portExpander = new Adafruit_MCP23008();
    this->measurement = new Vector3D<float>(0, 0, 0);
}


bool SGUberryCompassModule::begin() {
    if(!this->magnetometer->begin_I2C()) {
        // Communication via i2c bus failed
        return false;
    }

    // Use the highest performance that the sensor can provide -> 270 uA current
    this->magnetometer->setPerformanceMode(LIS3MDL_ULTRAHIGHMODE);
    if(this->magnetometer->getPerformanceMode() != LIS3MDL_ULTRAHIGHMODE) {
        // There must be an error in bus communication
        return false;
    }

    // Use continuous mode
    this->magnetometer->setOperationMode(LIS3MDL_CONTINUOUSMODE);
    if(this->magnetometer->getOperationMode() != LIS3MDL_CONTINUOUSMODE) {
        // Could not set mode -> bus error in some kind
        return false;
    }

    // Set sensor sampling rate. We must have a new value every 1 second.
    // Also, the lowest frequency possible should be used.
    // According to the datasheet this is 1.25 Hz.
    this->magnetometer->setDataRate(LIS3MDL_DATARATE_1_25_HZ);
    if(this->magnetometer->getDataRate() != LIS3MDL_DATARATE_1_25_HZ) {
        // Again, some error on bus
        return false;
    }

    // Set the range that the sensor can measure. The better it fits, the
    // more accurate values we will get.
    // The magnetic field of the earth is about 0.25 and 0.65 G, so the lowest
    // range of up to 4 Gauss is more than enough.
    // (-> https://en.wikipedia.org/wiki/Earth%27s_magnetic_field#Intensity)
    this->magnetometer->setRange(LIS3MDL_RANGE_4_GAUSS);
    if(this->magnetometer->getRange() != LIS3MDL_RANGE_4_GAUSS) {
        // Again, some error on bus
        return false;
    }

    // TODO: eventuell lis3mdl.configInterrupt benötigt! einfach mal testen!

    // Now we can define some datafields.
    // Magnetic field strength in gauss
    this->addDatafield<Vector3D<float>>("Magnetic field X (LIS3MDL)",
            "Gauss",
            &Vector3D<float>::getX,
            this->measurement
    );

    this->addDatafield<Vector3D<float>>("Magnetic field Y (LIS3MDL)",
            "Gauss",
            &Vector3D<float>::getY,
            this->measurement
    );

    this->addDatafield<Vector3D<float>>("Magnetic field Z (LIS3MDL)",
            "Gauss",
            &Vector3D<float>::getZ,
            this->measurement
    );

    // Angle we are heading to (0 - 360°)
    this->addDatafield("Heading angle (LIS3MDL)",
            "°",
            &SGUberryCompassModule::getHeadingDegrees,
            this
    );

    // Init the port expander. If this does not work, then the module
    // still fulfills its main purpose: Measuring the magnetic field.
    if(!this->portExpander->begin()) {
        return false;
    }

    // Set port expander to all output pins
    for(uint8_t pin = 0; pin < 8; pin++) {
        this->portExpander->pinMode(pin, OUTPUT);
    }

    // Everything went well, yeah!
    return true;
}


bool SGUberryCompassModule::sample() {

    // Fetch new values
    this->magnetometer->read();

    // Update measurement variable
    this->measurement->setX(this->magnetometer->x_gauss);
    this->measurement->setY(this->magnetometer->y_gauss);
    this->measurement->setZ(this->magnetometer->z_gauss);

    // Display the current rotation
    this->updateLEDs();

    // No detectable errors
    return true;
}


//TODO WRONG!!
double SGUberryCompassModule::getHeadingDegrees() {
    double angle = atan2(this->measurement->getY(), this->measurement->getX());
    angle = angle * 180.0 / M_PI;
    angle += DECLINATION_ANGLE_GERMANY;

    // Fit angle to range of [0...360)
    if(angle < 0) {
        angle += 360;
    }

    return angle;
}

//TODO WRONG!!
enum SGUberryCompassModule::Direction SGUberryCompassModule::degreeToDirection() {
    // The angle is in range of 0...360
    double angle = this->getHeadingDegrees();

    // Convert the angle to directions.
    // Due to the fact, that 8 LEDs cover the complete 360°,
    // we will use snippets of 45° for each direction. (N, NW, W, ...).
    if(angle < 22.5) {
        return NORTH;
    } else if(angle < 67.5) {
        return NORTH_EAST;
    } else if(angle < 112.5) {
        return EAST;
    } else if(angle < 157.5) {
        return SOUTH_EAST;
    } else if(angle < 202.5) {
        return SOUTH;
    } else if(angle < 247.5) {
        return SOUTH_WEST;
    } else if(angle < 292.5) {
        return WEST;
    } else if(angle < 337.5) {
        return NORTH_WEST;
    } else {
        return NORTH;
    }
}

void SGUberryCompassModule::updateLEDs() {
    switch (this->degreeToDirection()) {
        case NORTH:         this->portExpander->writeGPIO(1 << 3);  break;
        case NORTH_EAST:    this->portExpander->writeGPIO(1 << 4);  break;
        case EAST:          this->portExpander->writeGPIO(1 << 5);  break;
        case SOUTH_EAST:    this->portExpander->writeGPIO(1 << 6);  break;
        case SOUTH:         this->portExpander->writeGPIO(1 << 7);  break;
        case SOUTH_WEST:    this->portExpander->writeGPIO(1 << 0);  break;
        case WEST:          this->portExpander->writeGPIO(1 << 1);  break;
        case NORTH_WEST:    this->portExpander->writeGPIO(1 << 2);  break;
        default:            this->portExpander->writeGPIO(0);       break; // Disable all LEDs
    }
}
