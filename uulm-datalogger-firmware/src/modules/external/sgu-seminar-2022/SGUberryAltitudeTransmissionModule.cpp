//
// Created by Falko Alrik Schmidt on 15.03.23.
//

#include "modules/external/sgu-seminar-2022/SGUberryAltitudeTransmissionModule.h"

#define             SGUBERRY_NRF24_RADIO_CE                 (6u)
#define             SGUBERRY_NRF24_RADIO_CS                 (7u)

SGUberryAltitudeTransmissionModule::SGUberryAltitudeTransmissionModule(int8_t ssPinBME680)
    : GenericBME680Module(ssPinBME680), AbstractDebugger("SGUberry nRF24L01 debugger"),
    AbstractHandler("SGUberry nRF24L01 handler") {
    this->isStarted = false;
    this->radio = new RF24(SGUBERRY_NRF24_RADIO_CE, SGUBERRY_NRF24_RADIO_CS);
}

bool SGUberryAltitudeTransmissionModule::begin() {
    if(this->isStarted) {
        return true;
    }

    if(!GenericBME680Module::begin()) {
        return false;
    }

    if(!radio->begin()) {
        return false;
    }

    this->radio->setPALevel(RF24_PA_MAX);
    this->radio->stopListening();

    // TODO INIT routine of nRF module

    this->isStarted = true;
    return true;
}


void SGUberryAltitudeTransmissionModule::handleDebug(const DebuggingRecord &record) {
    // Write all records!
    if(record.getMessage().empty()) {
        return;
    }

    this->radio->write(&record.getMessage(), record.getMessage().length());

    // TODO transmit string to ground station

}

void SGUberryAltitudeTransmissionModule::writeRecord(const LogRecord &log) {
    // In this case, we will ignore the log record and only get the current height.
    auto currentHeight = (uint32_t)(this->getAltitude() * 100);
    this->radio->write(&currentHeight, sizeof(uint32_t));

    //TODO send the height (4 bytes)
}

