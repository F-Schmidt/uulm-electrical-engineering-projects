//
// Created by Falko Alrik Schmidt on 15.03.23.
//

#include "modules/external/sgu-seminar-2022/SGUberryWeatherKnife2000.h"
#include "util/Unit.h"

#define         SGUBERRY_WEATHER_KNIFE_INTERRUPT_PIN                (15u)

SGUberryWeatherKnife2000::SGUberryWeatherKnife2000(int8_t thermoCS) : ExternalModule("SGUberry Weather Knife 2000",
    "This module uses different sensors to gather air temperature and humidity."){
    this->thermoCS = thermoCS;
    this->temperature = 0.0;
    this->relativeHumidity = 0;
    this->interruptCount = 0;
    this->lastMeasurement = millis();
    this->thermo = new Adafruit_MAX31865(this->thermoCS);
}


float SGUberryWeatherKnife2000::getTemperature() const {
    return this->temperature;
}


double SGUberryWeatherKnife2000::getRelativeHumidity() const {
    return this->relativeHumidity;
}


float SGUberryWeatherKnife2000::getAbsoluteHumidity() const {
    float T = this->getTemperature();
    double rh = this->getRelativeHumidity();

    return (float)((6.112 * exp((17.67*T)/(T+243.5))*rh*2.1674)/(273.15+T));
}



void SGUberryWeatherKnife2000::irq_forwarder_function(void* pointer) {
    static_cast<SGUberryWeatherKnife2000*>(pointer)->countInterrupt();
}


bool SGUberryWeatherKnife2000::begin() {

    attachInterruptParam(SGUBERRY_WEATHER_KNIFE_INTERRUPT_PIN,
                         SGUberryWeatherKnife2000::irq_forwarder_function,
                         FALLING,
                         this);

    if(!thermo->begin(MAX31865_3WIRE)) {
        return false;
    }

    uint8_t fault = this->thermo->readFault();
    if (fault) {
        Serial.print("Fault 0x"); Serial.println(fault, HEX);
        if (fault & MAX31865_FAULT_HIGHTHRESH) {
            Serial.println("RTD High Threshold");
        }
        if (fault & MAX31865_FAULT_LOWTHRESH) {
            Serial.println("RTD Low Threshold");
        }
        if (fault & MAX31865_FAULT_REFINLOW) {
            Serial.println("REFIN- > 0.85 x Bias");
        }
        if (fault & MAX31865_FAULT_REFINHIGH) {
            Serial.println("REFIN- < 0.85 x Bias - FORCE- open");
        }
        if (fault & MAX31865_FAULT_RTDINLOW) {
            Serial.println("RTDIN- < 0.85 x Bias - FORCE- open");
        }
        if (fault & MAX31865_FAULT_OVUV) {
            Serial.println("Under/Over voltage");
        }
        thermo->clearFault();
    }

    this->addDatafield("Temperature (MAX31865)", Unit::CELSIUS,
                       &SGUberryWeatherKnife2000::getTemperature,
                       this);

    this->addDatafield("Relative Humidity (HPP801A)", Unit::PERCENT,
                       &SGUberryWeatherKnife2000::getRelativeHumidity,
                       this);

    this->addDatafield("Absolute Humidity ", Unit::frac("g", "m^3"),
                       &SGUberryWeatherKnife2000::getAbsoluteHumidity,
                       this);

    this->addDatafield("Raw interrupt count", "CLICKS",
                       &SGUberryWeatherKnife2000::getInterruptCount,
                       this);

    return true;
}


bool SGUberryWeatherKnife2000::sample() {
    this->temperature = thermo->temperature(100.0, 402.0);

    // Calculate frequency
    float frequency = (float)(this->interruptCount) / (float)(millis() - this->lastMeasurement);
    frequency *= 1000;

    // Calculate humidity from frequency
    this->relativeHumidity = (-0.09 * frequency + 653.57) * 100;

    // Update the time of last measurement and reset counter
    this->interruptCount = 0;
    this->lastMeasurement = millis();

    return true;
}


void SGUberryWeatherKnife2000::countInterrupt() {
    interruptCount++;
}

uint32_t SGUberryWeatherKnife2000::getInterruptCount() const {
    return interruptCount;
}

