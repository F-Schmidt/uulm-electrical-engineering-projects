//
// Created by Falko Schmidt on 26.08.22.
//

#include "modules/external/mikrobus/MikroBusModule.h"

MikroBusModule::MikroBusModule(const std::string &name, const std::string &description, MikroBusSocket &socket)
        : ExternalModule(name, description), socket(socket) {

}
