//
// Created by Falko Schmidt on 21.08.22.
//

#include "modules/external/arduino-mkr/ArduinoMKRThermModule.h"
#include "MKRTHERM.h"
#include "util/Unit.h"

ArduinoMKRThermModule::ArduinoMKRThermModule() : ExternalModule("Arduino MKR THERM Shield Module",
                     "This module handles temperature measurement with the Arduino MKR THERM shield.") {
    this->outdoorSensor = &THERM;
    this->outdoorTemperature = 0.0;
    this->referenceTemperature = 0.0;
}


bool ArduinoMKRThermModule::begin() {

    if(!this->outdoorSensor->begin()) {
        return false;
    }

    this->addDatafield("Outdoor temperature (MKR-THERM)", Unit::CELSIUS,
                       &ArduinoMKRThermModule::getOutdoorTemperature, this);
    this->addDatafield("Reference temperature (MKR-THERM)", Unit::CELSIUS,
                       &ArduinoMKRThermModule::getReferenceTemperature, this);
    return true;
}


bool ArduinoMKRThermModule::sample() {
    this->outdoorTemperature = this->outdoorSensor->readTemperature();
    this->referenceTemperature = this->outdoorSensor->readReferenceTemperature();
    return true;
}

double ArduinoMKRThermModule::getOutdoorTemperature() const {
    return outdoorTemperature;
}

double ArduinoMKRThermModule::getReferenceTemperature() const {
    return referenceTemperature;
}
