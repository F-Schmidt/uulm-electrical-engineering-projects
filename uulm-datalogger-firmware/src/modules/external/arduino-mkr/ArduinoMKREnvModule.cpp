//
// Created by Falko Schmidt on 21.08.22.
//

#include "modules/external/arduino-mkr/ArduinoMKREnvModule.h"
#include "util/Unit.h"

ArduinoMKREnvModule::ArduinoMKREnvModule()
    : ExternalModule("Arduino MKR ENV shield module",
                     "This module handles temperature measurement with the Arduino MKR ENV shield.") {
    this->env = &ENV;
    this->temperature   = 0.0;
    this->humidity      = 0.0;
    this->pressure      = 0.0;
    this->illuminance   = 0.0;
    this->uva           = 0.0;
    this->uvb           = 0.0;
    this->uvIndex       = 0.0;
}


bool ArduinoMKREnvModule::begin() {
    if(!this->env->begin()) {
        return false;
    }

    this->addDatafield("Temperature (MKR-ENV)", Unit::CELSIUS,
                       &ArduinoMKREnvModule::getTemperature, this);

    this->addDatafield("Humidity (MKR-ENV)", "%RH",
                       &ArduinoMKREnvModule::getHumidity, this);

    this->addDatafield("Pressure (MKR-ENV)", Unit::of(Unit::KILO, Unit::PASCAL),
                       &ArduinoMKREnvModule::getPressure, this);

    this->addDatafield("Illuminance (MKR-ENV)", Unit::LUX,
                       &ArduinoMKREnvModule::getIlluminance, this);

    this->addDatafield("UVA (MKR-ENV)", "" /* No unit, just a factor */,
                       &ArduinoMKREnvModule::getUva, this);

    this->addDatafield("UVB (MKR-ENV)", "" /* No unit, just a factor */,
                       &ArduinoMKREnvModule::getUvb, this);

    this->addDatafield("UV-Index (MKR-ENV)", "" /* No unit, just a factor */,
                       &ArduinoMKREnvModule::getUvIndex, this);

    return true;
}


bool ArduinoMKREnvModule::sample() {
    this->temperature   = this->env->readTemperature();
    this->humidity      = this->env->readHumidity();
    this->pressure      = this->env->readPressure();
    this->illuminance   = this->env->readIlluminance();
    this->uva           = this->env->readUVA();
    this->uvb           = this->env->readUVB();
    this->uvIndex       = this->env->readUVIndex();
    return true;
}

double ArduinoMKREnvModule::getTemperature() const {
    return temperature;
}

double ArduinoMKREnvModule::getHumidity() const {
    return humidity;
}

double ArduinoMKREnvModule::getPressure() const {
    return pressure;
}

double ArduinoMKREnvModule::getIlluminance() const {
    return illuminance;
}

double ArduinoMKREnvModule::getUva() const {
    return uva;
}

double ArduinoMKREnvModule::getUvb() const {
    return uvb;
}

double ArduinoMKREnvModule::getUvIndex() const {
    return uvIndex;
}
