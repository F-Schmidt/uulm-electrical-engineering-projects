//
// Created by Falko Schmidt on 21.08.22.
//

#include "modules/external/arduino-mkr/ArduinoMKRImuModule.h"
#include "util/Unit.h"

ArduinoMKRImuModule::ArduinoMKRImuModule() : ExternalModule("Arduino MKR IMU Shield Module",
                     "This module handles IMU readings with the Arduino MKR IMU shield.") {
    // Object reference to control IMU chip
    this->imu = &IMU;

    // Accelerometer
    this->accelerationX = 0.0;
    this->accelerationY = 0.0;
    this->accelerationZ = 0.0;

    // Euler angles
    this->heading = 0.0;
    this->roll = 0.0;
    this->pitch = 0.0;

    // Gyroscope
    this->gyroscopeX = 0.0;
    this->gyroscopeY = 0.0;
    this->gyroscopeZ = 0.0;

    // Magnetic field of earth
    this->magneticFieldX = 0.0;
    this->magneticFieldY = 0.0;
    this->magneticFieldZ = 0.0;

    // On-board temperature
    this->temperature = 0.0;
}


bool ArduinoMKRImuModule::begin() {

    if(!this->imu->begin()) {
        return false;
    }

    // Accelerometer
    this->addDatafield("Acceleration X", "g",&ArduinoMKRImuModule::getAccelerationX, this);
    this->addDatafield("Acceleration Y", "g",&ArduinoMKRImuModule::getAccelerationY, this);
    this->addDatafield("Acceleration Z", "g",&ArduinoMKRImuModule::getAccelerationZ, this);

    // Euler angles
    this->addDatafield("Heading angle", Unit::RADIANT, &ArduinoMKRImuModule::getHeading, this);
    this->addDatafield("Roll angle", Unit::RADIANT, &ArduinoMKRImuModule::getRoll, this);
    this->addDatafield("Pitch angle", Unit::RADIANT, &ArduinoMKRImuModule::getPitch, this);

    // Gyroscope
    this->addDatafield("Gyroscope X", Unit::frac(Unit::RADIANT, Unit::SECOND),
                       &ArduinoMKRImuModule::getGyroscopeX, this);
    this->addDatafield("Gyroscope Y", Unit::frac(Unit::RADIANT, Unit::SECOND),
                       &ArduinoMKRImuModule::getGyroscopeY, this);
    this->addDatafield("Gyroscope Z", Unit::frac(Unit::RADIANT, Unit::SECOND),
                       &ArduinoMKRImuModule::getGyroscopeZ, this);

    // Magnetic field of earth
    this->addDatafield("Magnetic Field X", Unit::of(Unit::MICRO, Unit::TESLA),
                       &ArduinoMKRImuModule::getMagneticFieldX, this);
    this->addDatafield("Magnetic Field Y", Unit::of(Unit::MICRO, Unit::TESLA),
                       &ArduinoMKRImuModule::getMagneticFieldY, this);
    this->addDatafield("Magnetic Field Z", Unit::of(Unit::MICRO, Unit::TESLA),
                       &ArduinoMKRImuModule::getMagneticFieldZ, this);

    // Temperature
    this->addDatafield("MKR-IMU Temperature", Unit::CELSIUS,
                       &ArduinoMKRImuModule::getTemperature, this);

    return true;
}


bool ArduinoMKRImuModule::sample() {

    bool success = true;

    if(this->imu->accelerationAvailable()) {
        // Update all values related to acceleration
        if(!this->imu->readAcceleration(this->accelerationX, this->accelerationY, this->accelerationZ)) {
            // Sampling error
            success = false;
        }
    }

    if(this->imu->eulerAnglesAvailable()) {
        // Update heading, roll and pitch
        if(!this->imu->readEulerAngles(this->heading, this->roll, this->pitch)) {
            success = false;
        }
    }

    if(this->imu->gyroscopeAvailable()) {
        // Update all values of gyroscope
        if(!this->imu->readGyroscope(this->gyroscopeX, this->gyroscopeY, this->gyroscopeZ)) {
            success = false;
        }
    }

    if(this->imu->magneticFieldAvailable()) {
        // Update magnetic field values
        if(!this->imu->readMagneticField(this->magneticFieldX, this->magneticFieldY, this->magneticFieldZ)) {
            success = false;
        }
    }

    // And finally...we will read the IMU chip temperature
    this->temperature = this->imu->readTemperature();

    return success;
}

float ArduinoMKRImuModule::getAccelerationX() const {
    return accelerationX;
}

float ArduinoMKRImuModule::getAccelerationY() const {
    return accelerationY;
}

float ArduinoMKRImuModule::getAccelerationZ() const {
    return accelerationZ;
}

float ArduinoMKRImuModule::getHeading() const {
    return heading;
}

float ArduinoMKRImuModule::getRoll() const {
    return roll;
}

float ArduinoMKRImuModule::getPitch() const {
    return pitch;
}

float ArduinoMKRImuModule::getGyroscopeX() const {
    return gyroscopeX;
}

float ArduinoMKRImuModule::getGyroscopeY() const {
    return gyroscopeY;
}

float ArduinoMKRImuModule::getGyroscopeZ() const {
    return gyroscopeZ;
}

float ArduinoMKRImuModule::getMagneticFieldX() const {
    return magneticFieldX;
}

float ArduinoMKRImuModule::getMagneticFieldY() const {
    return magneticFieldY;
}

float ArduinoMKRImuModule::getMagneticFieldZ() const {
    return magneticFieldZ;
}

float ArduinoMKRImuModule::getTemperature() const {
    return temperature;
}
