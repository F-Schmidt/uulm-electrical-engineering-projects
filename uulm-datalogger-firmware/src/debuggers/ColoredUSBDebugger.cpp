//
// Created by Falko Schmidt on 01.09.22.
//

#include <sstream>
#include "Arduino.h"
#include "debuggers/ColoredUSBDebugger.h"
#include "util/ANSIColor.h"

ColoredUSBDebugger::ColoredUSBDebugger() : USBDebugger("USB-Debugger with ANSI Color-codes") {

}

void ColoredUSBDebugger::handleDebug(const DebuggingRecord &record) {
    std::stringstream ss;
    ss << '[';
    switch (record.getType()) {
        case INFO:      ss << ANSI::colorString(ANSI::TextColor::RESET,     "INFO"); break;
        case WARN:      ss << ANSI::colorString(ANSI::TextColor::YELLOW,    "WARN"); break;
        case ERROR:     ss << ANSI::colorString(ANSI::TextColor::RED,       "FAIL"); break;
        case SUCCESS:   ss << ANSI::colorString(ANSI::TextColor::GREEN,     " OK "); break;
        default: break;
    }
    ss << ']' << ' ';
    Serial.print(ss.str().c_str());
    Serial.println(record.getMessage().c_str());
}
