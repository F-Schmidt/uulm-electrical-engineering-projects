//
// Created by Falko Schmidt on 01.09.22.
//

#include "debuggers/AbstractDebugger.h"

AbstractDebugger::AbstractDebugger(const std::string &name) : Nameable(name) {
    // Nothing to do here
}
