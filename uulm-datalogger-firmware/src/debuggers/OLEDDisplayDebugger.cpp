//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#include "debuggers/OLEDDisplayDebugger.h"

OLEDDisplayDebugger::OLEDDisplayDebugger(uint8_t width, uint8_t height)
    : AbstractDebugger("OLED-Display Debugger"),
      AbstractStatusIndicator("OLED-Display status indicator", "") {
    this->oledDisplay = Adafruit_SSD1306(width, height);
    this->errors = 0;
    this->warnings = 0;
}

bool OLEDDisplayDebugger::begin() {
    this->oledDisplay.begin(SSD1306_SWITCHCAPVCC, 0x3C);

    this->oledDisplay.clearDisplay();

    // Write some beginning text
    this->oledDisplay.setTextColor(SSD1306_WHITE);

    this->oledDisplay.setCursor(0, 0);
    this->oledDisplay.setTextSize(1);
    this->oledDisplay.println("=====================");

    this->oledDisplay.setTextSize(2);
    this->oledDisplay.println("Tempberry");

    this->oledDisplay.setTextSize(1);
    this->oledDisplay.println("=====================");

    this->oledDisplay.println("Datalogger v1.0-Proto");
    this->oledDisplay.println("Ulm University, 2022");
    this->oledDisplay.println("Falko Schmidt");
    this->oledDisplay.display();

    delay(3000);

    return true;
}

void OLEDDisplayDebugger::handleDebug(const DebuggingRecord &record) {
    switch (record.getType()) {
        case WARN:  this->warnings++;   break;
        case ERROR: this->errors++;     break;
        default:    return;
    }
    /*if(record.getType() == DebuggingType::ERROR) {
        // We will only display errors
        this->oledDisplay.clearDisplay();
        this->oledDisplay.setCursor(0, 0);
        this->oledDisplay.setTextColor(SSD1306_WHITE);
        this->oledDisplay.setTextSize(1);
        this->oledDisplay.println(record.getMessage().c_str());
        this->oledDisplay.display();
        delay(1000);
    }*/
}

void OLEDDisplayDebugger::onFinish(AbstractStatusIndicator::ProgramState state) {
    if(state == INITIALIZATION) {
        // Init process done. Evaluate errors and warns.
        this->oledDisplay.clearDisplay();
        this->oledDisplay.setCursor(0, 0);
        this->oledDisplay.setTextColor(SSD1306_WHITE);
        this->oledDisplay.setTextSize(1);

        if(this->warnings == 0 && this->errors == 0) {
            this->oledDisplay.println("All systems running!");
        } else {
            this->oledDisplay.print("WARNINGS: "); this->oledDisplay.println(this->warnings);
            this->oledDisplay.print("ERRORS:   "); this->oledDisplay.println(this->errors);
        }
        this->oledDisplay.display();
    }
}

void OLEDDisplayDebugger::clear() {
    this->oledDisplay.clearDisplay();
}

void OLEDDisplayDebugger::onStart(AbstractStatusIndicator::ProgramState state) {

}

