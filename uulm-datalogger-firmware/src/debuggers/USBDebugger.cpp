//
// Created by Falko Schmidt on 01.09.22.
//

#include "Arduino.h"
#include "debuggers/USBDebugger.h"

bool USBDebugger::begin() {
    //TODO
    Serial.begin(115200);
    delay(1500);
    return true;
}


USBDebugger::USBDebugger(const std::string &name) : AbstractDebugger(name) {

}


void USBDebugger::handleDebug(const DebuggingRecord &record) {
    std::string prefix;
    switch (record.getType()) {
        case INFO:    prefix = "[INFO]"; break;
        case WARN:    prefix = "[WARN]"; break;
        case ERROR:   prefix = "[FAIL]"; break;
        case SUCCESS: prefix = "[ OK ]"; break;
        default: break;
    }
    Serial.print(prefix.c_str());
    Serial.println(record.getMessage().c_str());
}
