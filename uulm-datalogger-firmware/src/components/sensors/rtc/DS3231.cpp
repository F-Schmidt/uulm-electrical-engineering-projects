//
// Created by Falko Alrik Schmidt on 22.01.23.
//

#include "components/sensors/rtc/DS3231.h"

DS3231::DS3231() : AbstractSensor(SensorType::REAL_TIME_CLOCK) {
    this->currentTemperature = 0.0;
    this->currentDateTime = nullptr;
}


bool DS3231::begin() {
    // First, we must call the begin-function of class RTC_DS3231.
    // This will init and check the i2c connection to the device.
    if(!RTC_DS3231::begin()) {
        return false;
    }

    // If there was a power loss, then we will reconfigure the time to
    // the compile time and date of this program. Of course, this is
    // not the valid current time, but at least we have a known state of
    // the clock.
    if(this->lostPower()) {
        this->adjust(DateTime(F(__DATE__), F(__TIME__)));
    }

    // No need for the 32 kHz output signal
    this->disable32K();

    // No need for ANY alarms. These will not be used, so we will disable them
    // and - in case there was an alarm already fired - we will clear the alarms.
    // The DS3231 has two alarms: alarm #1 and alarm #2.
    this->disableAlarm(1);
    this->disableAlarm(2);

    this->clearAlarm(1);
    this->clearAlarm(2);

    return true;
}


bool DS3231::sample() {
    // Fetch the temperature and current date and store them into
    // variables of this object. It is necessary to sample the
    // sensor within this function, so we can not use the
    // getTemperature of Arduino RTC_DS3231, because it reads
    // the sensor registers.
    this->currentTemperature = this->getTemperature();
    this->currentDateTime = this->now();
    return true;
}


const DateTime &DS3231::getCurrentDateTime() const {
    return currentDateTime;
}


float DS3231::getCurrentTemperature() const {
    return this->currentTemperature;
}
