//
// Created by Falko Alrik Schmidt on 26.01.23.
//

#include "components/sensors/environmental/BME680_SPI.h"

BME680_SPI::BME680_SPI(int8_t cspin, SPIClass *theSpi) : AbstractSensor(SensorType::TEMPERATURE),
        Adafruit_BME680(cspin, theSpi) {

}
