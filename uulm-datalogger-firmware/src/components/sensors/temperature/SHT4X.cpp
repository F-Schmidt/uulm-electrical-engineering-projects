//
// Created by Falko Schmidt on 28.08.22.
//

#include <cmath>
#include <stdint-gcc.h>
#include "components/sensors/temperature/SHT4X.h"

double SHT4X::getTypicalTemperatureAccuracy(float temp) {
    switch (this->shtSensorSeries) {
        case SHT40:
            // FIGURE 8 in datasheet, SOLID graph
            if(temp < -40.0) {
                // Too cold
                return NAN;
            } else if(temp < 0.0) {
                return -0.01 * temp + 0.2;
            } else if(temp <= 65.0) {
                return 0.2;
            } else if(temp <= 125.0) {
                return 0.00666667 * temp - 0.233333;
            } else {
                //Too hot
                return NAN;
            }
        case SHT41:
            // FIGURE 9 in datasheet, SOLID graph
            if(temp < -40.0) {
                return NAN;
            } else if(temp < 0.0) {
                return -0.0025 * temp + 0.2;
            } else if(temp <= 65.0) {
                return 0.2;
            } else if(temp <= 125.0) {
                return 0.00444444 * temp - 0.0555556;
            } else {
                return NAN;
            }
        case SHT45:
            // FIGURE 10 in datasheet, SOLID graph
            if(temp < -40.0) {
                return NAN;
            } else if(temp < -25.0) {
                return 0.2;
            } else if(temp < 5.0) {
                return -0.00333333 * temp + 0.116667;
            } else if(temp <= 60.0) {
                return 0.1;
            } else if(temp <= 90.0) {
                return 0.00333333 * temp - 0.1;
            } else if(temp <= 125.0) {
                return 0.00571429 * temp - 0.314286;
            } else {
                return NAN;
            }
        default:
            // Unknown series
            return NAN;
    }
}

double SHT4X::getMaximumTemperatureAccuracy(float temp) {
    switch (this->shtSensorSeries) {
        case SHT40:
            // FIGURE 8 in datasheet, DASHED graph
            if (temp < -40.0) {
                // Too cold
                return NAN;
            } else if (temp < 0.0) {
                return -0.015 * temp + 0.4;
            } else if (temp <= 65.0) {
                return 0.4;
            } else if (temp <= 125.0) {
                return 0.01 * temp - 0.25;
            } else {
                //Too hot
                return NAN;
            }
        case SHT41:
            // FIGURE 9 in datasheet, DASHED graph
            if (temp < -4.0) {
                return NAN;
            } else if (temp < 0.0) {
                return -0.0025 * temp + 0.4;
            } else if (temp <= 65.0) {
                return 0.4;
            } else if (temp <= 125.0) {
                return 0.005 * temp + 0.075;
            } else {
                return NAN;
            }
        case SHT45:
            if (temp < -40.0) {
                return NAN;
            } else if (temp < 90.0) {
                return 0.3;
            } else if (temp <= 125.0) {
                return 0.00857143 * temp - 0.471429;
            } else {
                return NAN;
            }
        default:
            // Unknown series
            return NAN;
    }
}

double SHT4X::getHumidityAccuracy(float temp, float hum) {
    switch (this->shtSensorSeries) {
        case SHT40:
            // FIGURE 2 and FIGURE 5 in datasheet
            if(temp < 0.0 || temp > 80.0) {
                return NAN;
            }
            if(hum <= 10.0 || hum > 90.0) {
                return 3.0;
            } else if(hum <= 30.0 || hum > 70.0) {
                return 2.0;
            } else {
                return 1.8;
            }
        case SHT41:
            // FIGURE 3 and FIGURE 6 in datasheet
            if(temp < 0.0 || temp > 80.0) {
                return NAN;
            }
            if(hum <= 25.0 || hum > 75.0) {
                return 2.0;
            } else {
                return 1.8;
            }
        case SHT45:
            // FIGURE 4 and FIGURE 7 in datasheet
            if(temp < 0.0 || temp > 80.0) {
                return NAN;
            }
            if(hum >= 95.0) {
                return 1.75;
            } else if(temp >= 20.0 && temp <= 60.0 && hum >= 20.0 && hum <= 70.0) {
                return 1.0;
            } else {
                return 1.5;
            }
        default:
            return NAN;
    }
}

bool SHT4X::begin() {
    SensirionI2CSht4x::begin(Wire);

    uint32_t serialNumber;  // Needed to store the serial number
    uint16_t error = this->serialNumber(serialNumber);

    return !error;
}

SHT4X::SHT4X(SHT4X::SHT4XSensorSeries shtSensorSeries) :
    AbstractSensor(SensorType::TEMPERATURE | SensorType::HUMIDITY) {
    this->shtSensorSeries = shtSensorSeries;
    this->temperature = 0.0;
    this->humidity = 0.0;
}

bool SHT4X::sample() {
    uint16_t error = this->measureHighPrecision(this->temperature, this->humidity);
    if(error) {
        return false;
    }
    return true;
}


SHT40::SHT40() : SHT4X(SHT4XSensorSeries::SHT40) {}

SHT41::SHT41() : SHT4X(SHT4XSensorSeries::SHT41) {}

SHT45::SHT45() : SHT4X(SHT4XSensorSeries::SHT45) {}
