//
// Created by Falko Alrik Schmidt on 24.01.23.
//

#include "components/sensors/port-expanders/MCP23017.h"

MCP23017::MCP23017(uint8_t a2, uint8_t a1, uint8_t a0) {
    this->i2cAddress = 0x20;
    this->i2cAddress |= a2 ? (1 << 3) : 0;
    this->i2cAddress |= a1 ? (1 << 2) : 0;
    this->i2cAddress |= a0 ? (1 << 1) : 0;
}

bool MCP23017::begin(TwoWire *wire) {
    return Adafruit_MCP23XXX::begin_I2C(this->i2cAddress, wire);
}

