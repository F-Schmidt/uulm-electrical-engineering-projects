//
// Created by Falko Schmidt on 26.08.22.
//

#include "components/connectors/MikroBusSocket.h"

MikroBusSocket::MikroBusSocket(uint8_t analogPin, uint8_t resetPin, uint8_t cs, HardwareSPI *spi, uint8_t pwmPin,
                               uint8_t interruptPin, HardwareSerial *uart, HardwareI2C *wire) {
    this->analogPin = analogPin;
    this->resetPin = resetPin;
    this->cs = cs;
    this->spi = spi;
    this->pwmPin = pwmPin;
    this->interruptPin = interruptPin;
    this->uart = uart;
    this->wire = wire;
}

uint8_t MikroBusSocket::getAnalogPin() const {
    return analogPin;
}

uint8_t MikroBusSocket::getResetPin() const {
    return resetPin;
}

uint8_t MikroBusSocket::getCs() const {
    return cs;
}

HardwareSPI *MikroBusSocket::getSpi() const {
    return spi;
}

uint8_t MikroBusSocket::getPwmPin() const {
    return pwmPin;
}

uint8_t MikroBusSocket::getInterruptPin() const {
    return interruptPin;
}

HardwareSerial *MikroBusSocket::getUart() const {
    return uart;
}

HardwareI2C *MikroBusSocket::getWire() const {
    return wire;
}

