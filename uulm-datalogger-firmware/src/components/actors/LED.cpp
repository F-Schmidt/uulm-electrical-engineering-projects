//
// Created by Falko Schmidt on 07.09.22.
//

#include <Arduino.h>
#include "components/actors/LED.h"


LED::LED(uint8_t pin, bool isActiveHigh) : Nameable("LED"),
        Describable("A simple LED connected to any pin of a microcontroller.") {
    // Set values
    this->pin = pin;
    this->isActiveHigh = isActiveHigh;
}

void LED::on() const {
    digitalWrite(this->pin, this->isActiveHigh ? HIGH : LOW);
}

void LED::off() const {
    digitalWrite(this->pin, this->isActiveHigh ? LOW : HIGH);
}

void LED::toggle() const {
    digitalWrite(this->pin, (digitalRead(this->pin) ? LOW : HIGH));
}

bool LED::begin() {
    // Init pin to be OUTPUT.
    pinMode(pin, OUTPUT);

    // Initially turn off LED.
    this->off();

    // No detectable errors, so we will return 'true'.
    return true;
}
