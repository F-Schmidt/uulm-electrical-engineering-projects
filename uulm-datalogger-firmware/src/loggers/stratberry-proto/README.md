This directory contains the software for the Stratberry Prototype version created in March 2022.
See image below for reference:

![Stratberry-RP2040-PCB](Stratberry.png)

Created by Falko Schmidt, University of Ulm.