//
// Created by Falko Schmidt on 04.06.22.
//

#include "loggers/stratberry-proto/StratberryPrototypeDatalogger.h"
#include "modules/internal/rp2040/RP2040UptimeModule.h"
#include "modules/internal/rp2040/RP2040CoreTemperatureModule.h"
#include "modules/external/uulm/UULMAmbientTemperatureModule.h"
#include "handlers/SimpleSDCardHandler.h"
#include "handlers/ColoredUSBHandler.h"
#include "status-indicators/StratberryPrototypeDataLoggerStatusIndicator.h"
#include "modules/external/uulm/UULMProbeClimateModule.h"
#include "modules/external/uulm/UULMRealTimeClockModule.h"
#include "util/datafields/GenericDatafield.h"
#include "debuggers/ColoredUSBDebugger.h"


StratberryPrototypeDatalogger::StratberryPrototypeDatalogger()
    : AbstractDatalogger("Stratberry Datalogger (Prototype)",
                         "The first working example of an RP2040 based datalogger.") {
    // Add modules
    // Internal
    this->addModule(new RP2040UptimeModule());
    this->addModule(new RP2040CoreTemperatureModule());

    // External
    this->addModule(new UULMAmbientTemperatureModule());
    this->addModule(new UULMProbeClimateModule());
    this->addModule(new UULMRealTimeClockModule());

    // Add handlers
    this->addHandler(new SimpleSDCardHandler(7));
    this->addHandler(new SimpleSDCardHandler(8));
    this->addHandler(new ColoredUSBHandler());

    // Add status indicators
    this->addStatusIndicator(new StratberryPrototypeDataLoggerStatusIndicator());

    // Add debuggers
    this->addDebugger(new ColoredUSBDebugger());

    // TODO ERROR INDICATORS??
}