// ====================================================================================================
// Created by Falko Schmidt on 04.06.22.
// ====================================================================================================
// Imports related to 'Arduino'-Framework
#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>
// Imports related to C/C++ STL
#include <algorithm>
#include <sstream>
// Imports related to this datalogger framework
#include "loggers/AbstractDatalogger.h"
#include "modules/AbstractModule.h"
#include "handlers/AbstractHandler.h"
// ====================================================================================================

using namespace arduino;

/**
 * Default constructor.
 * @param name the name of this module
 * @param description a short description of what the module does
 */
AbstractDatalogger::AbstractDatalogger(const std::string &name, const std::string &description)
    : Nameable(name), Describable(description) {
        this->modules           = std::list<AbstractModule*>();
        this->handlers          = std::vector<AbstractHandler*>();
        this->statusIndicators  = std::vector<AbstractStatusIndicator*>();
        this->debuggers         = std::vector<AbstractDebugger*>();
        this->delayMillis       = 2000;
        this->columnSeparator   = ';';
}



// **************************************************************************************************
// **************************************************************************************************
//
// A D D - F U N C T I O N S
//
// **************************************************************************************************
// **************************************************************************************************

/**
 * Adds a component to a list. This method will only be used within this
 * class.
 * @tparam T the type of the object that will be added to a list
 * @param list the list which this component will be added to
 * @param component the component which will be added to the list
 * @return true, if the component was added, false if the component is already inserted
 * into the list or if the component is the null pointer.
 */
template<typename T>
bool AbstractDatalogger::addComponent(std::vector<T*>& list, T* component) {

    if(component == nullptr) {
        return false;
    }

    if(std::find(list.begin(), list.end(), component) != list.end()) {
        // Already contained in the list, we will not add a duplicate
        return false;
    }

    list.push_back(component);
    return true;

}

// TODO change all to lists???
template<typename T>
bool AbstractDatalogger::addComponent(std::list<T*>& list, T* component) {

    if(component == nullptr) {
        return false;
    }

    if(std::find(list.begin(), list.end(), component) != list.end()) {
        // Already contained in the list, we will not add a duplicate
        return false;
    }

    list.push_back(component);
    return true;

}


/**
 * Adds a status indicator to the status indicator list.
 * @param statusIndicator the status indicator that will be added
 * @return true, if the component was added, false if the component is already inserted
 * into the list or if the component is the null pointer.
 */
bool AbstractDatalogger::addStatusIndicator(AbstractStatusIndicator *statusIndicator) {
    return this->addComponent(this->statusIndicators, statusIndicator);
}


/**
 * Adds a module to the module list.
 * @param module the module that will be added
 * @return true, if the component was added, false if the component is already inserted
 * into the list or if the component is the null pointer.
 */
bool AbstractDatalogger::addModule(AbstractModule *module) {
    return this->addComponent(this->modules, module);
}


/**
 * Adds a handler to the handler list.
 * @param handler the handler that will be added
 * @return true, if the component was added, false if the component is already inserted
 * into the list or if the component is the null pointer.
 */
bool AbstractDatalogger::addHandler(AbstractHandler* handler) {
    return this->addComponent(this->handlers, handler);
}


/**
 * Adds a debugger to the debugger list.
 * @param debugger the debugger that will be added
 * @return true, if the component was added, false if the component is already inserted
 * into the list or if the component is the null pointer.
 */
bool AbstractDatalogger::addDebugger(AbstractDebugger *debugger) {
    return this->addComponent(this->debuggers, debugger);
}



// **************************************************************************************************
// **************************************************************************************************
//
// P U B L I S H I N G - F U N C T I O N S
//
// **************************************************************************************************
// **************************************************************************************************

/**
 * Publishes a log record to all handlers.
 * @param log the log record.
 */
void AbstractDatalogger::publishLogRecord(const LogRecord &log) {

    if(this->handlers.empty()) {
        // There are no handlers, so we can directly return and continue
        // our normal programming cycle.
        return;
    }

    if(log.getMessage().empty()) {
        // There is no logging message
        return;
    }

    for(auto handler : this->handlers) {
        // Check if handler is not null. If this is the case,
        // then we will send the data to handler.
        if(handler != nullptr) {
            handler->writeRecord(log);
        }
    }

}


/**
 * Publishes a program state status to all status indicators.
 * @param ps the program state
 * @param start true, if the current state will be started, false if the current state was completed.
 */
void AbstractDatalogger::publishProgramStatus(AbstractStatusIndicator::ProgramState ps, bool start) {

    if(this->statusIndicators.empty()) {
        // No status indicators, so we can directly return and continue
        // with our normal program execution
        return;
    }

    for(auto statusIndicator : this->statusIndicators) {

        if(statusIndicator == nullptr) {
            // Just make sure to prevent errors within this method!
            // If there is any invalid status indicator, then we will just
            // skip it.
            continue;
        }

        // Distinguish between onStart and onFinish methods.
        if(start) {
            statusIndicator->onStart(ps);
        } else {
            statusIndicator->onFinish(ps);
        }
    }

}


/**
 * Publishes a program status started message to all program status indicators.
 * @param ps the program state which was started.
 */
void AbstractDatalogger::publishProgramStatusStart(AbstractStatusIndicator::ProgramState ps) {
    this->publishProgramStatus(ps, true);
}


/**
 * Publishes a program status finished message to all program status indicators.
 * @param ps the program state which is completed.
 */
void AbstractDatalogger::publishProgramStatusFinish(AbstractStatusIndicator::ProgramState ps) {
    this->publishProgramStatus(ps, false);
}


/**
 * Publishes a debugging record to all debuggers in the same order as they were added
 * to the list (first come, first serve). A debugging record will only be published,
 * if the message (string) is not empty.
 * @param debug the debugging record.
 */
void AbstractDatalogger::publishDebuggingRecord(const DebuggingRecord &debug) {

    if(this->debuggers.empty()) {
        return;
    }

    if(debug.getMessage().empty()) {
        return;
    }

    for(auto debugger : this->debuggers) {
        if(debugger != nullptr) {
            debugger->handleDebug(debug);
        }
    }

}



// **************************************************************************************************
// **************************************************************************************************
//
// I N I T - M E T H O D S
//
// **************************************************************************************************
// **************************************************************************************************

/**
 * Inits all status indicators.
 * @return false, if there was at least one error, true if everything went well.
 */
bool AbstractDatalogger::initStatusIndicators() {
    if(this->statusIndicators.empty()) {
        // No indicators -> no errors. We can just return true
        return true;
    }

    bool success = true;

    for(auto s : this->statusIndicators) {

        enum DebuggingType type;
        std::stringstream ss;

        if(s->begin()) {
            ss << "Started status indicator '" << s->getName() << "'.";
            type = SUCCESS;
        } else {
            ss << "Error wile starting status indicator '" << s->getName() << "'.";
            type = ERROR;
            success = false;
        }

        this->publishDebuggingRecord(DebuggingRecord(type, ss.str(), nullptr));
    }

    return success;
}


/**
 * Inits all debuggers. Please note: If there is an error, then there is no possibility
 * to write a debugging record. Instead, the built-in LED will light up, if there is any
 * error regarding the initialization process of the debuggers.
 * @return false, if there was at least one error, true if everything went well.
 */
bool AbstractDatalogger::initDebuggers() {
   if(this->debuggers.empty()) {
       return true;
   }

   bool success = true;

   for(auto d : this->debuggers) {
       if(!d->begin()) {
           // Activate on-board LED
           digitalWrite(LED_BUILTIN, HIGH);
           success = false;
       }
   }

   return success;
}


/**
 * Inits all handlers that are setup in this datalogger.
 * @return false, if there was at least one error, true if everything went well.
 */
bool AbstractDatalogger::initHandlers() {
    if(this->handlers.empty()) {
        return true;
    }

    bool success = true;

    for(auto h : this->handlers) {

        enum DebuggingType type;
        std::stringstream ss;

        if(h->begin()) {
            ss << "Started handler '" << h->getName() << "'.";
            type = SUCCESS;
        } else {
            ss << "Error while starting handler '" + h->getName() << "'.";
            type = ERROR;
            success = false;
        }

        this->publishDebuggingRecord(DebuggingRecord(type, ss.str(), nullptr));

        // This delay MIGHT be needed to stabilize voltage level. This will not apply
        // to all microcontrollers, but since the overall startup time does not really
        // matter, we will just add this delay for all handlers and all microcontrollers.
        delay(1000);

    }

    return success;
}


/**
 * This function will init all modules of this datalogger. Only modules that are
 * enabled, will be started.
 * @return false, if there was at least one error, true if everything went well.
 */
bool AbstractDatalogger::initModules() {
    if(this->modules.empty()) {
        return true;
    }

    bool success = true;
    for(auto m : this->modules) {

        enum DebuggingType type;
        std::stringstream ss;

        if(m->isEnabled()) {
            if (m->begin()) {
                ss << "Started module '" << m->getName() << "'.";
                type = SUCCESS;
            } else {
                ss << "Error while starting module '" << m->getName() << "'.";
                type = ERROR;
                success = false;
            }
        } else {
            // Module is not enabled -> Remove from list
            this->modules.remove(m);

            ss << "Module '" << m->getName() << "' is not enabled and will be ignored.";
            type = WARN;
        }

        this->publishDebuggingRecord(DebuggingRecord(type, ss.str(), m));

    }

    return success;
}



// **************************************************************************************************
// **************************************************************************************************
//
// B E G I N   /   R U N   R O U T I N E S
//
// **************************************************************************************************
// **************************************************************************************************

/**
 * This function MUST be called before any logging can be done!
 * This function will start all needed interfaces (USB, UART, I2C, SPI,...), init all components
 * of this datalogger (modules, debuggers, handlers,...). If there are some debuggers available,
 * then some information about the hardware system and software (C/C++) will be published.
 * @return true, if everything succeeded, false, if there is at least one error.
 */
bool AbstractDatalogger::begin() {

    bool hasError = false;

    // ================================================================================================
    // INIT ON-BOARD LED
    // ================================================================================================
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);

    // ================================================================================================
    // START DEBUGGERS
    // ================================================================================================
    hasError |= this->initDebuggers();

    // ================================================================================================
    // ENABLE INTERFACES
    // ================================================================================================
    // Start USB connection and let it settle
    Serial.begin(115200);
#if defined(MKR1310_LOGGER_PROTOTYPE)
    while(!Serial) {
            delay(10);
        }
#endif
    delay(1000);

    std::stringstream loggerInformation;
    loggerInformation << "============================== Running datalogger ==============================";
    this->publishDebuggingRecord(
            DebuggingRecord(SUCCESS, loggerInformation.str(), nullptr)
    );
    loggerInformation.str("");
    loggerInformation.clear();
    loggerInformation << "Name: " << this->getName();
    this->publishDebuggingRecord(
            DebuggingRecord(SUCCESS, loggerInformation.str(), nullptr)
    );
    loggerInformation.str("");
    loggerInformation.clear();
    loggerInformation << "Description: " << this->getDescription();
    this->publishDebuggingRecord(
            DebuggingRecord(SUCCESS, loggerInformation.str(), nullptr)
    );
    loggerInformation.str("");
    loggerInformation.clear();
    loggerInformation << "================================================================================";
    this->publishDebuggingRecord(
            DebuggingRecord(SUCCESS, loggerInformation.str(), nullptr)
    );

    // Initial messages
    //std::stringstream  ss;
    //ss << "Starting logger '" << this->getName() << "'.\nDescription: " << this->getDescription();
    //Serial.println(ss.str().c_str());

    this->publishDebuggingRecord(DebuggingRecord(SUCCESS,
                                                 "Activated USB interface", nullptr));

    // Start i2c0 bussystem
    Wire.begin();
    this->publishDebuggingRecord(DebuggingRecord(SUCCESS,
                                                 "Activated i2c0 interface.", nullptr));

    // Start spi0 bussystem
    SPI.begin();
    this->publishDebuggingRecord(DebuggingRecord(SUCCESS,
                                                 "Activated spi0 interface.", nullptr));


    // ================================================================================================
    // START PROGRAM STATUS INDICATORS
    // ================================================================================================
    hasError |= this->initStatusIndicators();
    this->publishProgramStatusStart(AbstractStatusIndicator::INITIALIZATION);

    // ================================================================================================
    // START MODULES
    // ================================================================================================
    hasError |= this->initModules();

    // ================================================================================================
    // START HANDLERS
    // ================================================================================================
    hasError |= this->initHandlers();
    this->publishProgramStatusFinish(AbstractStatusIndicator::INITIALIZATION);

    // Check if there was an error
    return !hasError;
}


/**
 * Logging cycle. This function will run forever, sample the sensors and store the data.
 */
[[noreturn]] void AbstractDatalogger::run() {
    // ----------------------------------------------------------------------------------------------
    // Create and write HEADER line to all handlers
    // ----------------------------------------------------------------------------------------------
    std::stringstream header;
    for(auto m : this->modules) {
        for (auto f : m->getDatafields()) {
            // Iterate through all datafields in all modules
            header << f->getHeader() << this->columnSeparator;
        }
    }

    // Create string from string stream and remove last char (column-separator)
    std::string headerString = header.str();
    headerString.pop_back();

    // Send string to all handlers
    this->publishLogRecord(LogRecord(HEADER, headerString));


    // **********************************************************************************************
    // Main logging routine
    // **********************************************************************************************
    while (true) {

        // ------------------------------------------------------------------------------------------
        // Step 0: Fetch current time
        // ------------------------------------------------------------------------------------------
        unsigned long startTime = millis();


        // ------------------------------------------------------------------------------------------
        // Step 1: Sample all modules
        // ------------------------------------------------------------------------------------------
        this->publishProgramStatusStart(AbstractStatusIndicator::SAMPLING);

        for(auto m : this->modules) {
            if(!m->sample()) {
                // Sampling error, this will be forwarded to debuggers
                std::stringstream ss;
                ss << "Error while sampling module " << m->getName() << '.';
                if(m->hasError()) {
                    // In this case the module has an overwritten function and
                    // therefore supports errno.
                    ss << "Error-Code:" << m->getErrno() << ", ";
                    ss << "Error-Msg:" << m->getErrorStr();

                    // Now the error is saved and recorded, it will be deleted
                    m->clearError();
                }

                // publish the error message
                this->publishDebuggingRecord(DebuggingRecord(ERROR, ss.str(), m));
            }
        }

        this->publishProgramStatusFinish(AbstractStatusIndicator::SAMPLING);


        // ------------------------------------------------------------------------------------------
        // Step 2: Fetch sampled data and create string
        // ------------------------------------------------------------------------------------------
        this->publishProgramStatusStart(AbstractStatusIndicator::WRITING_DATA);

        std::stringstream dataStream;
        for(auto m : this->modules) {
            for (auto f : m->getDatafields()) {
                // Iterate through all datafields in all modules
                dataStream << f->getFunctionResult() << this->columnSeparator;
            }
        }
        std::string data = dataStream.str();
        data.pop_back();
        this->publishLogRecord(LogRecord(DATA_ENTITY, data));

        this->publishProgramStatusFinish(AbstractStatusIndicator::WRITING_DATA);


        // ------------------------------------------------------------------------------------------
        // Step 3: Delay and then repeat the logging cycle
        // ------------------------------------------------------------------------------------------
        long long delayTime = millis() + this->delayMillis - startTime;
        if(delayTime > 0) {
            // Only wait, if sampling and storing data takes
            // less time than our logging cycle delay.
            delay((unsigned long) delayTime);
        }
    }
}
