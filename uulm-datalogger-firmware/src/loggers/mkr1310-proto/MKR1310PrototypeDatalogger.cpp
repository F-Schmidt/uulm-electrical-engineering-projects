//
// Created by Falko Schmidt on 21.08.22.
//

#include <Arduino_PMIC.h>
#include "loggers/mkr1310-proto/MKR1310PrototypeDatalogger.h"
#include "modules/internal/arduino-mkr1310/ArduinoMKR1310UptimeModule.h"
#include "modules/internal/arduino-mkr1310/ArduinoMKR1310CoreTemperatureModule.h"
#include "modules/external/arduino-mkr/ArduinoMKREnvModule.h"
#include "modules/external/arduino-mkr/ArduinoMKRImuModule.h"
#include "modules/external/arduino-mkr/ArduinoMKRThermModule.h"
#include "handlers/SimpleSDCardHandler.h"
#include "handlers/ColoredUSBHandler.h"
#include "debuggers/ColoredUSBDebugger.h"
#include "handlers/ArduinoMKR1310LoRaWANHandler.h"

MKR1310PrototypeDatalogger::MKR1310PrototypeDatalogger()
    : AbstractDatalogger("Arduino MKR1310 Datalogger (Prototype)",
                         "The first working example of an Arduino MKR1310 based datalogger.") {

    // Configure BQ24195
    MKR1310PrototypeDatalogger::configureBatteryManagement();

    // Override fields
    this->delayMillis = 1000;
    this->columnSeparator = ';';

    // Add modules
    // Internal
    this->addModule(new ArduinoMKR1310UptimeModule());
    this->addModule(new ArduinoMKR1310CoreTemperatureModule());

    // External
    this->addModule(new ArduinoMKREnvModule());
    this->addModule(new ArduinoMKRImuModule());
    this->addModule(new ArduinoMKRThermModule());

    // Add handlers
    this->addHandler(new SimpleSDCardHandler(4));
    this->addHandler(new ColoredUSBHandler(this->columnSeparator));
    this->addHandler(new ArduinoMKR1310LoRaWANHandler());

    // Add debuggers
    this->addDebugger(new ColoredUSBDebugger());

}


bool MKR1310PrototypeDatalogger::configureBatteryManagement() {

    // Check communication
    if(!PMIC.begin()) {
        Serial.println("[FAIL] Failed to connect to built-in BQ24195 battery controller.");
        return false;
    }

    // Use battery for powering and not USB (self-powered configuration)
    if(!PMIC.enableBoostMode()) {
        Serial.println("[FAIL] Failed to enable boost mode (use battery).");
        return false;
    }

    // Maximum input current should be 2A
    if(!PMIC.setInputCurrentLimit(2.0)) {
        Serial.println("[FAIL] Failed to set maximum input current limit to 2.0A.");
        return false;
    }

    // Minimum input voltage
    if(!PMIC.setInputVoltageLimit(3.88)) {
        Serial.println("[FAIL] Failed to set input voltage limit to 3.88V.");
        return false;
    }

    // Minimum MKR1310 system voltage
    if(!PMIC.setMinimumSystemVoltage(3.5)) {
        Serial.println("[FAIL] Failed to set minimum system voltage to 3.5V.");
        return false;
    }

    // Lithium-Ion battery charge voltage
    if (!PMIC.setChargeVoltage(4.2)) {
        Serial.println("[FAIL] Failed to set charge voltage to 4.2V.");
        return false;
    }

    // Lithium-Ion battery charge current
    if (!PMIC.setChargeCurrent(0.5)) {
        Serial.println("[FAIL] Failed to set charge current to 500mA.");
        return false;
    }

    return true;
}
