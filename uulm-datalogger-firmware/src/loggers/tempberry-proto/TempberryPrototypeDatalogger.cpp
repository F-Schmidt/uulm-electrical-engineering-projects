//
// Created by Falko Schmidt on 01.09.22.
//

#include "loggers/tempberry-proto/TempberryPrototypeDatalogger.h"
#include "modules/internal/rp2040/RP2040UptimeModule.h"
#include "modules/internal/rp2040/RP2040CoreTemperatureModule.h"
#include "handlers/ColoredUSBHandler.h"
#include "handlers/SimpleSDCardHandler.h"
#include "debuggers/ColoredUSBDebugger.h"
#include "modules/external/tempberry/TempberryDS18B20Module.h"
#include "modules/external/tempberry/TempberryDHT11Module.h"
#include "modules/external/tempberry/TempberrySHTC3Module.h"
#include "modules/external/tempberry/TempberrySHT4XModule.h"
#include "modules/external/tempberry/TempberryBMP280Module.h"
#include "modules/external/tempberry/TempberryBME680Module.h"
#include "modules/external/tempberry/TempberryMax31855Module.h"
#include "modules/external/tempberry/TempberryMCP3201Module.h"
#include "debuggers/OLEDDisplayDebugger.h"

#define             TEMPBERRY_UPPER_SD_CARD_CS_PIN                  (0u)
#define             TEMPBERRY_LOWER_SD_CARD_CS_PIN                  (1u)

#define             TEMPBERRY_ONE_WIRE_PIN                          (22u)
#define             TEMPBERRY_DHT11_DATA_PIN                        (23u)

#define             TEMPBERRY_MODULE_1_ENABLE_PIN                   (5u)
#define             TEMPBERRY_MODULE_2_ENABLE_PIN                   (6u)
#define             TEMPBERRY_MODULE_3_ENABLE_PIN                   (7u)
#define             TEMPBERRY_MODULE_4_ENABLE_PIN                   (9u)
#define             TEMPBERRY_MODULE_5_ENABLE_PIN                   (11u)
#define             TEMPBERRY_MODULE_6_ENABLE_PIN                   (12u)
#define             TEMPBERRY_MODULE_7_ENABLE_PIN                   (13u)
#define             TEMPBERRY_MODULE_8_ENABLE_PIN                   (14u)
#define             TEMPBERRY_MODULE_9_ENABLE_PIN                   (17u)
#define             TEMPBERRY_MODULE_10_ENABLE_PIN                  (16u)


#define             TEMPBERRY_BMP280_SPI_SS                         (19)
#define             TEMPBERRY_BME680_SPI_SS                         (18)
#define             TEMPBERRY_MAX31855_SPI_SS                       (20)
#define             TEMPBERRY_MCP3201_SPI_SS                        (21)


TempberryPrototypeDatalogger::TempberryPrototypeDatalogger()
        : AbstractDatalogger("Tempberry Prototype Datalogger",
             "A datalogger for temperature measurement. Adds Enable and Errno to the software.") {
    // Modify the settings of abstract datalogger.
    // We will sample every second and store with ';' separators
    this->delayMillis = 1000;
    this->columnSeparator = ';';

    // Add modules
    // Internal
    this->addModule(new RP2040UptimeModule());
    this->addModule(new RP2040CoreTemperatureModule());

    // External
    // Module #1 -> DS18B20 temperature sensor @ OneWire
    this->addModule(
            new TempberryDS18B20Module(TEMPBERRY_MODULE_1_ENABLE_PIN,TEMPBERRY_ONE_WIRE_PIN)
    );
    // Module #2 -> DHT11 temperature sensor @ GPIO
    this->addModule(
            new TempberryDHT11Module(TEMPBERRY_MODULE_2_ENABLE_PIN,
                                     TEMPBERRY_DHT11_DATA_PIN)
    );
    // Module #3 -> SHTC3 sensor @ I2C
    this->addModule(new TempberrySHTC3Module(TEMPBERRY_MODULE_3_ENABLE_PIN));
    // Module #4 -> SHT4X sensor @ I2C
    this->addModule(new TempberrySHT4XModule(TEMPBERRY_MODULE_4_ENABLE_PIN));
    // TODO Module #5
    // TODO Module #6
    // Module #7 -> BMP280 @ SPI
    this->addModule(
            new TempberryBMP280Module(TEMPBERRY_MODULE_7_ENABLE_PIN,
                                      TEMPBERRY_BMP280_SPI_SS)
    );
    // Module #8 -> BME680 @ SPI
    this->addModule(
            new TempberryBME680Module(TEMPBERRY_MODULE_8_ENABLE_PIN,
                                      TEMPBERRY_BME680_SPI_SS)
    );
    // Module #9 -> Temperature resistor @ voltage divider @ MCP3201 (12 bit ADC) @ SPI
    this->addModule(
            new TempberryMCP3201Module(TEMPBERRY_MODULE_9_ENABLE_PIN,
                                       TEMPBERRY_MCP3201_SPI_SS)
    );

    // Module #10 -> Type-K thermistor @ MAX31855K @ SPI
    this->addModule(
            new TempberryMax31855Module(TEMPBERRY_MODULE_10_ENABLE_PIN,
                                        TEMPBERRY_MAX31855_SPI_SS)
    );

    // Add handlers
    this->addHandler(new ColoredUSBHandler(this->columnSeparator));
    this->addHandler(new SimpleSDCardHandler(TEMPBERRY_UPPER_SD_CARD_CS_PIN));
    this->addHandler(new SimpleSDCardHandler(TEMPBERRY_LOWER_SD_CARD_CS_PIN));

    // Add status indicators
    // TODO OLED Display controller

    // Add debuggers
    this->addDebugger(new ColoredUSBDebugger());

    // Add OLED controller
    auto oled = new OLEDDisplayDebugger();
    this->addDebugger(oled);
    this->addStatusIndicator(oled);
}
