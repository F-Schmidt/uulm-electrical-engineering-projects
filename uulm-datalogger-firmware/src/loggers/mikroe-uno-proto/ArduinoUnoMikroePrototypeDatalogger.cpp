// ---------------------------------------------------------------------------------------------------------
// MikroBus Datalogger Prototype for ARDUINO UNO Boards
// ---------------------------------------------------------------------------------------------------------
// Falko Schmidt, Ulm University, August 2022
//
// This software is developed for an Arduino UNO Board with MikroBus Adapter-PCB and these modules:
//
//    - A module with Max31855 thermocouple IC is connected to the socket '1'.
//    - An SD-module is connected to the socket '2'.
//
// ---------------------------------------------------------------------------------------------------------
#include "Adafruit_MAX31855.h"
#include "SD.h"

// ---------------------------------------------------------------------------------------------------------
// S E T T I N G S
// ---------------------------------------------------------------------------------------------------------
// The baud rate of the serial interface (USB). Usual values are 9600 or 115200.
#define             SERIAL_BAUD_RATE                (115200)

// ONLY CHANGE THIS OF YOU ARE USING A DIFFERENT HARDWARE CONFIGURATION.
// The chip-select pin of socket '1'. This defaults to 10.
#define             THERMOCOUPLE_MAX31855_CS_PIN    (10)

// ONLY CHANGE THIS OF YOU ARE USING A DIFFERENT HARDWARE CONFIGURATION.
// The chip-select pin of socket '2'. This defaults to 9.
#define             SD_CS_PIN                       (9)

// The approximate delay time between two logged datasets.
#define             DELAY_TIME_MS                   (2000)

// ---------------------------------------------------------------------------------------------------------
// ! ! ! D O   N O T   C H A N G E   T H E   F O L L O W I N G   P A R T ! ! !
// ---------------------------------------------------------------------------------------------------------
// This object will be used to fetch data from our thermocouple IC (MAX31855).
Adafruit_MAX31855 thermocouple(THERMOCOUPLE_MAX31855_CS_PIN);

// The current file name. On each restart, a new file will be created.
// A maximum of 1000 (named 0 - 999) files is supported. After that, the
// datalogger will no longer continue with logging or file creation.
String currentDataFilename;

// ---------------------------------------------------------------------------------------------------------

/**
 * This function will open the current data file in writing mode,
 * append the given data and then close the file again.
 * @param data the data that will be written.
 */
void writeToFile(const String& data) {
    File dataFile = SD.open(currentDataFilename, FILE_WRITE);
    if(!dataFile) {
        // Error while opening file
        return;
    }
    dataFile.write(data.c_str());
    dataFile.write('\n');
    dataFile.close();
}


/**
 * Writs the measured data to the serial interface using the format required for
 * the built-in Arduino IDE Serial Plotter.
 * @param max31855Temperature the internal temperature of the MAX31855 component
 * @param ambientTemperature the external thermistor temperature
 */
inline void writeToSerialPlotter(double max31855Temperature, double ambientTemperature) {
    // No timestamp needed. We will just output the 'Y' values for our plotter.
    // Format: <variable_name>:<variable_value>
    Serial.print("MAX31855 Temperature [C]:");
    Serial.print(max31855Temperature);
    Serial.print(',');
    Serial.print("Ambient Temperature [C]:");
    Serial.println(ambientTemperature);
}


/**
 * Helper function for serial interface. This will format
 * a string with the prefix [ OK ]. Use it for successful
 * messages only!
 * @param str the success message
 */
inline void printSuccessMessage(const char* str) {
    Serial.println((String)"[ OK ] " + str);
}


/**
 * This function will check which files already exist and
 * return the next file with a numeric suffix.
 * @return true on success, false on error.
 */


/**
 * This function will check which files already exist and
 * return the next file with a numeric suffix.
 * @return 0 on success, 1 if the top dir could not be created
 * and 2 if too many files already exist.
 */
uint8_t determineFilename() {

    // Note: maximum name length is 8 (SD only supports 8.3 names).
    // Also: This variable must start with a '/'. Otherwise, no
    // directory will be created!
    String dir = "/MIKROBUS";

    if(!SD.exists(dir)) {
        if(!SD.mkdir(dir)) {
            //printErrorMessage("Failed to create path for data file!");
            //return false;
            return 1;
        }
    }

    for(uint16_t suffix = 0; suffix < 1000; suffix++) {

        String fileName = dir + '/' + "DATA_";
        if(suffix < 10)   fileName += '0';
        if(suffix < 100)  fileName += '0';
        fileName += suffix;
        fileName += ".log";

        if(!SD.exists(fileName)) {
            // Not yet existing -> return the filename
            currentDataFilename = fileName;
            return 0;
        }
    }

    // Too many files
    return 2;
}


/**
 * Prints an error message to the serial interface,
 * activates the built-in LED and terminates program.
 * @param errorMsg the error message
 */
[[noreturn]] void errorHalt(const char* errorMsg) {

    digitalWrite(LED_BUILTIN, LOW);

    if(errorMsg != nullptr) {
        Serial.println((String)"[FAIL] " + errorMsg);
    }

    while (true) {
        //digitalWrite(LED_BUILTIN, HIGH);
        //delay(100);
        //digitalWrite(LED_BUILTIN, LOW);
        delay(100);
    }
}


/**
 * This function only be executed once at the beginning of the
 * program and will init everything we need for further programming.
 */
void setup() {

    // Init LED and disable
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);

    // Start USB interface
    Serial.begin(SERIAL_BAUD_RATE);
    while(!Serial) {
        delay(1);
    }


    // Enable connection to thermocouple module
    if(thermocouple.begin()) {
        printSuccessMessage("Initialization of communication interface for MAX31855 thermocouple done.");

        uint8_t thermocoupleError = thermocouple.readError();
        if(thermocoupleError == 0x00) {
            printSuccessMessage("Initialization of MAX31855 successfully finished!");
        } else {
            if (thermocoupleError & 0x01) {
                errorHalt("MAX31855: Open circuit detected, please connect thermocouple!");
            }

            if (thermocoupleError & 0x02) {
                errorHalt("MAX31855: Short circuit to GND detected!");
            }

            if (thermocoupleError & 0x04) {
                errorHalt("MAX31855: Short circuit to VCC detected!");
            }
        }
    } else {
        errorHalt("Could not communicate with MAX31855 thermocouple circuit!");
    }

    // Start communication with SD card
    if(SD.begin(SD_CS_PIN)) {
        printSuccessMessage("Initialization of SD card done.");
    } else {
        errorHalt("Failed to init SD card module! Is an SD-card inserted?");
    }


    uint8_t fileCreation = determineFilename();
    if(fileCreation == 0) {
        // Success
        printSuccessMessage(((String)"Current data file: " + currentDataFilename).c_str());
    } else if(fileCreation == 1) {
        // Failed to create dir
        errorHalt("Failed to create path for data file!");
    } else if(fileCreation == 2) {
        // Too many files
        errorHalt("Too many files exist. Please delete them from the SD and restart this datalogger.");
    } else {
        // Other error
        errorHalt("Unknown error during file creation on SD!");
    }


    // Create header string and write it into file (only once).
    // We have three data fields:
    //      - Uptime in milliseconds
    //      - Internal temperature of MAX31855 in Celsius
    //      - Ambient Temperature in Celsius
    String header = "Uptime [ms]";
    header += ";";
    header += "MAX31855 Internal Temperature [C]";
    header += ';';
    header += "Ambient Temperature [C]";
    writeToFile(header);
}


/**
 * This function will run forever.
 * In this case, the logging cycle is implemented here.
 */
void loop() {

    // Step 0: Enable LED.
    digitalWrite(LED_BUILTIN, HIGH);

    // Step 1: Fetch data.
    unsigned long currentMillis = millis();
    double max31855Temperature = thermocouple.readInternal();
    double ambientTemperature = thermocouple.readCelsius();

    // Step 2: Create string from data.
    String data = "";
    data += currentMillis;
    data += ';';
    data += max31855Temperature;
    data += ';';
    data += ambientTemperature;

    // Step 3: Store data to file (SD) and serial interface.
    writeToFile(data);
    writeToSerialPlotter(max31855Temperature, ambientTemperature);

    // Step 4: Disable LED.
    digitalWrite(LED_BUILTIN, LOW);

    // Step 5: Wait some time.
    delay(DELAY_TIME_MS);
}
