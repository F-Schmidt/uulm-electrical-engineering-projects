//
// Created by Falko Schmidt on 23.09.22.
//

#include "loggers/sguberry-2022/SGUberry2022Datalogger.h"
#include "modules/internal/rp2040/RP2040UptimeModule.h"
#include "modules/internal/rp2040/RP2040CoreTemperatureModule.h"
#include "handlers/SimpleSDCardHandler.h"
#include "handlers/ColoredUSBHandler.h"
#include "debuggers/ColoredUSBDebugger.h"
#include "status-indicators/SGUberryHelloWorldStatusIndicator.h"
#include "handlers/OpenLogHandler.h"
#include "modules/external/sgu-seminar-2022/SGUberryCompassModule.h"
#include "debuggers/OLEDDisplayDebugger.h"
#include "modules/external/sgu-seminar-2022/SGUberryWeatherKnife2000.h"
#include "modules/external/sgu-seminar-2022/SGUberryGasMeasurementModule.h"
#include "modules/external/sgu-seminar-2022/SGUberryRadiationModule.h"
#include "modules/external/sgu-seminar-2022/SGUberryAltitudeTransmissionModule.h"

#define             SGUBERRY_SPI_SS0                        (5u)
#define             SGUBERRY_SPI_SS1                        (6u)
#define             SGUBERRY_SPI_SS2                        (7u)
#define             SGUBERRY_SPI_SS3                        (8u)
#define             SGUBERRY_SPI_SS4                        (9u)
#define             SGUBERRY_SPI_SS5                        (10u)
#define             SGUBERRY_SPI_SS6                        (11u)
#define             SGUBERRY_INTERNAL_SD_SS_PIN             (12u)
#define             SGUBERRY_LED_HELLO_WORLD_PIN            (13u)   // This is also LED_BUILTIN

#define             SGUBERRY_OPENLOG_RESET_0                (14u)
#define             SGUBERRY_OPENLOG_RESET_1                (22u)

#define             SGUBERRY_OPENLOG_BAUD_RATE              (9600u)

SGUberry2022Datalogger::SGUberry2022Datalogger() : AbstractDatalogger("SGUberry2022Datalogger",
    "This data logger was developed as part of a seminar course in 2022 at "
    "Ulm University together with students from the Schubart Gymnasium Ulm.") {

    // Override fields from super class
    this->columnSeparator   = ';';
    this->delayMillis       = 1500;

    // Add modules
    // Internal
    this->addModule(new RP2040UptimeModule());              // Tested: success, 20/04/2023
    this->addModule(new RP2040CoreTemperatureModule());     // TODO Clashing with gas module!

    // External
    // Ulm university modules
    this->addModule(new SGUberryCompassModule());           // Tested: success, 20/04/2023

    // Student modules
    this->addModule(new SGUberryWeatherKnife2000(SGUBERRY_SPI_SS3));
    this->addModule(new SGUberryGasMeasurementModule(A2));
    this->addModule(new SGUberryRadiationModule());

    //TODO add student modules
    // Student modules

    // Add handlers
    this->addHandler(new SimpleSDCardHandler(SGUBERRY_INTERNAL_SD_SS_PIN));
    this->addHandler(new ColoredUSBHandler(this->columnSeparator));
    this->addHandler(new OpenLogHandler(&Serial1,SGUBERRY_OPENLOG_RESET_0,
                                        SGUBERRY_OPENLOG_BAUD_RATE));
    this->addHandler(new OpenLogHandler(&Serial2,SGUBERRY_OPENLOG_RESET_1,
                                        SGUBERRY_OPENLOG_BAUD_RATE));

    //TODO add student handlers

    // Add status indicators
    this->addStatusIndicator(new SGUberryHelloWorldStatusIndicator(SGUBERRY_LED_HELLO_WORLD_PIN));

    // Add debuggers
    this->addDebugger(new ColoredUSBDebugger());

    // Add OLED
    auto debug = new OLEDDisplayDebugger();
    this->addStatusIndicator(debug);
    this->addDebugger(debug);

    // Add antenna
    auto antenna = new SGUberryAltitudeTransmissionModule(SGUBERRY_SPI_SS0);
    this->addModule(antenna);
    this->addHandler(antenna);
}


bool SGUberry2022Datalogger::begin() {
    // These interfaces are used for OpenLog-modules.
    // It is necessary to start them here, otherwise the
    // modules do not receive / log any data!
    Serial1.begin(SGUBERRY_OPENLOG_BAUD_RATE);
    Serial2.begin(SGUBERRY_OPENLOG_BAUD_RATE);

    // After init of serial, we call the standard begin()-function.
    return AbstractDatalogger::begin();
}
