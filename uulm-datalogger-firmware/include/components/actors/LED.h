//
// Created by Falko Schmidt on 07.09.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_LED_H
#define UULM_DATALOGGER_FIRMWARE_LED_H

#include <cstdint>
#include "util/general/Nameable.h"
#include "util/general/Describable.h"
#include "util/general/Beginnable.h"

class LED : public Nameable, public Describable, public Beginnable {

private:
    uint8_t pin;
    bool isActiveHigh;

public:
    explicit LED(uint8_t pin, bool isActiveHigh = true);
    void on() const;
    void off() const;
    void toggle() const;

    bool begin() override;
};


#endif //UULM_DATALOGGER_FIRMWARE_LED_H
