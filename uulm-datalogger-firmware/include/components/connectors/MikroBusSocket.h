//
// Created by Falko Schmidt on 26.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_MIKROBUSSOCKET_H
#define UULM_DATALOGGER_FIRMWARE_MIKROBUSSOCKET_H

#include "../../../../../.platformio/packages/toolchain-gccarmnoneeabi/arm-none-eabi/include/c++/9.2.1/cstdint"
#include "../../../../../.platformio/packages/framework-arduino-mbed/libraries/SPI/SPI.h"
#include "../../../../../.platformio/packages/framework-arduino-mbed/libraries/Wire/Wire.h"

class MikroBusSocket {

private:
    // Left side
    uint8_t analogPin;
    uint8_t resetPin;
    uint8_t cs;
    HardwareSPI* spi;

    // Right side
    uint8_t pwmPin;
    uint8_t interruptPin;
    HardwareSerial* uart;
    HardwareI2C* wire;

public:
    MikroBusSocket(uint8_t analogPin, uint8_t resetPin, uint8_t cs, HardwareSPI *spi, uint8_t pwmPin,
                   uint8_t interruptPin, HardwareSerial *uart, HardwareI2C *wire);

    uint8_t getAnalogPin() const;

    uint8_t getResetPin() const;

    uint8_t getCs() const;

    HardwareSPI *getSpi() const;

    uint8_t getPwmPin() const;

    uint8_t getInterruptPin() const;

    HardwareSerial *getUart() const;

    HardwareI2C *getWire() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_MIKROBUSSOCKET_H
