//
// Created by Falko Alrik Schmidt on 24.01.23.
//

#ifndef UULM_DATALOGGER_FIRMWARE_MCP23017_H
#define UULM_DATALOGGER_FIRMWARE_MCP23017_H


#include <Wire.h>
#include "Adafruit_MCP23X17.h"

class MCP23017 : Adafruit_MCP23X17 {

private:
    uint8_t i2cAddress;

public:
    MCP23017(uint8_t a2, uint8_t a1, uint8_t a0);
    bool begin(TwoWire *wire = &Wire);
};


#endif //UULM_DATALOGGER_FIRMWARE_MCP23017_H
