//
// Created by Falko Schmidt on 28.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_SHT4X_H
#define UULM_DATALOGGER_FIRMWARE_SHT4X_H


#include "components/sensors/AbstractSensor.h"
#include "SensirionI2CSht4x.h"

class SHT4X : public AbstractSensor, public SensirionI2CSht4x {

public:

    /**
     * The different sensor series with different
     * accuracy.
     */
    enum SHT4XSensorSeries {
        SHT40,
        SHT41,
        SHT45
    };

private:
    enum SHT4XSensorSeries shtSensorSeries;

    float temperature;
    float humidity;

protected:
    explicit SHT4X(SHT4X::SHT4XSensorSeries shtSensorSeries);

public:
    double getTypicalTemperatureAccuracy(float temp);
    double getMaximumTemperatureAccuracy(float temp);
    double getHumidityAccuracy(float temp, float hum);

    bool begin() override;
    bool sample() override;

};


class SHT40 : public SHT4X {
public:
    explicit SHT40();
};

class SHT41 : public SHT4X {
public:
    explicit SHT41();
};

class SHT45 : public SHT4X {
public:
    explicit SHT45();
};

#endif //UULM_DATALOGGER_FIRMWARE_SHT4X_H
