//
// Created by Falko Alrik Schmidt on 22.01.23.
//

#ifndef UULM_DATALOGGER_FIRMWARE_DS3231_H
#define UULM_DATALOGGER_FIRMWARE_DS3231_H


#include "components/sensors/AbstractSensor.h"
#include "RTClib.h"

class DS3231 : public RTC_DS3231, public AbstractSensor {

private:
    float currentTemperature;
    DateTime currentDateTime;

public:
    explicit DS3231();
    bool begin() override;
    bool sample() override;

    float getCurrentTemperature() const;
    const DateTime &getCurrentDateTime() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_DS3231_H
