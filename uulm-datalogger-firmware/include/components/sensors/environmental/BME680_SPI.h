//
// Created by Falko Alrik Schmidt on 26.01.23.
//

#ifndef UULM_DATALOGGER_FIRMWARE_BME680_SPI_H
#define UULM_DATALOGGER_FIRMWARE_BME680_SPI_H


#include "Adafruit_BME680.h"
#include "components/sensors/AbstractSensor.h"

class BME680_SPI : public AbstractSensor, public Adafruit_BME680 {

public:
    BME680_SPI(int8_t cspin, SPIClass *theSpi = SPI);

};


#endif //UULM_DATALOGGER_FIRMWARE_BME680_SPI_H
