//
// Created by Falko Schmidt on 26.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_ABSTRACTSENSOR_H
#define UULM_DATALOGGER_FIRMWARE_ABSTRACTSENSOR_H

#include <cstdint>
#include "util/general/Sampleable.h"
#include "util/general/Beginnable.h"

#define         SL(X)           ((uint64_t)(1 << X))

class AbstractSensor : public Beginnable, public Sampleable {

protected:
    enum SensorType {
        OTHER           = 0,         // Use this, if none of the following types apply.
        TEMPERATURE     = SL(0),     // 1 (1)
        HUMIDITY        = SL(1),     // 2 (10)
        ACCELEROMETER   = SL(2),     // 4 (100)
        MAGNETOMETER    = SL(3),     // 8 (1000)
        REAL_TIME_CLOCK = SL(4),
        // ...
    };

private:
    uint64_t sensorType;

public:
    explicit AbstractSensor(uint64_t sensorType);

};


#endif //UULM_DATALOGGER_FIRMWARE_ABSTRACTSENSOR_H
