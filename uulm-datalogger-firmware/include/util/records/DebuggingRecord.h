//
// Created by Falko Schmidt on 01.09.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_DEBUGGINGRECORD_H
#define UULM_DATALOGGER_FIRMWARE_DEBUGGINGRECORD_H

#include <string>
#include "GenericRecord.h"
#include "modules/AbstractModule.h"

/**
 * The enum used to specify the type of debugging records.
 */
enum DebuggingType {
    SUCCESS = 0x00,
    INFO    = 0x01,
    WARN    = 0x02,
    ERROR   = 0x03
};


/**
 * Objects of this class will be passed to all debuggers and consist of
 * a debugging type as well as a message (string).
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class DebuggingRecord : public GenericRecord<DebuggingType> {

private:
    AbstractModule* theModule;

public:
    DebuggingRecord(DebuggingType type, const std::string &message, AbstractModule *theModule);
    AbstractModule *getModule() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_DEBUGGINGRECORD_H
