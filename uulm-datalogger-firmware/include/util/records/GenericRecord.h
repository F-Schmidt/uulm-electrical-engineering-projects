//
// Created by Falko Alrik Schmidt on 19.11.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_GENERICRECORD_H
#define UULM_DATALOGGER_FIRMWARE_GENERICRECORD_H

#include <string>

/**
 * Generic class for any record which contains a type and a message (string).
 * @tparam T the record type
 *
 * @author Falko Schmidt
 * @since 1.0
 */
template<typename T>
class GenericRecord {

private:
    T type;
    std::string message;

public:
    GenericRecord(T type, const std::string &message);
    T getType() const;
    const std::string &getMessage() const;
};

template<typename T>
GenericRecord<T>::GenericRecord(T type, const std::string &message) {
    this->type = type;
    this->message = message;
}

template<typename T>
T GenericRecord<T>::getType() const {
    return type;
}

template<typename T>
const std::string &GenericRecord<T>::getMessage() const {
    return message;
}

#endif //UULM_DATALOGGER_FIRMWARE_GENERICRECORD_H
