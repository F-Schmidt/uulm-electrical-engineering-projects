//
// Created by Falko Schmidt on 16.06.22.
//

#ifndef UULM_STRATOLOGGER_LOGRECORD_H
#define UULM_STRATOLOGGER_LOGRECORD_H


#include <string>
#include "util/records/GenericRecord.h"

/**
 * Enum to specify the type of log records.
 */
enum LogType {
    HEADER,
    DATA_ENTITY
};

/**
 * Objects of this class will be passed to handlers to store the sampled data.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class LogRecord : public GenericRecord<enum LogType> {

public:

    explicit LogRecord(LogType type, const std::string &message);

};


#endif //UULM_STRATOLOGGER_LOGRECORD_H
