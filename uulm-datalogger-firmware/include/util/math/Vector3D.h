//
// Created by Falko Schmidt on 28.05.22.
//

#ifndef UULM_STRATOLOGGER_VECTOR3D_H
#define UULM_STRATOLOGGER_VECTOR3D_H


#include "util/math/Vector2D.h"

template<typename T>
class Vector3D : public Vector2D<T> {

private:
    T z;

public:

    Vector3D(T x, T y, T z);

    T getZ() const;

    void setZ(T z);

};


#endif //UULM_STRATOLOGGER_VECTOR3D_H
