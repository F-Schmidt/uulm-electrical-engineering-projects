//
// Created by Falko Schmidt on 28.05.22.
//

#ifndef UULM_STRATOLOGGER_VECTOR2D_H
#define UULM_STRATOLOGGER_VECTOR2D_H

template<typename T>
class Vector2D {

private:
    T x;
    T y;

public:
    Vector2D(T x, T y);

    T getX() const;

    T getY() const;

    void setX(T x);

    void setY(T y);


    Vector2D<T> operator+(Vector2D<T> &add);

};


#endif //UULM_STRATOLOGGER_VECTOR2D_H
