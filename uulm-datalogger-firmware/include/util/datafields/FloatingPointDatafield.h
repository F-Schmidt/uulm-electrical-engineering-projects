//
// Created by Falko Alrik Schmidt on 20.11.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_FLOATINGPOINTDATAFIELD_H
#define UULM_DATALOGGER_FIRMWARE_FLOATINGPOINTDATAFIELD_H


#include "GenericDatafield.h"

#define             DEFAULT_FLOATING_POINT_DATAFIELD_PRECISION              (3)

template<typename T>
class FloatingPointDatafield : public GenericDatafield {

private:
    const std::function<T()> func;
    int precision;

public:
    explicit FloatingPointDatafield(const std::string &header, const std::function<T()> &func,
                                    int precision = DEFAULT_FLOATING_POINT_DATAFIELD_PRECISION);

private:
    std::string getFunctionResult() override;

};



#endif //UULM_DATALOGGER_FIRMWARE_FLOATINGPOINTDATAFIELD_H
