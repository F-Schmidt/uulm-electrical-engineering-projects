//
// Created by Falko Schmidt on 23.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_GENERICDATAFIELD_H
#define UULM_DATALOGGER_FIRMWARE_GENERICDATAFIELD_H

#include <string>
#include <functional>
#include <utility>
#include <iomanip>

class GenericDatafield {

private:
    const std::string header;

public:
    explicit GenericDatafield(std::string header);

    const std::string &getHeader() const;

    // This function MUST be overwritten in every subclass.
    virtual std::string getFunctionResult() = 0;

};

#endif //UULM_DATALOGGER_FIRMWARE_GENERICDATAFIELD_H
