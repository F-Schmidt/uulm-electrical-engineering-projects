//
// Created by Falko Alrik Schmidt on 20.11.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_NUMERICDATAFIELD_H
#define UULM_DATALOGGER_FIRMWARE_NUMERICDATAFIELD_H


#include "GenericDatafield.h"

template<typename T>
class NumericDatafield : public GenericDatafield {

private:
    const std::function<T()> func;

public:
    NumericDatafield(const std::string &header, const std::function<T()> &func);

private:
    std::string getFunctionResult() override;


};


#endif //UULM_DATALOGGER_FIRMWARE_NUMERICDATAFIELD_H
