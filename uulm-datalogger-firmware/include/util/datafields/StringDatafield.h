//
// Created by Falko Alrik Schmidt on 20.11.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_STRINGDATAFIELD_H
#define UULM_DATALOGGER_FIRMWARE_STRINGDATAFIELD_H


#include "GenericDatafield.h"

class StringDatafield : public GenericDatafield {

private:
    std::function<std::string()> func;
public:
    explicit StringDatafield(const std::string &header, std::function<std::string()> &func);

private:
    std::string getFunctionResult() override;
};


#endif //UULM_DATALOGGER_FIRMWARE_STRINGDATAFIELD_H
