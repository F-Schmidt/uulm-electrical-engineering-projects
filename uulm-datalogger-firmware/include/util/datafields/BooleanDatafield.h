//
// Created by Falko Alrik Schmidt on 20.11.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_BOOLEANDATAFIELD_H
#define UULM_DATALOGGER_FIRMWARE_BOOLEANDATAFIELD_H


#include <string>
#include <functional>
#include "GenericDatafield.h"

class BooleanDatafield : public GenericDatafield {

public:
    enum OutputType {
        NUMBER = 0x00,  // (DEFAULT CASE)   true -> '1',     false -> '0'
        STRING = 0x01   //                  true -> "true",  false -> "false"
    };

private:
    std::function<bool()> func;
    enum OutputType outputType;

public:
    BooleanDatafield(const std::string &header, std::function<bool()> &func, OutputType outputType = STRING);

private:
    std::string getFunctionResult() override;
};


#endif //UULM_DATALOGGER_FIRMWARE_BOOLEANDATAFIELD_H
