//
// Created by Falko Schmidt on 13.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_CLEARABLE_H
#define UULM_DATALOGGER_FIRMWARE_CLEARABLE_H

/**
 * This interface can be used for sensors, actors or other hardware related objects,
 * that can be cleared into a default state.
 *
 * Examples of some use cases:
 * - Empty display (OLED, LCD,...)
 * - Disable LED
 * - ...
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Clearable {

public:
    /**
     * This function will clear or reset a specific device.
     */
    virtual void clear() = 0;

};


#endif //UULM_DATALOGGER_FIRMWARE_CLEARABLE_H
