//
// Created by Falko Schmidt on 30.07.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_DESCRIBABLE_H
#define UULM_DATALOGGER_FIRMWARE_DESCRIBABLE_H


#include <string>
#include <functional>

/**
 * This class can be used to describe any object type.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Describable {

private:
    /**
     * The description of the object type.
     */
     std::string description;

public:
    /**
     * Default constructor.
     * @param description any description text
     */
    explicit Describable(std::string description);
    explicit Describable(const std::function<std::string()>& f);

    /**
     * Getter of description attribute
     * @return the description as string reference
     */
    [[nodiscard]] const std::string &getDescription() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_DESCRIBABLE_H
