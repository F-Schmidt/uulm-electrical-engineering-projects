//
// Created by Falko Schmidt on 13.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_BEGINNABLE_H
#define UULM_DATALOGGER_FIRMWARE_BEGINNABLE_H

/**
 * Most of the sensors or other hardware related objects must initially execute a program.
 * Therefor this interface can be used to ensure the implementation of a successful startup routine.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Beginnable {

public:
    /**
     * This method inits everything that is needed for the further program execution.
     * @return true on success, false if an error occurs
     */
    virtual bool begin() = 0;

};


#endif //UULM_DATALOGGER_FIRMWARE_BEGINNABLE_H
