//
// Created by Falko Schmidt on 02.09.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_IDENTIFIABLE_H
#define UULM_DATALOGGER_FIRMWARE_IDENTIFIABLE_H

template<typename T>
class Identifiable {

private:
    T id;

public:
    explicit Identifiable(T id);
    T getId() const;

};


#endif //UULM_DATALOGGER_FIRMWARE_IDENTIFIABLE_H
