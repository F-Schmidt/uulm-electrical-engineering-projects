//
// Created by Falko Schmidt on 30.07.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_NAMEABLE_H
#define UULM_DATALOGGER_FIRMWARE_NAMEABLE_H

#include <string>

/**
 * This class can be used to name any object type.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Nameable {

private:
    /**
     * The name attribute
     */
    const std::string name;

public:
    /**
     * Default constructor.
     * @param name the name of any object type
     */
    explicit Nameable(std::string name);

    /**
     * Getter of name attribute
     * @return the name as string reference
     */
    [[nodiscard]] const std::string &getName() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_NAMEABLE_H
