//
// Created by Falko Schmidt on 01.09.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_ENABLEABLE_H
#define UULM_DATALOGGER_FIRMWARE_ENABLEABLE_H


class Enableable {

public:
    virtual bool isEnabled() = 0;

};


#endif //UULM_DATALOGGER_FIRMWARE_ENABLEABLE_H
