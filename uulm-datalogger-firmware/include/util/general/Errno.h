//
// Created by Falko Alrik Schmidt on 05.11.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_ERRNO_H
#define UULM_DATALOGGER_FIRMWARE_ERRNO_H

#include <string>
#include <Stream.h>

#define         ERRNO_NO_ERROR              (0u)

// TODO function supportsErrno() with return bool

class Errno {

private:
    uint32_t errorNum;

protected:
    void setErrno(uint32_t errorNum);

public:
    Errno();

    uint32_t getErrno() const;
    bool hasError() const;
    void clearError();
    void printErrorStr(Stream &stream) const;

    virtual std::string getErrorStr() const = 0;
    virtual bool supportsErrno() const = 0;

};


#endif //UULM_DATALOGGER_FIRMWARE_ERRNO_H
