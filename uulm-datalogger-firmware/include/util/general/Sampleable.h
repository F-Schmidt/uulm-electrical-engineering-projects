//
// Created by Falko Schmidt on 28.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_SAMPLEABLE_H
#define UULM_DATALOGGER_FIRMWARE_SAMPLEABLE_H

/**
 * This class can be used for any device (sensors etc) that can sample data.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Sampleable {

public:
    virtual bool sample() = 0;

};


#endif //UULM_DATALOGGER_FIRMWARE_SAMPLEABLE_H
