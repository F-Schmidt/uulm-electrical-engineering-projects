//
// Created by Falko Alrik Schmidt on 27.11.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_ANSICOLOR_H
#define UULM_DATALOGGER_FIRMWARE_ANSICOLOR_H


#include <sstream>

namespace ANSI {

    enum TextColor {
        BLACK = 30,
        RED = 31,
        GREEN = 32,
        YELLOW = 33,
        BLUE = 34,
        MAGENTA = 35,
        CYAN = 36,
        WHITE = 37,
        RESET = 0
    };


    inline std::string ansiToString(enum ANSI::TextColor color) {
        std::stringstream ss;
        ss << "\u001b[" << color << "m";
        return ss.str();
    }


    inline std::string colorString(enum ANSI::TextColor color, const char* str) {
        std::stringstream ss;
        ss << ansiToString(color) << str << ansiToString(ANSI::TextColor::RESET);
        return ss.str();
    }


    inline enum TextColor next(enum ANSI::TextColor current) {
        switch (current) {
            // case BLACK:     return TextColor::RED;
            case RED:       return TextColor::GREEN;
            case GREEN:     return TextColor::YELLOW;
            case YELLOW:    return TextColor::BLUE;
            case BLUE:      return TextColor::MAGENTA;
            case MAGENTA:   return TextColor::CYAN;
            case CYAN:      return TextColor::WHITE;
            default:        return TextColor::RED; //TextColor::BLACK;
        }
    }
}


#endif //UULM_DATALOGGER_FIRMWARE_ANSICOLOR_H
