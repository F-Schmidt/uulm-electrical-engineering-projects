//
// Created by Falko Alrik Schmidt on 19.11.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_UNIT_H
#define UULM_DATALOGGER_FIRMWARE_UNIT_H

#include <string>
#include <sstream>

namespace Unit {
    /*
     * GENERAL
     */
    static constexpr const char* PERCENT = "%";

    /*
     * TEMPERATURE (K defined in section 'SI-Units')
     */
    static constexpr const char* CELSIUS    = "C";
    static constexpr const char* FAHRENHEIT = "F";

    /*
     * UNIT PREFIXES
     */
    static constexpr const char* TERA   = "T";
    static constexpr const char* GIGA   = "G";
    static constexpr const char* MEGA   = "M";
    static constexpr const char* KILO   = "k";
    static constexpr const char* HECTO  = "h";
    static constexpr const char* DECA   = "da";
    static constexpr const char* DECI   = "d";
    static constexpr const char* CENTI  = "c";
    static constexpr const char* MILLI  = "m";
    static constexpr const char* MICRO  = "u";
    static constexpr const char* NANO   = "n";
    static constexpr const char* PICO   = "p";

    /*
     * SI-UNITS
     */
    static constexpr const char* SECOND     = "s";
    static constexpr const char* METER      = "m";
    static constexpr const char* GRAM       = "g";
    static constexpr const char* AMPERE     = "A";
    static constexpr const char* KELVIN     = "K";
    static constexpr const char* MOLE       = "mol";
    static constexpr const char* CANDELA    = "cd";

    /**
     * Pressure
     */
     static constexpr const char* PASCAL    = "Pa";

     /**
      * Light
      */
      static constexpr const char* LUX      = "lx";

      /**
       * Angles
       */
      static constexpr const char* RADIANT  = "rad";

      /**
       * Magnetic field strength
       */
      static constexpr const char* TESLA  = "T";


    /**
       * Creates a unit string from given values.
       * @param prefix the unit prefix, for example milli or kilo
       * @param unit the unit, for example grams or second
       * @return a string containing the expected unit.
       */
       std::string of(const char* prefix, const char* unit);

       /**
        * Creates a unit as fraction, for example rad/s
        * @param numerator the numerator. In above example: rad
        * @param denominator the denominator. In above example: s
        * @return a unit string of any fraction
        */
       std::string frac(const char* numerator, const char* denominator);

}

#endif //UULM_DATALOGGER_FIRMWARE_UNIT_H
