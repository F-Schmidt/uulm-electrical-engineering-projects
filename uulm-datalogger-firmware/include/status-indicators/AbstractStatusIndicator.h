//
// Created by Falko Schmidt on 13.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_ABSTRACTSTATUSINDICATOR_H
#define UULM_DATALOGGER_FIRMWARE_ABSTRACTSTATUSINDICATOR_H


#include "util/general/Describable.h"
#include "util/general/Nameable.h"
#include "util/general/Beginnable.h"
#include "util/general/Clearable.h"

class AbstractStatusIndicator : public Nameable, public Beginnable, Clearable, Describable {

public:
    AbstractStatusIndicator(const std::string &name, const std::string &description);

    enum ProgramState {
        INITIALIZATION,
        SAMPLING,
        WRITING_DATA
    };

    // TODO CHANGE to multiple functions: onStart(ProgramState), onFinish(ProgramState, hasError).
    //  for a better structure!
    //  then -> onStart(INIT), bool error = /*init process*/; onFinish(INIT, error);

public:

    virtual void onStart(ProgramState state)  = 0;
    virtual void onFinish(ProgramState state) = 0;
    void clear() override = 0;
};


#endif //UULM_DATALOGGER_FIRMWARE_ABSTRACTSTATUSINDICATOR_H
