//
// Created by Falko Schmidt on 14.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_STRATBERRYPROTOTYPEDATALOGGERSTATUSINDICATOR_H
#define UULM_DATALOGGER_FIRMWARE_STRATBERRYPROTOTYPEDATALOGGERSTATUSINDICATOR_H


#include "status-indicators/AbstractStatusIndicator.h"
#include "components/actors/LED.h"

class StratberryPrototypeDataLoggerStatusIndicator : public AbstractStatusIndicator {

private:
    LED* initRunningLED;
    LED* initSuccessLED;
    LED* initFailureLED;
    LED* samplingRunningLED;
    LED* samplingFailureLED;
    LED* writingDataLED;
    LED* sdCardFailureLED;


public:
    explicit StratberryPrototypeDataLoggerStatusIndicator();

    void onStart(ProgramState state) override;

    void onFinish(ProgramState state) override;

    void clear() override;

private:
    bool begin() override;
};


#endif //UULM_DATALOGGER_FIRMWARE_STRATBERRYPROTOTYPEDATALOGGERSTATUSINDICATOR_H
