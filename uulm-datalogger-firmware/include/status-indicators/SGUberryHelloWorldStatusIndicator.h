//
// Created by Falko Alrik Schmidt on 27.09.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_SGUBERRYHELLOWORLDSTATUSINDICATOR_H
#define UULM_DATALOGGER_FIRMWARE_SGUBERRYHELLOWORLDSTATUSINDICATOR_H


#include "AbstractStatusIndicator.h"

class SGUberryHelloWorldStatusIndicator : public AbstractStatusIndicator {

private:
    uint8_t ledPin;

public:
    explicit SGUberryHelloWorldStatusIndicator(uint8_t ledPin);

    bool begin() override;

    void onStart(ProgramState state) override;

    void onFinish(ProgramState state) override;

    void clear() override;
};


#endif //UULM_DATALOGGER_FIRMWARE_SGUBERRYHELLOWORLDSTATUSINDICATOR_H
