//
// Created by Falko Schmidt on 01.09.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_USBDEBUGGER_H
#define UULM_DATALOGGER_FIRMWARE_USBDEBUGGER_H


#include "AbstractDebugger.h"
#include "util/records/DebuggingRecord.h"

class USBDebugger : public AbstractDebugger {
public:
    explicit USBDebugger(const std::string &name = "Plain USB Debugger");
    bool begin() override;
    void handleDebug(const DebuggingRecord& record) override;
};


#endif //UULM_DATALOGGER_FIRMWARE_USBDEBUGGER_H
