//
// Created by Falko Schmidt on 01.09.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_ABSTRACTDEBUGGER_H
#define UULM_DATALOGGER_FIRMWARE_ABSTRACTDEBUGGER_H


#include "util/general/Beginnable.h"
#include "util/general/Nameable.h"
#include "util/records/DebuggingRecord.h"

class AbstractDebugger : public Nameable, public Beginnable {

public:
    explicit AbstractDebugger(const std::string &name);
    bool begin() override = 0;

    // This must be overwritten in every subclass
    virtual void handleDebug(const DebuggingRecord& record) = 0;

};


#endif //UULM_DATALOGGER_FIRMWARE_ABSTRACTDEBUGGER_H
