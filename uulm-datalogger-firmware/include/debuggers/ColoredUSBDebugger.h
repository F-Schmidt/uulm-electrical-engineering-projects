//
// Created by Falko Schmidt on 01.09.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_COLOREDUSBDEBUGGER_H
#define UULM_DATALOGGER_FIRMWARE_COLOREDUSBDEBUGGER_H


#include "USBDebugger.h"

class ColoredUSBDebugger : public USBDebugger {

public:
    ColoredUSBDebugger();
    void handleDebug(const DebuggingRecord& record) override;

};


#endif //UULM_DATALOGGER_FIRMWARE_COLOREDUSBDEBUGGER_H
