//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_OLEDDISPLAYDEBUGGER_H
#define UULM_DATALOGGER_FIRMWARE_OLEDDISPLAYDEBUGGER_H


#include "AbstractDebugger.h"
#include "Adafruit_SSD1306.h"
#include "status-indicators/AbstractStatusIndicator.h"

class OLEDDisplayDebugger : public AbstractDebugger, public AbstractStatusIndicator {

private:
    Adafruit_SSD1306 oledDisplay;

    uint8_t warnings;
    uint8_t errors;

public:
    explicit OLEDDisplayDebugger(uint8_t width = 128, uint8_t height = 64);

    bool begin() override;

    void handleDebug(const DebuggingRecord &record) override;

    void onFinish(ProgramState state) override;

    void onStart(ProgramState state) override;

    void clear() override;
};


#endif //UULM_DATALOGGER_FIRMWARE_OLEDDISPLAYDEBUGGER_H
