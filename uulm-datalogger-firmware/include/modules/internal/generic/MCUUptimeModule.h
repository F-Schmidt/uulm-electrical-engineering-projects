//
// Created by Falko Schmidt on 17.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_MCUUPTIMEMODULE_H
#define UULM_DATALOGGER_FIRMWARE_MCUUPTIMEMODULE_H


#include "modules/internal/InternalModule.h"

class MCUUptimeModule : public InternalModule {

protected:
    explicit MCUUptimeModule(const std::string &name, const std::string &description);

public:
    bool begin() override;

protected:
    uint64_t uptimeMicros;

protected:
    uint64_t getUptimeMicros() const;

};


#endif //UULM_DATALOGGER_FIRMWARE_MCUUPTIMEMODULE_H
