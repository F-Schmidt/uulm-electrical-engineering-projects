//
// Created by Falko Schmidt on 18.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_MCUCORETEMPERATUREMODULE_H
#define UULM_DATALOGGER_FIRMWARE_MCUCORETEMPERATUREMODULE_H


#include "modules/internal/InternalModule.h"

class MCUCoreTemperatureModule : public InternalModule {

protected:
    double coreTemperature;

    double getCoreTemperature() const;

protected:
    explicit MCUCoreTemperatureModule(const std::string &name = "MCU Kernel Temperature [°C]",
         const std::string &description = "This module reads the internal temperature sensor of the used MCU.");

public:
    bool begin() override;

    bool sample() override = 0;

};


#endif //UULM_DATALOGGER_FIRMWARE_MCUCORETEMPERATUREMODULE_H
