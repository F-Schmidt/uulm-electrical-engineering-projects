//
// Created by Falko Schmidt on 17.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_ARDUINOMKR1310UPTIMEMODULE_H
#define UULM_DATALOGGER_FIRMWARE_ARDUINOMKR1310UPTIMEMODULE_H


#include "modules/internal/generic/MCUUptimeModule.h"

class ArduinoMKR1310UptimeModule : public MCUUptimeModule {

public:
    explicit ArduinoMKR1310UptimeModule();

private:
    bool begin() override;
    bool sample() override;

};


#endif //UULM_DATALOGGER_FIRMWARE_ARDUINOMKR1310UPTIMEMODULE_H
