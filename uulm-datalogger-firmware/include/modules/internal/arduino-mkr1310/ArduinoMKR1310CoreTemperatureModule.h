//
// Created by Falko Schmidt on 18.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_ARDUINOMKR1310CORETEMPERATUREMODULE_H
#define UULM_DATALOGGER_FIRMWARE_ARDUINOMKR1310CORETEMPERATUREMODULE_H

#include "modules/internal/generic/MCUCoreTemperatureModule.h"
#include "TemperatureZero.h"

class ArduinoMKR1310CoreTemperatureModule : public MCUCoreTemperatureModule {

private:
    TemperatureZero* mkr1310TemperatureReader;

public:
    explicit ArduinoMKR1310CoreTemperatureModule();
    bool begin() override;
    bool sample() override;

};


#endif //UULM_DATALOGGER_FIRMWARE_ARDUINOMKR1310CORETEMPERATUREMODULE_H
