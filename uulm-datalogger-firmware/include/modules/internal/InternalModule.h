//
// Created by Falko Schmidt on 25.05.22.
//

#ifndef UULM_STRATOLOGGER_INTERNALMODULE_H
#define UULM_STRATOLOGGER_INTERNALMODULE_H

#include "modules/AbstractModule.h"

class InternalModule : public AbstractModule {

protected:
    explicit InternalModule(const std::string &name, const std::string &description);

public:
    bool begin() override = 0;
    bool sample() override = 0;

    bool isEnabled() override;

    std::string getErrorStr() const override;

};


#endif //UULM_STRATOLOGGER_INTERNALMODULE_H
