//
// Created by Falko Schmidt on 26.05.22.
//

#ifndef UULM_STRATOLOGGER_RP2040UPTIMEMODULE_H
#define UULM_STRATOLOGGER_RP2040UPTIMEMODULE_H


#include "modules/internal/generic/MCUUptimeModule.h"

class RP2040UptimeModule : public MCUUptimeModule {

public:
    // Default constructor
    explicit RP2040UptimeModule();

private:
    bool begin() override;
    bool sample() override;
};


#endif //UULM_STRATOLOGGER_RP2040UPTIMEMODULE_H
