//
// Created by Falko Schmidt on 25.05.22.
//

#ifndef UULM_STRATOLOGGER_RP2040CORETEMPERATUREMODULE_H
#define UULM_STRATOLOGGER_RP2040CORETEMPERATUREMODULE_H


#include "cstdint"
#include "modules/internal/generic/MCUCoreTemperatureModule.h"

class RP2040CoreTemperatureModule : public MCUCoreTemperatureModule {

public:
    explicit RP2040CoreTemperatureModule();
    bool begin() override;
    bool sample() override;

};


#endif //UULM_STRATOLOGGER_RP2040CORETEMPERATUREMODULE_H
