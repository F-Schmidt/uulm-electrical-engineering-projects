//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_TEMPBERRYBME680MODULE_H
#define UULM_DATALOGGER_FIRMWARE_TEMPBERRYBME680MODULE_H


#include "TempberryModule.h"
#include "Adafruit_BME680.h"

class TempberryBME680Module : public TempberryModule {

private:


    Adafruit_BME680 bme680;

    float temperature;
    float humidity;
    uint32_t gasResistance;
    float altitude;

public:
    explicit TempberryBME680Module(uint8_t enableSwitchPin, int8_t bme680ssPin);

    bool begin() override;
    bool sample() override;

    float getTemperature() const;

    float getHumidity() const;

    float getGasResistance() const;

    float getAltitude() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_TEMPBERRYBME680MODULE_H
