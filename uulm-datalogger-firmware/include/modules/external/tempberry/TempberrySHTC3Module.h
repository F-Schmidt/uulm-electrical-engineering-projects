//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_TEMPBERRYSHTC3MODULE_H
#define UULM_DATALOGGER_FIRMWARE_TEMPBERRYSHTC3MODULE_H


#include "TempberryModule.h"
#include "SHTSensor.h"

class TempberrySHTC3Module : public TempberryModule {

private:
    SHTSensor shtc3;

    float humidity;
    float temperature;

public:
    explicit TempberrySHTC3Module(uint8_t enableSwitchPin);

    bool begin() override;
    bool sample() override;

    float getHumidity() const;

    float getTemperature() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_TEMPBERRYSHTC3MODULE_H
