//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_TEMPBERRYDHT11MODULE_H
#define UULM_DATALOGGER_FIRMWARE_TEMPBERRYDHT11MODULE_H


#include "TempberryModule.h"

#include "DHT.h"


class TempberryDHT11Module : public TempberryModule {

private:
    DHT *dht11;

    float humidity;
    float temperature;

public:
    explicit TempberryDHT11Module(uint8_t enableSwitchPin, uint8_t dht11Pin);

    bool begin() override;
    bool sample() override;

    float getHumidity() const;

    float getTemperature() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_TEMPBERRYDHT11MODULE_H
