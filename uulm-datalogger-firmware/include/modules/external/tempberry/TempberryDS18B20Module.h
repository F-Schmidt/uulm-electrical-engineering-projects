//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_TEMPBERRYDS18B20MODULE_H
#define UULM_DATALOGGER_FIRMWARE_TEMPBERRYDS18B20MODULE_H


#include "OneWire.h"
#include "DallasTemperature.h"
#include "modules/external/ExternalModule.h"
#include "TempberryModule.h"

class TempberryDS18B20Module : public TempberryModule {

private:
    OneWire oneWire;
    DallasTemperature sensors;

    float temperature;

public:
    explicit TempberryDS18B20Module(uint8_t enableSwitchPin, uint8_t oneWirePin);

    bool begin() override;
    bool sample() override;
    float getTemperature() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_TEMPBERRYDS18B20MODULE_H
