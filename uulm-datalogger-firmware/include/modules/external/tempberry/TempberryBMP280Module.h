//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_TEMPBERRYBMP280MODULE_H
#define UULM_DATALOGGER_FIRMWARE_TEMPBERRYBMP280MODULE_H


#include "TempberryModule.h"
#include "Adafruit_BMP280.h"

class TempberryBMP280Module : public TempberryModule {

private:

    Adafruit_BMP280 bmp280;

    float temperature;
    float pressure;
    float altitude;

public:
   explicit TempberryBMP280Module(uint8_t enableSwitchPin, int8_t bmp280ssPin);

    bool begin() override;
    bool sample() override;

    float getTemperature() const;
    float getPressure() const;

    float getAltitude() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_TEMPBERRYBMP280MODULE_H
