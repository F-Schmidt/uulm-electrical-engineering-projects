//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_TEMPBERRYMCP3201MODULE_H
#define UULM_DATALOGGER_FIRMWARE_TEMPBERRYMCP3201MODULE_H


#include "TempberryModule.h"
#include "Mcp320x.h"

class TempberryMCP3201Module : public TempberryModule {

private:

    MCP3201 *adc;

    double temperature;

public:
    explicit TempberryMCP3201Module(uint8_t enableSwitchPin, uint8_t mcp3201ssPin);

    bool begin() override;
    bool sample() override;

    double getTemperature() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_TEMPBERRYMCP3201MODULE_H
