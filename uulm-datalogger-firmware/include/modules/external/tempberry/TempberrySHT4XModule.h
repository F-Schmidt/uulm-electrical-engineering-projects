//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_TEMPBERRYSHT4XMODULE_H
#define UULM_DATALOGGER_FIRMWARE_TEMPBERRYSHT4XMODULE_H


#include "TempberryModule.h"
#include "SHTSensor.h"

class TempberrySHT4XModule : public TempberryModule {

private:
    float temperature;
    float humidity;

    SHTSensor sht4x;

public:
    explicit TempberrySHT4XModule(uint8_t enableSwitchPin);

    bool begin() override;
    bool sample() override;

    float getTemperature() const;

    float getHumidity() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_TEMPBERRYSHT4XMODULE_H
