//
// Created by Falko Schmidt on 02.09.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_TEMPBERRYMODULE_H
#define UULM_DATALOGGER_FIRMWARE_TEMPBERRYMODULE_H


#include "modules/external/ExternalModule.h"
#include "util/general/Identifiable.h"

class TempberryModule : public ExternalModule {

private:
    uint8_t enableSwitchPin;

public:
    explicit TempberryModule(const std::string &name, const std::string &description, uint8_t enableSwitchPin);

    bool begin() override = 0;
    bool sample() override = 0;

    bool isEnabled() override;
};


#endif //UULM_DATALOGGER_FIRMWARE_TEMPBERRYMODULE_H
