//
// Created by Falko Alrik Schmidt on 03.02.23.
//

#ifndef UULM_DATALOGGER_FIRMWARE_GENERICBME680MODULE_H
#define UULM_DATALOGGER_FIRMWARE_GENERICBME680MODULE_H


#include "modules/external/ExternalModule.h"
#include "Adafruit_BME680.h"

class GenericBME680Module : public ExternalModule {

private:
    Adafruit_BME680 *bme;

    float temperature;
    float humidity;
    uint32_t pressure;
    uint32_t gasResistance;
    float altitude;

public:
    explicit GenericBME680Module(int8_t ssPin,
        const std::string &name = "Generic BME680 Module",
        const std::string &description = "This module measures temperature, humidity, air pressure and calculates the current altitude.");

    bool begin() override;
    bool sample() override;

    bool supportsErrno() const override;
    std::string getErrorStr() const override;

    float getTemperature() const;
    float getHumidity() const;
    uint32_t getPressure() const;
    uint32_t getGasResistance() const;
    float getAltitude() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_GENERICBME680MODULE_H
