//
// Created by Falko Alrik Schmidt on 22.01.23.
//

#ifndef UULM_DATALOGGER_FIRMWARE_GENERICRTCMODULE_H
#define UULM_DATALOGGER_FIRMWARE_GENERICRTCMODULE_H


#include "modules/external/ExternalModule.h"
#include "RTClib.h"

/**
 *
 */
class GenericRTCModule : public ExternalModule {

public:
    enum RTCType {
        DS1307,
        DS3231,
        PCF8523,
        PCF8563
    };

private:
    RTC_I2C rtc;

public:
    GenericRTCModule();

public:
    static void forType(RTCType type);

};


#endif //UULM_DATALOGGER_FIRMWARE_GENERICRTCMODULE_H
