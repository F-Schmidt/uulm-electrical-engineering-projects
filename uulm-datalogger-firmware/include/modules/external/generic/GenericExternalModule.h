//
// Created by Falko Alrik Schmidt on 09.12.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_GENERICEXTERNALMODULE_H
#define UULM_DATALOGGER_FIRMWARE_GENERICEXTERNALMODULE_H


#include "modules/external/ExternalModule.h"
#include "components/sensors/AbstractSensor.h"

class GenericExternalModule : public ExternalModule {

private:
    AbstractSensor* sensor;

};


#endif //UULM_DATALOGGER_FIRMWARE_GENERICEXTERNALMODULE_H
