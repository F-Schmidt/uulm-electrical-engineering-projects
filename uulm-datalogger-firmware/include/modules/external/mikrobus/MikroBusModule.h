//
// Created by Falko Schmidt on 26.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_MIKROBUSMODULE_H
#define UULM_DATALOGGER_FIRMWARE_MIKROBUSMODULE_H


#include "modules/external/ExternalModule.h"
#include "components/connectors/MikroBusSocket.h"

class MikroBusModule : public ExternalModule {

private:
    MikroBusSocket &socket;

public:
    explicit MikroBusModule(const std::string &name, const std::string &description, MikroBusSocket &socket);
};


#endif //UULM_DATALOGGER_FIRMWARE_MIKROBUSMODULE_H
