//
// Created by Falko Schmidt on 25.05.22.
//

#ifndef UULM_STRATOLOGGER_EXTERNALMODULE_H
#define UULM_STRATOLOGGER_EXTERNALMODULE_H


#include "modules/AbstractModule.h"
#include "loggers/AbstractDatalogger.h"

class ExternalModule : public AbstractModule {

protected:
public:
    explicit ExternalModule(const std::string &name, const std::string &description);

public:
    bool begin() override = 0;
    bool sample() override = 0;

};


#endif //UULM_STRATOLOGGER_EXTERNALMODULE_H
