//
// Created by Falko Schmidt on 28.05.22.
//

#ifndef UULM_STRATOLOGGER_UULMREALTIMECLOCKMODULE_H
#define UULM_STRATOLOGGER_UULMREALTIMECLOCKMODULE_H

#include "RTClib.h"
#include "modules/external/ExternalModule.h"

class UULMRealTimeClockModule : public ExternalModule {

private:
    RTC_DS3231 *ds3231;
    DateTime *now;
    float temperature;

public:
    // Default constructor
    explicit UULMRealTimeClockModule();

    // Overridden methods from AbstractModule
    bool begin() override;

    bool sample() override;

    // Custom logging functions from this module
    std::string getTimestamp();

    std::string getDS3231Temperature() const;

private:
    // These methods increment the time stored in RTC by 1 on button press
    void updateHours();

    void updateMinutes();

};


#endif //UULM_STRATOLOGGER_UULMREALTIMECLOCKMODULE_H
