//
// Created by Falko Schmidt on 28.05.22.
//

#ifndef UULM_STRATOLOGGER_UULMPROBETEMPERATUREMODULE_H
#define UULM_STRATOLOGGER_UULMPROBETEMPERATUREMODULE_H


#include "modules/external/ExternalModule.h"
#include "Adafruit_BME280.h"
#include "SHTSensor.h"

class UULMProbeClimateModule : public ExternalModule {

private:
    Adafruit_BME280 *bme280;
    SHTSensor *sht4x;

    // Variables to sample the sensor BME280.
    // For the sensirion sensor all values are stored
    // within the software library used.
    float humidityBme280;
    float temperatureBme280;
    float pressureBme280;
    float altitudeBme280;

public:
    explicit UULMProbeClimateModule();

    bool begin() override;

    bool sample() override;

    inline std::string getHumidityBME280() const;

    inline std::string getTemperatureBME280() const;

    inline std::string getPressureBME280() const;

    inline std::string getAltitudeBME280() const;

    inline std::string getHumiditySHT4X();

    inline std::string getTemperatureSHT4X();

};


#endif //UULM_STRATOLOGGER_UULMPROBETEMPERATUREMODULE_H
