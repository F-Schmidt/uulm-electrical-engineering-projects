//
// Created by Falko Schmidt on 28.05.22.
//

#ifndef UULM_STRATOLOGGER_UULMAMBIENTTEMPERATUREMODULE_H
#define UULM_STRATOLOGGER_UULMAMBIENTTEMPERATUREMODULE_H

#include "Mcp320x.h"
#include "modules/external/ExternalModule.h"

class UULMAmbientTemperatureModule : public ExternalModule {

private:
    MCP3204 *adc;
    uint16_t adcValuesRaw[4] = {0, 0, 0, 0};

public:
    // Default constructor
    explicit UULMAmbientTemperatureModule();

    // Overridden methods from AbstractModule
    //bool inscribe(AbstractDatalogger *logger) override;

    bool begin() override;

    bool sample() override;

    // Custom logging functions from this module
    inline std::string getTemperatureThermistor0();

    inline std::string getTemperatureThermistor1();

    inline std::string getTemperatureThermistor2();

    inline std::string getTemperatureThermistor3();

private:
    uint16_t sampleThermistor(uint8_t pin, MCP3204::Channel);

    double getTemperatureThermistor(uint8_t channel);

};


#endif //UULM_STRATOLOGGER_UULMAMBIENTTEMPERATUREMODULE_H
