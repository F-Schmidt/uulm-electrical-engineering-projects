//
// Created by Falko Schmidt on 28.05.22.
//

#ifndef UULM_STRATOLOGGER_STEMMAQTTLV493DMODULE_H
#define UULM_STRATOLOGGER_STEMMAQTTLV493DMODULE_H

#include "Tlv493d.h"
#include "modules/external/ExternalModule.h"
#include "util/math/Vector3D.h"

class StemmaQtTLV493DModule : public ExternalModule {

private:
    Tlv493d *magnetometer;
    Vector3D<float> *magnetStrength;

public:
    // Default constructor
    explicit StemmaQtTLV493DModule();

    // Overridden methods from AbstractModule
    bool begin() override;

    bool sample() override;

    // Custom logging functions from this module
    inline std::string getMagnetFieldStrengthX();

    inline std::string getMagnetFieldStrengthY();

    inline std::string getMagnetFieldStrengthZ();

};


#endif //UULM_STRATOLOGGER_STEMMAQTTLV493DMODULE_H
