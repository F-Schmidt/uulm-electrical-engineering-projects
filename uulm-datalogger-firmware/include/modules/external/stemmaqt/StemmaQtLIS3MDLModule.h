//
// Created by Falko Schmidt on 28.05.22.
//

#ifndef UULM_STRATOLOGGER_STEMMAQTLIS3MDLMODULE_H
#define UULM_STRATOLOGGER_STEMMAQTLIS3MDLMODULE_H

#include "modules/external/ExternalModule.h"
#include "Adafruit_LIS3MDL.h"
#include "util/math/Vector3D.h"

class StemmaQtLIS3MDLModule : public ExternalModule {

private:
    Adafruit_LIS3MDL *magnetometer;
    Vector3D<float> *magneticStrength;

public:
    // Default constructor
    StemmaQtLIS3MDLModule();

    // Overridden methods from AbstractModule
    bool begin() override;

    bool sample() override;

    // Custom logging functions from this module
    inline std::string getMagnetFieldStrengthX();

    inline std::string getMagnetFieldStrengthY();

    inline std::string getMagnetFieldStrengthZ();
};


#endif //UULM_STRATOLOGGER_STEMMAQTLIS3MDLMODULE_H
