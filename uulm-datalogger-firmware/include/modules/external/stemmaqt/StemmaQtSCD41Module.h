//
// Created by Falko Schmidt on 02.07.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_STEMMAQTSCD41MODULE_H
#define UULM_DATALOGGER_FIRMWARE_STEMMAQTSCD41MODULE_H

#include "modules/external/ExternalModule.h"
#include "SensirionI2CScd4x.h"

class StemmaQtSCD41Module : public ExternalModule {

private:
    SensirionI2CScd4x *scd4x;

    uint16_t co2;
    float temperature;
    float humidity;

public:
    explicit StemmaQtSCD41Module();

    bool begin() override;

    bool sample() override;

    inline std::string getCO2Concentration() const;

    inline std::string getTemperature() const;

    inline std::string getHumidity() const;

};


#endif //UULM_DATALOGGER_FIRMWARE_STEMMAQTSCD41MODULE_H
