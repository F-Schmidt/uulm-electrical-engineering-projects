//
// Created by Falko Schmidt on 20.07.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_STEMMAQTPMSA003IMODULE_H
#define UULM_DATALOGGER_FIRMWARE_STEMMAQTPMSA003IMODULE_H


#include "modules/external/ExternalModule.h"
#include "Adafruit_PM25AQI.h"
#include <string>

class StemmaQtPMSA003IModule : public ExternalModule {

private:
    Adafruit_PM25AQI *pmsa003i;
    PM25_AQI_Data *measurement;

public:
    explicit StemmaQtPMSA003IModule();

    bool begin() override;

    bool sample() override;

private:
    std::string getParticles03um();

    std::string getParticles05um();

    std::string getParticles10um();

    std::string getParticles25um();

    std::string getParticles50um();

    std::string getParticles100um();

};


#endif //UULM_DATALOGGER_FIRMWARE_STEMMAQTPMSA003IMODULE_H
