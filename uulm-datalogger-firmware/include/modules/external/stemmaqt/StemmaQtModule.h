//
// Created by Falko Schmidt on 28.05.22.
//

#ifndef UULM_STRATOLOGGER_STEMMAQTMODULE_H
#define UULM_STRATOLOGGER_STEMMAQTMODULE_H


#include "modules/AbstractModule.h"

class StemmaQtModule {

public:
    enum StemmaQtSensor {
        // Add supported modules from Stemma Qt here
        LIS3MDL,
        TLV493D,
        TSL2591,
        MCP9808,
        SCD41,
        PMSA003I,
        LTR390,
        HTU31D,
        BME680,
        AS7341,
        MSA301,
    };

public:
    static AbstractModule *fromSensor(enum StemmaQtSensor sensor);

};


#endif //UULM_STRATOLOGGER_STEMMAQTMODULE_H
