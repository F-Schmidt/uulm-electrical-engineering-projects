//
// Created by Falko Schmidt on 20.07.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_STEMMAQTAS7341MODULE_H
#define UULM_DATALOGGER_FIRMWARE_STEMMAQTAS7341MODULE_H


#include "Adafruit_AS7341.h"
#include "modules/external/ExternalModule.h"

class StemmaQtAS7341Module : public ExternalModule {

private:
    Adafruit_AS7341 *as7341;
    uint16_t readings[12];

public:
    explicit StemmaQtAS7341Module();

    bool begin() override;

    bool sample() override;

private:
    std::string getLightAt415nm();

    std::string getLightAt445nm();

    std::string getLightAt480nm();

    std::string getLightAt515nm();

    std::string getLightAt555nm();

    std::string getLightAt590nm();

    std::string getLightAt630nm();

    std::string getLightAt680nm();

    std::string getLightClear();

    std::string getLightNIR();
};


#endif //UULM_DATALOGGER_FIRMWARE_STEMMAQTAS7341MODULE_H
