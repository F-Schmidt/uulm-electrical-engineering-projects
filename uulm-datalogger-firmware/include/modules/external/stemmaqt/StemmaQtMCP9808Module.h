//
// Created by Falko Schmidt on 02.07.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_STEMMAQTMCP9808MODULE_H
#define UULM_DATALOGGER_FIRMWARE_STEMMAQTMCP9808MODULE_H


#include "Adafruit_MCP9808.h"
#include "modules/external/ExternalModule.h"


class StemmaQtMCP9808Module : public ExternalModule {

private:
    Adafruit_MCP9808 *mcp9808;
    float temperatureC;
    float temperatureF;

public:
    StemmaQtMCP9808Module();

    bool begin() override;

    bool sample() override;

    std::string getTemperatureC() const;

    std::string getTemperatureF() const;

};


#endif //UULM_DATALOGGER_FIRMWARE_STEMMAQTMCP9808MODULE_H
