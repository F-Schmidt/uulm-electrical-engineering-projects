//
// Created by Falko Schmidt on 20.07.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_STEMMAQTLTR390MODULE_H
#define UULM_DATALOGGER_FIRMWARE_STEMMAQTLTR390MODULE_H


#include "modules/external/ExternalModule.h"
#include "loggers/AbstractDatalogger.h"
#include "Adafruit_LTR390.h"

class StemmaQtLTR390Module : public ExternalModule {

private:
    Adafruit_LTR390 *ltr;
    uint32_t uvs;
    uint32_t als;

public:
    StemmaQtLTR390Module();

    bool begin() override;

    bool sample() override;

private:
    uint8_t getGainFactor();

    double getResolutionFactor();

    double calculateLux();

    double calculateUVIndex();

    std::string getLux();

    std::string getUVIndex();

};


#endif //UULM_DATALOGGER_FIRMWARE_STEMMAQTLTR390MODULE_H
