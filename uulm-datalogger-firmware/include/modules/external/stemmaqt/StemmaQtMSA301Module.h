//
// Created by Falko Schmidt on 20.07.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_STEMMAQTMSA301MODULE_H
#define UULM_DATALOGGER_FIRMWARE_STEMMAQTMSA301MODULE_H


#include "Adafruit_MSA301.h"
#include "modules/external/ExternalModule.h"
#include "util/math/Vector3D.h"

class StemmaQtMSA301Module : public ExternalModule {

private:
    Adafruit_MSA301 *msa301;
    Vector3D<float> *acceleration;

public:
    StemmaQtMSA301Module();

    bool begin() override;

    bool sample() override;

    std::string getAccelerationX();

    std::string getAccelerationY();

    std::string getAccelerationZ();
};


#endif //UULM_DATALOGGER_FIRMWARE_STEMMAQTMSA301MODULE_H
