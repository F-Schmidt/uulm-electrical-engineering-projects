//
// Created by Falko Schmidt on 20.07.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_STEMMAQTBME680MODULE_H
#define UULM_DATALOGGER_FIRMWARE_STEMMAQTBME680MODULE_H


#include "modules/external/ExternalModule.h"
#include "Adafruit_BME680.h"

class StemmaQtBME680Module : public ExternalModule {

private:
    Adafruit_BME680 *bme680;
    float temperature;
    float humidity;
    float pressure;
    uint32_t gasResistance;
    float altitude;

public:
    StemmaQtBME680Module();

    bool begin() override;

    bool sample() override;

private:
    std::string getTemperature();

    std::string getHumidity();

    std::string getPressure();

    std::string getGasResistance();

    std::string getAltitude();
};


#endif //UULM_DATALOGGER_FIRMWARE_STEMMAQTBME680MODULE_H
