//
// Created by Falko Schmidt on 20.07.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_STEMMAQTHTU31DMODULE_H
#define UULM_DATALOGGER_FIRMWARE_STEMMAQTHTU31DMODULE_H


#include "modules/external/ExternalModule.h"
#include "Adafruit_Sensor.h"
#include "Adafruit_HTU31D.h"

class StemmaQtHTU31DModule : public ExternalModule {

private:
    Adafruit_HTU31D *htu;
    sensors_event_t humidity;
    sensors_event_t temp;

public:
    explicit StemmaQtHTU31DModule();

    bool begin() override;

    bool sample() override;

    std::string getTemperature();

    std::string getHumidity();

};


#endif //UULM_DATALOGGER_FIRMWARE_STEMMAQTHTU31DMODULE_H
