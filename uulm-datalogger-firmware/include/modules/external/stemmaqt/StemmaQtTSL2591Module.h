//
// Created by Falko Schmidt on 08.06.22.
//

#ifndef UULM_STRATOLOGGER_STEMMAQTTSL2591MODULE_H
#define UULM_STRATOLOGGER_STEMMAQTTSL2591MODULE_H

#include "Adafruit_TSL2591.h"
#include "modules/external/ExternalModule.h"

class StemmaQtTSL2591Module : public ExternalModule {

private:
    Adafruit_TSL2591 *tsl2591;
    uint32_t lum;
    uint16_t ir;
    uint16_t full;
    float lux;

public:
    explicit StemmaQtTSL2591Module();

    bool begin() override;

    bool sample() override;
};


#endif //UULM_STRATOLOGGER_STEMMAQTTSL2591MODULE_H
