//
// Created by Falko Alrik Schmidt on 14.01.23.
//

#ifndef UULM_DATALOGGER_FIRMWARE_SGUBERRYCOMPASSMODULE_H
#define UULM_DATALOGGER_FIRMWARE_SGUBERRYCOMPASSMODULE_H


#include "modules/external/ExternalModule.h"
#include "Adafruit_LIS3MDL.h"
#include "util/math/Vector3D.h"
#include "Adafruit_MCP23008.h"

class SGUberryCompassModule : public ExternalModule {

private:
    Adafruit_LIS3MDL *magnetometer;
    Adafruit_MCP23008 *portExpander;
    Vector3D<float> *measurement;

public:
    explicit SGUberryCompassModule();
    bool begin() override;
    bool sample() override;

private:
    enum Direction {
        NORTH,
        NORTH_EAST,
        EAST,
        SOUTH_EAST,
        SOUTH,
        SOUTH_WEST,
        WEST,
        NORTH_WEST
    };

private:
    double getHeadingDegrees();
    enum SGUberryCompassModule::Direction degreeToDirection();
    void updateLEDs();

};


#endif //UULM_DATALOGGER_FIRMWARE_SGUBERRYCOMPASSMODULE_H
