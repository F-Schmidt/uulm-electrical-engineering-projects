//
// Created by Falko Alrik Schmidt on 15.03.23.
//

#ifndef UULM_DATALOGGER_FIRMWARE_SGUBERRYALTITUDETRANSMISSIONMODULE_H
#define UULM_DATALOGGER_FIRMWARE_SGUBERRYALTITUDETRANSMISSIONMODULE_H


#include "debuggers/AbstractDebugger.h"
#include "modules/external/ExternalModule.h"
#include "modules/external/generic/GenericBME680Module.h"
#include "RF24.h"

class SGUberryAltitudeTransmissionModule : public GenericBME680Module, public AbstractDebugger, public AbstractHandler {

private:
    RF24 *radio;
    bool isStarted; // Dirty hack to only execute setup() once

public:
    explicit SGUberryAltitudeTransmissionModule(int8_t ssPinBME680);

    bool begin() override;

    void writeRecord(const LogRecord &log) override;

    void handleDebug(const DebuggingRecord &record) override;
};


#endif //UULM_DATALOGGER_FIRMWARE_SGUBERRYALTITUDETRANSMISSIONMODULE_H
