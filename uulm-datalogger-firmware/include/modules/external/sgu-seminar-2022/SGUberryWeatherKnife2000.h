//
// Created by Falko Alrik Schmidt on 15.03.23.
//

#ifndef UULM_DATALOGGER_FIRMWARE_SGUBERRYWEATHERKNIFE2000_H
#define UULM_DATALOGGER_FIRMWARE_SGUBERRYWEATHERKNIFE2000_H


#include "modules/external/ExternalModule.h"
#include "Adafruit_MAX31865.h"

class SGUberryWeatherKnife2000 : public ExternalModule {

private:
    int8_t thermoCS;
    Adafruit_MAX31865* thermo;
    float temperature;
    double relativeHumidity;
    volatile uint32_t interruptCount;
    unsigned long lastMeasurement;

public:
    explicit SGUberryWeatherKnife2000(int8_t thermoCS);
    bool begin() override;
    bool sample() override;

    static void irq_forwarder_function(void* pointer);

    float getTemperature() const;
    double getRelativeHumidity() const;
    float getAbsoluteHumidity() const;

    void countInterrupt();

private:
    uint32_t getInterruptCount() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_SGUBERRYWEATHERKNIFE2000_H
