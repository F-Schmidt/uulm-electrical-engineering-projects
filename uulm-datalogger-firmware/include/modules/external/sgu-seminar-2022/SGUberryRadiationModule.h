//
// Created by Falko Alrik Schmidt on 13.02.23.
//

#ifndef UULM_DATALOGGER_FIRMWARE_SGUBERRYRADIATIONMODULE_H
#define UULM_DATALOGGER_FIRMWARE_SGUBERRYRADIATIONMODULE_H


#include "modules/external/ExternalModule.h"
#include "Adafruit_MCP23X17.h"

class SGUberryRadiationModule : public ExternalModule {

private:

    // Debounce for software clicks
    unsigned long lastClickMicros;

    // Software clicks
    uint16_t swIrqCount;
    uint16_t momentarySwCount{};
    unsigned long totalSoftwareClicks{};

    // Hardware clicks
    uint16_t hwClickCount{};
    uint16_t momentaryHwCount{};
    unsigned long totalHardwareClicks{};
    Adafruit_MCP23X17 *portExpander;
    bool isPortExpanderWorking;

    // Variables that the sampled data will be stored in
    double softwareCPM{};
    double hardwareCPM{};

    // Time to calculate cpm
    unsigned long lastMeasurementMillis;

public:
    SGUberryRadiationModule();
    bool begin() override;
    bool sample() override;

private:
    void countInterrupt();
    static void irq_forwarder_function(void* pointer);

public:
    unsigned long getTotalSoftwareClicks() const;
    unsigned long getTotalHardwareClicks() const;
    double getSoftwareCPM() const;
    double getHardwareCPM() const;

    uint16_t readHardwareCounters();
    static void resetHardwareCounter();

private:
    static uint8_t reverseByte(uint8_t b);
};


#endif //UULM_DATALOGGER_FIRMWARE_SGUBERRYRADIATIONMODULE_H
