//
// Created by Falko Alrik Schmidt on 02.02.23.
//

#ifndef UULM_DATALOGGER_FIRMWARE_SGUBERRYALTITUDEMODULE_H
#define UULM_DATALOGGER_FIRMWARE_SGUBERRYALTITUDEMODULE_H

#include "modules/external/ExternalModule.h"
#include "modules/external/generic/GenericBME680Module.h"

class SGUberryAltitudeModule : public GenericBME680Module {

public:
    explicit SGUberryAltitudeModule(int8_t ssPin);
};


#endif //UULM_DATALOGGER_FIRMWARE_SGUBERRYALTITUDEMODULE_H
