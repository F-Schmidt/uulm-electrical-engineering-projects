//
// Created by Falko Alrik Schmidt on 15.03.23.
//

#ifndef UULM_DATALOGGER_FIRMWARE_SGUBERRYGASMEASUREMENTMODULE_H
#define UULM_DATALOGGER_FIRMWARE_SGUBERRYGASMEASUREMENTMODULE_H


#include "modules/external/ExternalModule.h"
#include "SensirionI2CScd4x.h"

/**
 * CO2 and methane in PPM
 */
class SGUberryGasMeasurementModule : public ExternalModule {

private:
    SensirionI2CScd4x* scd4x;
    uint8_t pinMQ4;

    float temperature = 0.0f;
    float humidity = 0.0f;

    uint16_t co2PPM;
    double methanePPM;

public:
    explicit SGUberryGasMeasurementModule(uint8_t pinMQ4);
    bool begin() override;
    bool sample() override;

    float getTemperature() const;
    float getHumidity() const;
    float getCO2() const;

    double getMethane() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_SGUBERRYGASMEASUREMENTMODULE_H
