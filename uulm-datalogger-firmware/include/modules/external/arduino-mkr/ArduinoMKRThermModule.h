//
// Created by Falko Schmidt on 21.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_ARDUINOMKRTHERMMODULE_H
#define UULM_DATALOGGER_FIRMWARE_ARDUINOMKRTHERMMODULE_H


#include "modules/external/ExternalModule.h"
#include "MKRTHERM.h"

class ArduinoMKRThermModule : public ExternalModule {

private:
    // Member variable to access outdoor temperature sensor
    THERMClass* outdoorSensor;

    // Member variables for sampling values
    double outdoorTemperature;
    double referenceTemperature;

public:
    explicit ArduinoMKRThermModule();
    bool begin() override;
    bool sample() override;

private:
    double getOutdoorTemperature() const;
    double getReferenceTemperature() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_ARDUINOMKRTHERMMODULE_H
