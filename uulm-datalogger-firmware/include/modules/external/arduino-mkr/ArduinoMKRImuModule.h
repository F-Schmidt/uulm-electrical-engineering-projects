//
// Created by Falko Schmidt on 21.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_ARDUINOMKRIMUMODULE_H
#define UULM_DATALOGGER_FIRMWARE_ARDUINOMKRIMUMODULE_H


#include "modules/external/ExternalModule.h"
#include "IMU.h"

class ArduinoMKRImuModule : public ExternalModule {

private:
    IMUClass* imu;
    float accelerationX, accelerationY, accelerationZ;
    float heading, roll, pitch;
    float gyroscopeX, gyroscopeY, gyroscopeZ;
    float magneticFieldX, magneticFieldY, magneticFieldZ;
    float temperature;

public:
    explicit ArduinoMKRImuModule();
    bool begin() override;
    bool sample() override;

private:
public:
    float getAccelerationX() const;

    float getAccelerationY() const;

    float getAccelerationZ() const;

    float getHeading() const;

    float getRoll() const;

    float getPitch() const;

    float getGyroscopeX() const;

    float getGyroscopeY() const;

    float getGyroscopeZ() const;

    float getMagneticFieldX() const;

    float getMagneticFieldY() const;

    float getMagneticFieldZ() const;

    float getTemperature() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_ARDUINOMKRIMUMODULE_H
