//
// Created by Falko Schmidt on 21.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_ARDUINOMKRENVMODULE_H
#define UULM_DATALOGGER_FIRMWARE_ARDUINOMKRENVMODULE_H


#include "modules/external/ExternalModule.h"
#include "MKRENV.h"

class ArduinoMKREnvModule : public ExternalModule {

private:
    ENVClass* env;
    double temperature;
    double humidity;
    double pressure;
    double illuminance;
    double uva;
    double uvb;
    double uvIndex;

public:
    explicit ArduinoMKREnvModule();
    bool begin() override;
    bool sample() override;

private:
    double getTemperature() const;
    double getHumidity() const;
    double getIlluminance() const;
    double getUva() const;
    double getUvb() const;
    double getUvIndex() const;

public:
    double getPressure() const;
};


#endif //UULM_DATALOGGER_FIRMWARE_ARDUINOMKRENVMODULE_H
