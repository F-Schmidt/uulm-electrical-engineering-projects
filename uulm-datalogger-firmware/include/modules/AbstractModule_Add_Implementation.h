//
// Created by Falko Alrik Schmidt on 20.11.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_ABSTRACTMODULE_ADD_IMPLEMENTATION_H
#define UULM_DATALOGGER_FIRMWARE_ABSTRACTMODULE_ADD_IMPLEMENTATION_H

#include <sstream>
#include "util/datafields/NumericDatafield.h"
#include "util/datafields/StringDatafield.h"
#include "util/datafields/BooleanDatafield.h"
#include "util/datafields/FloatingPointDatafield.h"

/**
 * MUST BE INLINE!
 * @param description
 * @param unit
 * @return
 */
inline std::string createHeader(const std::string &description, const std::string &unit) {
    // ------------------------------------------------------------
    // Construct the header string which has this format:
    // ------------------------------------------------------------
    // RP2040 Core Temperature [C]
    //     ^                    ^
    // Description             Unit
    // ------------------------------------------------------------
    std::stringstream ss;
    ss << description << ' ' << '[' << unit << ']';
    return ss.str();
}

/**
 * FLOAT
 * @tparam T
 * @param header
 * @param unit
 * @param func
 * @param instance
 * @param precision
 */
template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit, float (T::*func)(),
                                  T *instance, int precision) {
    this->datafields.push_back(new FloatingPointDatafield<float>(
            createHeader(header, unit),
            std::bind(func, instance),
            precision
    ));
}


/**
 * DOUBLE
 * @tparam T
 * @param header
 * @param unit
 * @param func
 * @param instance
 * @param precision
 */
template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit, double (T::*func)(),
                                  T *instance, int precision) {
    this->datafields.push_back(new FloatingPointDatafield<double>(
            createHeader(header, unit),
            std::bind(func, instance),
            precision
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  double (T::*func)() const, T *instance, int precision) {
    this->datafields.push_back(new FloatingPointDatafield<double>(
            createHeader(header, unit),
            std::bind(func, instance),
            precision
    ));
}


template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  float (T::*func)() const, T *instance, int precision) {
    this->datafields.push_back(new FloatingPointDatafield<double>(
            createHeader(header, unit),
            std::bind(func, instance),
            precision
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  bool (T::*func)(), T *instance,
                                  enum BooleanDatafield::OutputType outputType) {
    this->datafields.push_back(new BooleanDatafield(
            createHeader(header, unit),
            std::bind(func, instance),
            outputType
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  bool (T::*func)() const, T *instance,
                                  enum BooleanDatafield::OutputType outputType) {
    this->datafields.push_back(new BooleanDatafield(
            createHeader(header, unit),
            std::bind(func, instance),
            outputType
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  std::string (T::*func)(), T *instance) {
    this->datafields.push_back(new StringDatafield(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}


template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  std::string (T::*func)() const, T *instance) {
    this->datafields.push_back(new StringDatafield(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}


template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  char (T::*func)(), T *instance) {
    this->datafields.push_back(new NumericDatafield<char>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  char (T::*func)() const, T *instance) {
    this->datafields.push_back(new NumericDatafield<char>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  uint8_t (T::*func)(), T *instance) {
    this->datafields.push_back(new NumericDatafield<uint8_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  uint16_t (T::*func)(), T *instance) {
    this->datafields.push_back(new NumericDatafield<uint16_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  uint32_t (T::*func)(), T *instance) {
    this->datafields.push_back(new NumericDatafield<uint32_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  uint64_t (T::*func)(), T *instance) {
    this->datafields.push_back(new NumericDatafield<uint64_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  int8_t (T::*func)(), T *instance) {
    this->datafields.push_back(new NumericDatafield<int8_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  int16_t (T::*func)(), T *instance) {
    this->datafields.push_back(new NumericDatafield<int16_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  int32_t (T::*func)(), T *instance) {
    this->datafields.push_back(new NumericDatafield<int32_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  int64_t (T::*func)(), T *instance) {
    this->datafields.push_back(new NumericDatafield<int64_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void
AbstractModule::addDatafield(const std::string &header, const std::string &unit, uint8_t (T::*func)() const, T *instance) {
    this->datafields.push_back(new NumericDatafield<uint8_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit, uint16_t (T::*func)() const,
                                  T *instance) {
    this->datafields.push_back(new NumericDatafield<uint16_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit, uint32_t (T::*func)() const,
                                  T *instance) {
    this->datafields.push_back(new NumericDatafield<uint32_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void AbstractModule::addDatafield(const std::string &header, const std::string &unit,
                                  uint64_t (T::*func)() const,
                                  T *instance) {
    this->datafields.push_back(new NumericDatafield<uint64_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void
AbstractModule::addDatafield(const std::string &header, const std::string &unit, int8_t (T::*func)() const, T *instance) {
    this->datafields.push_back(new NumericDatafield<int8_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void
AbstractModule::addDatafield(const std::string &header, const std::string &unit, int16_t (T::*func)() const, T *instance) {
    this->datafields.push_back(new NumericDatafield<int16_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void
AbstractModule::addDatafield(const std::string &header, const std::string &unit, int32_t (T::*func)() const, T *instance) {
    this->datafields.push_back(new NumericDatafield<int32_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}

template<class T>
void
AbstractModule::addDatafield(const std::string &header, const std::string &unit, int64_t (T::*func)() const, T *instance) {
    this->datafields.push_back(new NumericDatafield<int64_t>(
            createHeader(header, unit),
            std::bind(func, instance)
    ));
}


#endif //UULM_DATALOGGER_FIRMWARE_ABSTRACTMODULE_ADD_IMPLEMENTATION_H
