#ifndef UULM_STRATOLOGGER_ABSTRACTMODULE_H
#define UULM_STRATOLOGGER_ABSTRACTMODULE_H

// ====================================================================================================
// Created by Falko Schmidt on 25.05.22.
// ====================================================================================================
// Imports related to C/C++ STL
#include <string>
#include <vector>
#include <ostream>
// Imports related to this datalogger framework
#include "util/general/Nameable.h"
#include "util/general/Describable.h"
#include "util/general/Beginnable.h"
#include "util/general/Enableable.h"
#include "util/general/Errno.h"
#include "util/datafields/GenericDatafield.h"
#include "util/datafields/BooleanDatafield.h"
// ====================================================================================================

class AbstractModule : public Nameable, public Describable, public Beginnable, public Enableable,
                       public Errno {

// ====================================================================================================
// E N U M - D E F I N I T I O N S
// ====================================================================================================
protected:
    /**
     * Indicates whether this module is connected internally (MCU-related)
     * or externally (PCB / HAT).
     */
    enum ModuleType {
        INTERNAL = 0x00,
        EXTERNAL = 0x01
    };

// ====================================================================================================
// F I E L D S
// ====================================================================================================
private:
    /**
     * Module type indication according to the enum defined above.
     */
    enum ModuleType type;

    /**
     * All datafields related to this module. A datafield consists of
     * a header (string) and a function which will be called to receive
     * data collected by this module.
     */
    std::vector<GenericDatafield*> datafields;

// ====================================================================================================
// M E T H O D S
// ====================================================================================================

// ----------------------------------------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------------------------------------
protected:
    explicit AbstractModule(const std::string &name, const std::string &description, ModuleType type);

// ----------------------------------------------------------------------------------------------------
// Getters
// ----------------------------------------------------------------------------------------------------
public:
    // Getters for attributes
    ModuleType getType() const;
    const std::vector<GenericDatafield *> &getDatafields() const;

// ----------------------------------------------------------------------------------------------------
// Necessary methods
// These functions MUST be overwritten in every subclass
// ----------------------------------------------------------------------------------------------------
public:
    bool begin() override = 0;
    virtual bool sample() = 0;

// ----------------------------------------------------------------------------------------------------
// Optional methods for error detection / error indication:
// This function MAY be overwritten in every subclass.
//
// Interface: Errno
// ----------------------------------------------------------------------------------------------------
public:
    bool supportsErrno() const override;
    std::string getErrorStr() const override;

// ----------------------------------------------------------------------------------------------------
// Optional methods for enabling a module (for example using a switch or button).
// This function MAY be overwritten in every subclass.
//
// Interface: Enableable
// ----------------------------------------------------------------------------------------------------
public:
    bool isEnabled() override;

// ----------------------------------------------------------------------------------------------------
// Add-Methods
// ----------------------------------------------------------------------------------------------------
    /*
     * Add datafields returning FLOAT
     */
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            float (T::*func)(), T *instance, int precision = 2);
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            float (T::*func)() const, T *instance, int precision = 2);

    /*
     * Add datafields returning DOUBLE
     */
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            double (T::*func)(), T *instance, int precision = 2);
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            double (T::*func)() const, T *instance, int precision = 2);

    /*
     * Add datafields returning BOOL
     */
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            bool (T::*func)(), T *instance, enum BooleanDatafield::OutputType outputType);
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            bool (T::*func)() const, T *instance, enum BooleanDatafield::OutputType outputType);

    /*
     * Add datafields returning STRING (object)
     */
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            std::string (T::*func)(), T *instance);
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            std::string (T::*func)() const, T *instance);

    /*
     * Add datafields returning char
     */
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            char (T::*func)(), T *instance);
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            char (T::*func)() const, T *instance);


    /*
     * Add datafields returning uint8_t (unsigned 8 bit integer)
     */
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            uint8_t (T::*func)(), T *instance);
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            uint8_t (T::*func)() const, T *instance);

    /*
     * Add datafields returning uint16_t (unsigned 16 bit integer)
     */
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            uint16_t (T::*func)(), T *instance);
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            uint16_t (T::*func)() const, T *instance);

    /*
     * Add datafields returning uint32_t (unsigned 32 bit integer)
     */
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            uint32_t (T::*func)(), T *instance);
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            uint32_t (T::*func)() const, T *instance);

    /*
     * Add datafields returning uint64_t (unsigned 64 bit integer)
     */
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            uint64_t (T::*func)(), T *instance);
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            uint64_t (T::*func)() const, T *instance);

    /*
     * Add datafields returning int8_t (signed 8 bit integer)
     */
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            int8_t (T::*func)(), T *instance);
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            int8_t (T::*func)() const, T *instance);

    /*
     * Add datafields returning int16_t (signed 16 bit integer)
     */
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            int16_t (T::*func)(), T *instance);
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            int16_t (T::*func)() const, T *instance);

    /*
     * Add datafields returning int32_t (signed 32 bit integer)
     */
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            int32_t (T::*func)(), T *instance);
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            int32_t (T::*func)() const, T *instance);

    /*
     * Add datafields returning int64_t (signed 64 bit integer)
     */
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            int64_t (T::*func)(), T *instance);
    template<class T> void addDatafield(const std::string& header, const std::string& unit,
            int64_t (T::*func)() const, T *instance);

};

// ----------------------------------------------------------------------------------------------------
// !!! DO NOT REMOVE !!!
// The following header file contains the implementations of the generic functions defined above
// ----------------------------------------------------------------------------------------------------
#include "AbstractModule_Add_Implementation.h"

#endif //UULM_STRATOLOGGER_ABSTRACTMODULE_H
