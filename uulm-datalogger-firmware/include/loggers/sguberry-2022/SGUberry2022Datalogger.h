//
// Created by Falko Schmidt on 23.09.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_SGUBERRY2022DATALOGGER_H
#define UULM_DATALOGGER_FIRMWARE_SGUBERRY2022DATALOGGER_H


#include "loggers/AbstractDatalogger.h"

class SGUberry2022Datalogger : public AbstractDatalogger {

public:
    explicit SGUberry2022Datalogger();

    bool begin() override;
};


#endif //UULM_DATALOGGER_FIRMWARE_SGUBERRY2022DATALOGGER_H
