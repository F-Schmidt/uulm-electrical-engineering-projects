//
// Created by Falko Schmidt on 04.06.22.
//

#ifndef UULM_STRATOLOGGER_STRATBERRYPROTOTYPEDATALOGGER_H
#define UULM_STRATOLOGGER_STRATBERRYPROTOTYPEDATALOGGER_H

#include "loggers/AbstractDatalogger.h"

class StratberryPrototypeDatalogger : public AbstractDatalogger {

public:
    explicit StratberryPrototypeDatalogger();

};


#endif //UULM_STRATOLOGGER_STRATBERRYPROTOTYPEDATALOGGER_H
