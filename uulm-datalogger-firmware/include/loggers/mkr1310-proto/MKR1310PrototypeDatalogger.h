//
// Created by Falko Schmidt on 21.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_MKR1310PROTOTYPEDATALOGGER_H
#define UULM_DATALOGGER_FIRMWARE_MKR1310PROTOTYPEDATALOGGER_H

#include "loggers/AbstractDatalogger.h"

class MKR1310PrototypeDatalogger : public AbstractDatalogger {

public:
    explicit MKR1310PrototypeDatalogger();

private:
    static bool configureBatteryManagement();

};


#endif //UULM_DATALOGGER_FIRMWARE_MKR1310PROTOTYPEDATALOGGER_H
