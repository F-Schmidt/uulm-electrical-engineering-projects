//
// Created by Falko Schmidt on 01.09.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_TEMPBERRYPROTOTYPEDATALOGGER_H
#define UULM_DATALOGGER_FIRMWARE_TEMPBERRYPROTOTYPEDATALOGGER_H


#include "loggers/AbstractDatalogger.h"

class TempberryPrototypeDatalogger : public AbstractDatalogger {

public:
    explicit TempberryPrototypeDatalogger();
};


#endif //UULM_DATALOGGER_FIRMWARE_TEMPBERRYPROTOTYPEDATALOGGER_H
