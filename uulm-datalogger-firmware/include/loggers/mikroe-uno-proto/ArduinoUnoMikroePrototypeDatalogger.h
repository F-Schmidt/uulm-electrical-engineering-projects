//
// Created by Falko Schmidt on 26.08.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_ARDUINOUNOMIKROEPROTOTYPEDATALOGGER_H
#define UULM_DATALOGGER_FIRMWARE_ARDUINOUNOMIKROEPROTOTYPEDATALOGGER_H

#include "loggers/AbstractDatalogger.h"
#include "components/connectors/MikroBusSocket.h"

class ArduinoUnoMikroePrototypeDatalogger : public AbstractDatalogger {

private:
    MikroBusSocket* slot1;
    MikroBusSocket* slot2;

public:
    explicit ArduinoUnoMikroePrototypeDatalogger();
};


#endif //UULM_DATALOGGER_FIRMWARE_ARDUINOUNOMIKROEPROTOTYPEDATALOGGER_H
