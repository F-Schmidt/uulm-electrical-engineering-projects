#ifndef UULM_STRATOLOGGER_ABSTRACTDATALOGGER_H
#define UULM_STRATOLOGGER_ABSTRACTDATALOGGER_H

// ====================================================================================================
// Created by Falko Schmidt on 04.06.22.
// ====================================================================================================
// Imports related to C/C++ STL
#include <vector>
#include <list>
// Imports related to this datalogger framework
#include "util/general/Beginnable.h"
#include "util/general/Nameable.h"
#include "util/general/Describable.h"
#include "debuggers/AbstractDebugger.h"
#include "handlers/AbstractHandler.h"
#include "modules/AbstractModule.h"
#include "status-indicators/AbstractStatusIndicator.h"
#include "util/records/LogRecord.h"
#include "util/records/DebuggingRecord.h"
// ====================================================================================================

class AbstractDatalogger : public Beginnable, private Nameable, private Describable {

// ====================================================================================================
// F I E L D S
// ====================================================================================================
protected:
    /**
     * The list containing all modules. Modules are used to sample sensors.
     */
    std::list<AbstractModule*> modules;

    /**
     * The list containing all handlers. Handlers are used to store data.
     */
    std::vector<AbstractHandler*> handlers;

    /**
     * The list containing all status indicators. Status indicators display the
     * program routine which is currently executed.
     */
    std::vector<AbstractStatusIndicator*> statusIndicators;

    /**
     * The list containing all debuggers. Debuggers will be used to output
     * debugging information and error indications.
     */
    std::vector<AbstractDebugger*> debuggers;

protected:
    /**
     * The delay time between two samplings in milliseconds (1000 ms = 1 s).
     */
    uint16_t delayMillis;

    /**
     * The column separator which will be used in the created .CSV file.
     */
    char columnSeparator;

// ====================================================================================================
// M E T H O D S
// ====================================================================================================

// ----------------------------------------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------------------------------------
protected:
    explicit AbstractDatalogger(const std::string& name, const std::string& description);

// ----------------------------------------------------------------------------------------------------
// Add-Methods
// ----------------------------------------------------------------------------------------------------
private:
    template<typename T>
    bool addComponent(std::vector<T*>& list, T* component);
    template<typename T>
    bool addComponent(std::list<T*>& list, T* component);
protected:
    bool addModule(AbstractModule *module);
    bool addHandler(AbstractHandler *handler);
    bool addStatusIndicator(AbstractStatusIndicator* statusIndicator);
    bool addDebugger(AbstractDebugger* debugger);

// ----------------------------------------------------------------------------------------------------
// Publish-Methods
// ----------------------------------------------------------------------------------------------------
protected:
    void publishLogRecord(const LogRecord &log);
    void publishDebuggingRecord(const DebuggingRecord &debug);

private:
    void publishProgramStatus(enum AbstractStatusIndicator::ProgramState ps, bool start);
protected:
    void publishProgramStatusStart(enum AbstractStatusIndicator::ProgramState ps);
    void publishProgramStatusFinish(enum AbstractStatusIndicator::ProgramState ps);

// ----------------------------------------------------------------------------------------------------
// Init-Methods
// ----------------------------------------------------------------------------------------------------
private:
    // Init methods
    bool initStatusIndicators();
    bool initHandlers();
    bool initModules();
    bool initDebuggers();

// ----------------------------------------------------------------------------------------------------
// Begin and Run Routine
// ----------------------------------------------------------------------------------------------------
public:
    /**
     * This function is logger-specific. All interfaces and bus systems must
     * be activated within this begin function!
     * @return true, if everything succeeded, false, if there is at least one error.
     */
    bool begin() override;

    /**
     * The main logging cycle.
     */
    [[noreturn]] void run();
};


#endif //UULM_STRATOLOGGER_ABSTRACTDATALOGGER_H
