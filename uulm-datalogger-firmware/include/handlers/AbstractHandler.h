//
// Created by Falko Schmidt on 04.06.22.
//

#ifndef UULM_STRATOLOGGER_ABSTRACTHANDLER_H
#define UULM_STRATOLOGGER_ABSTRACTHANDLER_H


#include <string>
#include "util/records/LogRecord.h"
#include "util/general/Beginnable.h"
#include "util/general/Nameable.h"

class AbstractHandler : public Nameable, Beginnable {

public:
    explicit AbstractHandler(const std::string &name);

    //virtual void writeData(const std::string& data) = 0;
    virtual void writeRecord(const LogRecord &log) = 0;

    bool begin() override = 0;
};


#endif //UULM_STRATOLOGGER_ABSTRACTHANDLER_H
