//
// Created by Falko Alrik Schmidt on 22.10.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_OPENLOGHANDLER_H
#define UULM_DATALOGGER_FIRMWARE_OPENLOGHANDLER_H

#include <Arduino.h>
#include "AbstractHandler.h"
#include "util/records/LogRecord.h"

#define             OPENLOG_DEFAULT_BAUD_RATE               (9600ul)
#define             OPENLOG_DEFAULT_ESCAPE_CHAR             (26u)
#define             OPENLOG_DEFAULT_AMOUNT_ESCAPE_CHAR      (3u)

/**
 * OpenLog driver with support for low level functions.
 * File and directory manipulation are NOT supported.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class OpenLogHandler : public AbstractHandler {

private:
    arduino::UART *uart;
    int resetPin;
    unsigned long baud;
    bool isInCommandMode;

public:
    explicit OpenLogHandler(UART *uart,     int resetPin = -1, unsigned long baud = OPENLOG_DEFAULT_BAUD_RATE);

    bool begin() override;
    void writeRecord(const LogRecord &log) override;

private:
    void reset() const;
    void softReset();
    void initSD();
    void syncSD();
    std::string readSDInformation();
    std::string getAvailableCommands();

private:
    void sendCommand(const char* cmd);

    //TODO get version number
    //  disk, init, sync,
    //  file functions (maybe...)

private:
    // TODO: TEST
    void enterCommandMode();
    bool isCommandMode() const;
    void sendEscapeSequence();
};


#endif //UULM_DATALOGGER_FIRMWARE_OPENLOGHANDLER_H
