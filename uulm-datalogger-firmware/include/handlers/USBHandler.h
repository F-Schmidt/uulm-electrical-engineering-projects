//
// Created by Falko Schmidt on 04.06.22.
//

#ifndef UULM_STRATOLOGGER_USBHANDLER_H
#define UULM_STRATOLOGGER_USBHANDLER_H


#include "AbstractHandler.h"

class USBHandler : public AbstractHandler {
public:
    explicit USBHandler(const std::string &name = "USB-Handler");

    void writeRecord(const LogRecord &log) override;

    //void writeData(const std::string& data) override;

    bool begin() override;
};


#endif //UULM_STRATOLOGGER_USBHANDLER_H
