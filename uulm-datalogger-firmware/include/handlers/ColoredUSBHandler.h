//
// Created by Falko Schmidt on 04.06.22.
//

#ifndef UULM_STRATOLOGGER_COLOREDUSBHANDLER_H
#define UULM_STRATOLOGGER_COLOREDUSBHANDLER_H


#include "USBHandler.h"

class ColoredUSBHandler : public USBHandler {

private:
    char columnSeparator;

public:
    explicit ColoredUSBHandler(char columnSeparator);
    void writeRecord(const LogRecord &log) override;
};


#endif //UULM_STRATOLOGGER_COLOREDUSBHANDLER_H
