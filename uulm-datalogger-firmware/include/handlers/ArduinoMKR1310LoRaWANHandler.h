//
// Created by Falko Alrik Schmidt on 11.12.22.
//

#ifndef UULM_DATALOGGER_FIRMWARE_ARDUINOMKR1310LORAWANHANDLER_H
#define UULM_DATALOGGER_FIRMWARE_ARDUINOMKR1310LORAWANHANDLER_H


#include "AbstractHandler.h"
#include "util/records/LogRecord.h"

class ArduinoMKR1310LoRaWANHandler : public AbstractHandler {
public:
    explicit ArduinoMKR1310LoRaWANHandler();

    void writeRecord(const LogRecord &log) override;
    bool begin() override;
};


#endif //UULM_DATALOGGER_FIRMWARE_ARDUINOMKR1310LORAWANHANDLER_H
