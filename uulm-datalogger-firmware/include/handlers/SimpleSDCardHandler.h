//
// Created by Falko Schmidt on 04.06.22.
//

#ifndef UULM_STRATOLOGGER_SDCARDHANDLER_H
#define UULM_STRATOLOGGER_SDCARDHANDLER_H

#include <string>
#include "AbstractHandler.h"

class SimpleSDCardHandler : public AbstractHandler {

private:
    uint8_t csPin;
    std::string dataFileName;

public:
    explicit SimpleSDCardHandler(uint8_t csPin, const std::string &dataFileName = "DATA.txt");

    bool begin() override;

    void writeRecord(const LogRecord &log) override;
};


#endif //UULM_STRATOLOGGER_SDCARDHANDLER_H
