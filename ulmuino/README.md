# ulmuino

![Ulmuino](./pcb-design/ULMuino.png)

The "ulm" implementation of an Arduino Uno with some extra "specials".

Falko Schmidt, Ulm University
