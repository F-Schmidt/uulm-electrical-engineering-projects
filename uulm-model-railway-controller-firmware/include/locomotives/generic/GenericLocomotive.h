//
// Created by Falko Alrik Schmidt on 11.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICLOCOMOTIVE_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICLOCOMOTIVE_H

#include <cstdint>
#include <Serial.h>

#include "general/Nameable.h"
#include "general/Describable.h"
#include "general/Identificable.h"
#include "general/Addressable.h"


#define         COMMAND_SET_LOCOMOTIVE_SPEED                    (0x04)
#define         COMMAND_CHANGE_LOCOMOTIVE_DIRECTION             (0x05)
#define         COMMAND_SET_LOCOMOTIVE_FUNCTION                 (0x06)

#define         GENERIC_LOCOMOTIVE_START_STOP_DELAY_OFF_FUNCTION        (0x04)

#define         LOCOMOTIVE_FULL_SPEED                           (1000)
#define         LOCOMOTIVE_HALF_SPEED                           (500)
#define         LOCOMOTIVE_SLOW_SPEED                           (250)
#define         LOCOMOTIVE_NO_SPEED                             (0)


class GenericLocomotive : public Addressable, public Nameable, public Describable {

public:

    /**
     * Refer to CAN protocol version 2.0 documentation, page 31
     */
    enum Direction {
        KEEP        = 0,
        FORWARD     = 1,
        BACKWARD    = 2,
        TOGGLE      = 3
    };

    /**
     * The epochs defined by Märklin.
     */
    enum Epoch {
        EPOCH_I     = 1,
        EPOCH_II    = 2,
        EPOCH_III   = 3,
        EPOCH_IV    = 4,
        EPOCH_V     = 5,
        EPOCH_VI    = 6
    };

    enum Type {
        STEAM_LOCOMOTIVE,
        DIESEL_LOCOMOTIVE,
        ELECTRIC_LOCOMOTIVE,
        POWERED_RAIL_CARS       // Triebwagen
    };

private:
    Epoch epoch;  // We will use the epochs defined by Märklin which range from I to VI (1 to 6).
    uint32_t articleNumber; // The number of the locomotive or the set in which it is included.
    const char* ms2Name;
    enum Type type;

public:

    explicit GenericLocomotive(const char* name, const char* description, const char* ms2Name, const uint32_t& address,
                      Epoch epoch, Type type, const uint32_t& articleNumber);

    void setDirection(enum Direction direction) const;

    void setSpeed(uint16_t speed) const;
    void stop() const;

    Type getType() const;
    const char* getTypeString();

    void toggleDirection() const;

    void setFunction(uint8_t func, bool active) const;

    bool begin() const;

    Epoch getEpoch() const;
    const char* getEpochRoman();

    uint32_t getArticleNumber() const;

    const char *getMs2Name() const;

    inline void setStartStopDelay(bool activate) const;
    inline void enableStartStopDelay() const;
    inline void disableStartStopDelay() const;


    void printInformation(Print *p = &Serial);

};


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICLOCOMOTIVE_H
