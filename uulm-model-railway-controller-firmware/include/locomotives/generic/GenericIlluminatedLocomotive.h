//
// Created by Falko Alrik Schmidt on 18.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICILLUMINATEDLOCOMOTIVE_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICILLUMINATEDLOCOMOTIVE_H


#include "locomotives/generic/GenericLocomotive.h"

#define         GENERIC_LOCOMOTIVE_HEADLIGHT_FUNCTION                  (0x00)
#define         GENERIC_LOCOMOTIVE_FLASHING_LIGHT_FUNCTION             (0x01)

/**
 * A locomotive with any light function (headlight or flash light or both)
 */
class GenericIlluminatedLocomotive : public GenericLocomotive {

private:
    bool hasHeadlight;
    bool hasFlashlight;

public:
    explicit GenericIlluminatedLocomotive(const char *name, const char *description, const char *ms2Name,
                                 const uint32_t &address, Epoch epoch, Type type, const uint32_t &articleNumber,
                                 bool hasHeadlight, bool hasFlashlight);

    void setHeadlight(bool activate);
    void enableHeadlight();
    void disableHeadlight();

    void setFlashlight(bool activate);
    void enableFlashlight();
    void disableFlashlight();

    bool begin();

};


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICILLUMINATEDLOCOMOTIVE_H
