//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_HALLOWEENGLOWLOCOMOTIVE_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_HALLOWEENGLOWLOCOMOTIVE_H


#include "GenericHolidayLocomotive.h"

class HalloweenGlowLocomotive : public GenericHolidayLocomotive {
public:
    HalloweenGlowLocomotive();
    bool begin();
};


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_HALLOWEENGLOWLOCOMOTIVE_H
