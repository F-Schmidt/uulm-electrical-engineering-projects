//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICHOLIDAYLOCOMOTIVE_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICHOLIDAYLOCOMOTIVE_H


#include "locomotives/generic/GenericLocomotive.h"

class GenericHolidayLocomotive : public GenericLocomotive {

public:
    enum Holiday {
        EASTER,
        HALLOWEEN,
        CHRISTMAS
    };

private:
    const enum Holiday holiday;

public:
    GenericHolidayLocomotive(const char *name, const char *description, const char *ms2Name, const uint32_t &address,
                             Epoch epoch, Type type, const uint32_t &articleNumber, Holiday holiday);
    Holiday getHoliday() const;
};


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICHOLIDAYLOCOMOTIVE_H
