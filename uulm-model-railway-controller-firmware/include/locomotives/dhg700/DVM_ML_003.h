//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_DVM_ML_003_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_DVM_ML_003_H


#include "GenericDHG700Locomotive.h"

class DVM_ML_003 : public GenericDHG700Locomotive {
public:
    DVM_ML_003();
};


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_DVM_ML_003_H
