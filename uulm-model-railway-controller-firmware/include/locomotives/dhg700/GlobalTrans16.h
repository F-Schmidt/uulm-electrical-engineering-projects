//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GLOBALTRANS16_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GLOBALTRANS16_H

#include "GenericDHG700Locomotive.h"

#define         GLOBAL_TRANS_16_DRIVING_SOUND_FUNCTION              (0x02)
#define         GLOBAL_TRANS_16_SIGNAL_HORN_SOUND_FUNCTION          (0x03)

class GlobalTrans16 : public GenericDHG700Locomotive {
public:
    explicit GlobalTrans16();

    inline void setDrivingSound(bool activate);
    inline void setSignalHorn(bool activate);

    bool begin();
};


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GLOBALTRANS16_H
