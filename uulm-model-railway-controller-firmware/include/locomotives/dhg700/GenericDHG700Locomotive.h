//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICDHG700LOCOMOTIVE_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICDHG700LOCOMOTIVE_H


#include "locomotives/generic/GenericLocomotive.h"
#include "locomotives/generic/GenericIlluminatedLocomotive.h"

#define         GENERIC_DHG700_HEADLIGHT_FUNCTION                  (0x00)
#define         GENERIC_DHG700_FLASHING_LIGHT_FUNCTION             (0x01)

/**
 * This class represents a generic DHG700 locomotive.
 * All of these engines have three functions:
 * headlight, flashing light and deactivation of start/stop delay.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class GenericDHG700Locomotive : public GenericIlluminatedLocomotive {

    // Spitzenlicht, blinklicht, abv

public:
    GenericDHG700Locomotive(const char *name, const char *description, const char *ms2Name,
                            const uint32_t &address, const uint32_t &articleNumber);
};


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICDHG700LOCOMOTIVE_H
