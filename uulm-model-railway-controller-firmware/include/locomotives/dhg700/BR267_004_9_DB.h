//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_BR267_004_9_DB_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_BR267_004_9_DB_H

#include "GenericDHG700Locomotive.h"

#define         BR267_004_9_DB_DRIVING_SOUND_FUNCTION               (0x02)
#define         BR267_004_9_DB_SIGNAL_HORN_SOUND_FUNCTION           (0x03)
#define         BR267_004_9_DB_BREAK_SQUEAK_SOUND_FUNCTION          (0x05)
#define         BR267_004_9_DB_BUFFER_SOUND_FUNCTION                (0x06)
#define         BR267_004_9_DB_COUPLE_SOUND_FUNCTION                (0x07)
#define         BR267_004_9_DB_DECOUPLE_SOUND_FUNCTION              (0x08)
#define         BR267_004_9_DB_SHUNTING_HORN_SOUND_FUNCTION         (0x09)
#define         BR267_004_9_DB_SHUNTING_MODE_FUNCTION               (0x0A)
#define         BR267_004_9_DB_CONDUCTOR_WHISTLE_SOUND_FUNCTION     (0x0B)
#define         BR267_004_9_DB_RANDOM_RAIL_JOINT_SOUND_FUNCTION     (0x0C)
#define         BR267_004_9_DB_SAND_SOUND_FUNCTION                  (0x0D)

class BR267_004_9_DB : public GenericDHG700Locomotive {

    //TODO Problem beim Konstruktoraufruf der GenericDHG class!!!

public:
    BR267_004_9_DB();

    inline void setDrivingSound(bool activate);
    inline void setSignalHorn(bool activate);
    inline void setBreakSqueak(bool activate);
    inline void setBufferSound(bool activate);
    inline void setCoupleSound(bool activate);
    inline void setDecoupleSound(bool activate);
    inline void setShuntingHornSound(bool activate);
    inline void setShuntingMode(bool activate);
    inline void setConductorWhistleSound(bool activate);
    inline void setRandomRailJointSound(bool activate);
    inline void setSandSound(bool activate);

    void disableSound();

    bool begin();

};


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_BR267_004_9_DB_H
