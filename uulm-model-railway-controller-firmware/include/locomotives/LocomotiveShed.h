//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_LOCOMOTIVESHED_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_LOCOMOTIVESHED_H


#include "locomotives/generic/GenericLocomotive.h"

class LocomotiveShed {

public:
    enum LocomotiveType {

        /**
         * These locomotives are all subtypes of the DHG700 locomotive.
         */
        DHG700_GLOBAL_TRANS_16,
        DHG700_BR267_004_9_DB,
        DHG700_DVM_ML_003,

        /**
         * These locomotives have specific holiday colorings.
         */


    };

    static GenericLocomotive* parkOut(LocomotiveType type);

};


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_LOCOMOTIVESHED_H
