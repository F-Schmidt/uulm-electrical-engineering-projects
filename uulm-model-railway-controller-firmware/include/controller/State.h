//
// Created by Falko Alrik Schmidt on 21.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_STATE_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_STATE_H


class State {

public:
    void onActivation();
    void onDeactivation();

};


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_STATE_H
