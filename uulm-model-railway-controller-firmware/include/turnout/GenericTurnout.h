//
// Created by Falko Alrik Schmidt on 16.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICTURNOUT_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICTURNOUT_H

#include "stdint.h"
#include "general/Addressable.h"

#define             TURNOUT_STATE_CHANGE_COMMAND         (0x0B)

class GenericTurnout : public Addressable {

    //TODO nameable, describable

public:

    enum Type {
        LEFT,
        RIGHT
    };

    enum Direction {
        STRAIGHT,
        TURN
    };

private:
    const enum Type type;

public:
    GenericTurnout(uint32_t address, GenericTurnout::Type type);
    bool begin() const;
    void setDirection(Direction direction) const;

};


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_GENERICTURNOUT_H
