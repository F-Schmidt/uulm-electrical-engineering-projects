//
// Created by Falko Alrik Schmidt on 16.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_ADDRESSABLE_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_ADDRESSABLE_H


#include <stdint.h>

class Addressable {

private:
    uint32_t address;

public:
    explicit Addressable(uint32_t address);

    uint32_t getAddress() const;
};

#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_ADDRESSABLE_H
