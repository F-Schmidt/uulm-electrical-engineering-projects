//
// Created by Falko Alrik Schmidt on 11.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_NAMEABLE_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_NAMEABLE_H

class Nameable {

private:
    const char* name;

public:
    explicit Nameable(const char *name);
    const char *getName() const;
};


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_NAMEABLE_H
