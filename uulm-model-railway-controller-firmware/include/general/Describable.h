//
// Created by Falko Alrik Schmidt on 11.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_DESCRIBABLE_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_DESCRIBABLE_H

class Describable {

private:
    const char* description;

public:
    explicit Describable(const char *description);

    const char *getDescription() const;
};


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_DESCRIBABLE_H
