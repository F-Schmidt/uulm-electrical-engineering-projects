//
// Created by Falko Alrik Schmidt on 02.01.24.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_MARKLINCANMESSAGE_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_MARKLINCANMESSAGE_H


#include <cstdint>

class MarklinCANMessage {

private:
    uint8_t priority;
    uint8_t command;
    bool response;
    uint16_t hash;
    uint8_t dlc;    // Data Length Code (DLC)
    uint8_t data[8];

public:
    enum Priority {

    };

};


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_MARKLINCANMESSAGE_H
