//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_MAERKLINCANTRANSCEIVER_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_MAERKLINCANTRANSCEIVER_H

#include "CANMessage.h"
#include "MCP2515.h"

class MaerklinCANTransceiver : public MCP2515Class {

private:
    uint16_t hash;
    bool debug;

private:
    /**
     *
     * @param baudRate defaults to 250kbps
     * @return
     */
    int begin(long baudRate) override;

public:
    explicit MaerklinCANTransceiver(bool debug = false);
    int begin();
    void writeMessage(const CANMessage& message) const;
    static void setPower(bool activate);

    bool operator<<(CANMessage& msg);

};

//TODO debug should default to false?
extern MaerklinCANTransceiver maerklinCANTransceiver;


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_MAERKLINCANTRANSCEIVER_H
