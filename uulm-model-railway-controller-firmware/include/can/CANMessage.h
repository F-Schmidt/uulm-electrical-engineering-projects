//
// Created by Falko Alrik Schmidt on 14.10.22.
//

#ifndef UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_CANMESSAGE_H
#define UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_CANMESSAGE_H

#include <Arduino.h>
#include <stdint.h>

class CANMessage {

public:
    enum Priority {
        STANDARD = 0,
        EMERGENCY = 1,
        FEEDBACK = 2,
        LOCO_STOP = 3,
        LOCO_GENERAL = 4,
        ACCESSORY_GENERAL = LOCO_GENERAL
    };

private:

    class CANMessageBuilder4 {

    protected:
        uint8_t currentIndex = 0;
    public:

        template<typename T>
        CANMessageBuilder4& withData(uint8_t index, T t);

        template<typename T>
        CANMessageBuilder4& withData(T t);

        virtual CANMessage build() = 0;
    };

    class CANMessageBuilder3 {
    public:
        virtual CANMessageBuilder4& setDataLength(uint8_t length) = 0;
    };

    class CANMessageBuilder2 {
    public:
        virtual CANMessageBuilder3& awaitResponse(bool await) = 0;
    };

    class CANMessageBuilder1 {
    public:
        virtual CANMessageBuilder2& executeCommand(uint8_t command) = 0;
    };

    class CANMessageBuilder0 {
    public:
        virtual CANMessageBuilder1& ofType(enum Priority priority) = 0;
    };

    class CANMessageBuilder : public CANMessageBuilder0, public CANMessageBuilder1, public CANMessageBuilder2,
                              public CANMessageBuilder3, public CANMessageBuilder4 {

    private:
        uint8_t priority;   // Only the lower 4 bits will be used
        uint8_t command;
        bool response;
        unsigned char data[8]{};
        uint8_t length;     // Only the lower 4 bits will be used

    public:
        CANMessageBuilder();
        CANMessageBuilder1 &ofType(enum Priority priority) override;
        CANMessageBuilder2 &executeCommand(uint8_t command) override;
        CANMessageBuilder3 &awaitResponse(bool await) override;
        CANMessageBuilder4 &setDataLength(uint8_t length) override;

        CANMessage build() override;

        uint8_t getPriority() const;

        uint8_t getCommand() const;

        bool isResponse() const;

        const unsigned char *getData() const;

        uint8_t getLength() const;
    };


private:
    uint8_t priority;   // Only the lower 4 bits will be used
    uint8_t command;
    bool response;
    unsigned char data[8]{};
    uint8_t length;     // Only the lower 4 bits will be used

private:
    CANMessage(uint8_t priority, uint8_t command, bool response, unsigned char data[8], uint8_t length);

public:
    static CANMessageBuilder newBuilder();

    uint8_t getPriority() const;

    uint8_t getCommand() const;

    bool isResponse() const;

    const unsigned char *getData() const;

    uint8_t getLength() const;


};



template<typename T>
CANMessage::CANMessageBuilder4 &CANMessage::CANMessageBuilder4::withData(uint8_t index, T t) {
    for(size_t i = 0; i < sizeof(t); i++, index++) {
        this->withData(index, (uint8_t)(t >> (8*i)));
    }
    return *this;
}

template<typename T>
CANMessage::CANMessageBuilder4 &CANMessage::CANMessageBuilder4::withData(T t) {
    for(size_t i = 0; i < sizeof(t); i++, this->currentIndex++) {
        this->withData(this->currentIndex, (uint8_t)(t >> (8*i)));
    }
    return *this;
}


#endif //UULM_MODEL_RAILWAY_CONTROLLER_FIRMWARE_CANMESSAGE_H
