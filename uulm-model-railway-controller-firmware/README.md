# uulm-model-railway-controller-firmware

This repository contains the first version of a software implementing a Petri net to control a model train. Current state: Trains are controllable, hardware is tested, but the Petri net was not yet developed.

Falko Schmidt, Ulm University.
