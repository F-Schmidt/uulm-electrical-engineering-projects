//
// Created by Falko Alrik Schmidt on 16.10.22.
//

#include "general/Addressable.h"

Addressable::Addressable(uint32_t address) : address(address) {}

uint32_t Addressable::getAddress() const {
    return address;
}
