//
// Created by Falko Alrik Schmidt on 11.10.22.
//

#include "general/Describable.h"

Describable::Describable(const char *description) : description(description) {}

const char *Describable::getDescription() const {
    return description;
}
