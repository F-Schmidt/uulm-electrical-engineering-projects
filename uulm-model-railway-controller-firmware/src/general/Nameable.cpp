//
// Created by Falko Alrik Schmidt on 11.10.22.
//

#include "general/Nameable.h"

Nameable::Nameable(const char *name) : name(name) {}

const char *Nameable::getName() const {
    return name;
}
