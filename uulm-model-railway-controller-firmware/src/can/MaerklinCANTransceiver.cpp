//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#include "can/MaerklinCANTransceiver.h"

#define         MCP2515_CS_PIN              (10u)
#define         MCP2515_IRQ_PIN             (2u)

void MaerklinCANTransceiver::writeMessage(const CANMessage& message) const {

    long packetId = 0;

    // The hash covers the lower 16 bits
    packetId |= this->hash;

    // Prio is in bit 17
    packetId |= ((long)(message.isResponse() ? 1 : 0)) << 16;

    // Command on bits 18 to 24
    packetId |= ((long)message.getCommand()) << 17;

    // Priority on bits 25 to 29
    packetId |= ((long)(message.getPriority() & 0x0F)) << 25;

    // DLC is lower 4 bits of message length
    uint8_t dlc = message.getLength() & 0x0F;

    // Write id and dlc, after that write the amount of bytes that
    // the message contains.
    if(!CAN.beginExtendedPacket(packetId, dlc)) {
        if (this->debug) {
            Serial.println("Failed to start extended packet transmit.");
        }
        return;
    }
    for(uint8_t index = 0; index < message.getLength(); index++) {
        CAN.write(message.getData()[index]);
    }
    CAN.endPacket();

    delayMicroseconds(1000);
}

int MaerklinCANTransceiver::begin(long baudRate) {

    // Set pins according to hardware design
    this->setPins(MCP2515_CS_PIN, MCP2515_IRQ_PIN);
    if(this->debug) {
        Serial.println((String) "[INIT] Set pin CS to " + MCP2515_CS_PIN);
        Serial.println((String) "[INIT] Set pin IRQ to " + MCP2515_IRQ_PIN);
    }

    // First, we will call the super class constructor to
    // init the MCP2515 SPI device
    if(MCP2515Class::begin(baudRate)) {
        // Success
        if(this->debug) {
            Serial.println("[INIT] Setup of MCP2515 CAN transceiver complete.");
        }
    } else {
        // Error
        if(debug) {
            Serial.println("[FAIL] Failed to init MCP2515 CAN transceiver!");
        }
        return false;
    }

    // Now we need to create a hash code for CAN bus communication
    //TODO

    return true;
}

int MaerklinCANTransceiver::begin() {
    // Simple wrapper to start CAN with the correct speed (250kbps).
    return this->begin(250E3);
}

MaerklinCANTransceiver::MaerklinCANTransceiver(bool debug) {
    this->hash = 0xDF24; // This will be calculated in the begin()-function
    this->debug = debug;
}

void MaerklinCANTransceiver::setPower(bool activate) {
    maerklinCANTransceiver.writeMessage(
            CANMessage::newBuilder().ofType(CANMessage::EMERGENCY)
            .executeCommand(0x00)
            .awaitResponse(false)
            .setDataLength(5)
            .withData((uint32_t) 0x00)
            .withData(activate)
            .build()
    );

    //unsigned char data[5] = {0, 0, 0, 0, activate};
    //CANMessage msg(0, 0x00, false, data, 5);
    //this->writeMessage(msg);
}

bool MaerklinCANTransceiver::operator<<(CANMessage &msg) {
    //TODO CAN.beginExtendedPacket()
}

MaerklinCANTransceiver maerklinCANTransceiver(true);

