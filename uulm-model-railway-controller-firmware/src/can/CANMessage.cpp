//
// Created by Falko Alrik Schmidt on 14.10.22.
//

#include <Arduino.h>
#include "can/CANMessage.h"














































/*
uint8_t CANMessage::getPrio() const {
    return prio;
}

uint8_t CANMessage::getCommand() const {
    return command;
}

bool CANMessage::hasResponse() const {
    return response;
}

const unsigned char *CANMessage::getData() const {
    return data;
}

uint8_t CANMessage::getLength() const {
    return length;
}

CANMessage::CANMessage(uint8_t prio, uint8_t command, bool response, const unsigned char data[], uint8_t length) {
    this->prio = prio;
    this->command = command;
    this->response = response;
    for(uint8_t index = 0; index < 8; index++) {
        this->data[index] = data[index];
    }
    this->length = length;
}














CANMessage CANMessage::CANMessageBuilder::build() {
    return CANMessage(0, 0, false, nullptr, 0);
}

CANMessage::CANMessageStepBuilder5 *CANMessage::CANMessageBuilder::awaitResponse(bool hasResponse) {
    return nullptr;
}

CANMessage::CANMessageBuilder::CANMessageStepBuilder3* CANMessage::CANMessageBuilder::withData(uint8_t index, uint8_t data) {

}

CANMessage::CANMessageBuilder::CANMessageStepBuilder3* CANMessage::CANMessageBuilder::withData(uint8_t index, uint16_t data) {

}

CANMessage::CANMessageBuilder::CANMessageStepBuilder3* CANMessage::CANMessageBuilder::withData(uint8_t index, uint32_t data) {

}

CANMessage::CANMessageBuilder::CANMessageStepBuilder3* CANMessage::CANMessageBuilder::withData(uint64_t data) {
    for(uint8_t index = 0; index < 8; index++) {
        // MSB must be written to data[0], LSB to data[7]
        // data[0] = data >> 56;
        // data[1] = data >> 48;
        // data[2] = data >> 40;
        // ...
        this->data[index] = data >> (8 * (7 - index));
    }
}

CANMessage::CANMessageStepBuilder3 *CANMessage::CANMessageBuilder::setDataLength(uint8_t len) {
    this->length = len;
    return this;
}

CANMessage::CANMessageStepBuilder2 *CANMessage::CANMessageBuilder::executeCommand(uint8_t cmd) {
    this->command = cmd;
    return this;
}

CANMessage::CANMessageStepBuilder1 *CANMessage::CANMessageBuilder::ofType(CANMessage::Priority priority) {
    this->prio = priority;
    return this;
}

CANMessage::CANMessageBuilder CANMessage::newBuilder() {
    return {};
}

CANMessage::CANMessageBuilder1 *CANMessage::CANMessageBuilder0::ofType(CANMessage::Priority priority) {

}
*/
CANMessage::CANMessageBuilder1 &CANMessage::CANMessageBuilder::ofType(CANMessage::Priority priority) {
    this->priority = priority;
    return *this;
}

CANMessage::CANMessageBuilder::CANMessageBuilder() {
    this->priority = Priority::STANDARD;
    this->command = 0x00;
    this->response = false;
    this->length = 0x00;
}

CANMessage::CANMessageBuilder2 &CANMessage::CANMessageBuilder::executeCommand(uint8_t command) {
    this->command = command;
    return *this;
}

CANMessage::CANMessageBuilder3 &CANMessage::CANMessageBuilder::awaitResponse(bool await) {
    this->response = await;
    return *this;
}

CANMessage::CANMessageBuilder4 &CANMessage::CANMessageBuilder::setDataLength(uint8_t length) {
    this->length = length;
    return *this;
}
/*
CANMessage::CANMessageBuilder4 &CANMessage::CANMessageBuilder::withData(uint8_t index, uint8_t data) {
    if(index < 0 || index > 7) {
        return *this;
    }
    this->data[index] = data;
    return *this;
}

CANMessage::CANMessageBuilder4 &CANMessage::CANMessageBuilder::withData(uint8_t index, uint16_t data) {
    for(size_t i = 0; i < sizeof(data); i++, index++) {
        this->withData(index, (uint8_t)(data >> (8*i)));
    }
    //this->withData(index++, (uint8_t)(data >> 8));
    //this->withData(index, (uint8_t)data);
    return *this;
}

CANMessage::CANMessageBuilder4 &CANMessage::CANMessageBuilder::withData(uint8_t index, uint32_t data) {
    return *this;
}

CANMessage::CANMessageBuilder4 &CANMessage::CANMessageBuilder::withData(uint8_t index, uint64_t data) {
    return *this;
}*/

uint8_t CANMessage::CANMessageBuilder::getPriority() const {
    return priority;
}

uint8_t CANMessage::CANMessageBuilder::getCommand() const {
    return command;
}

bool CANMessage::CANMessageBuilder::isResponse() const {
    return response;
}

const unsigned char *CANMessage::CANMessageBuilder::getData() const {
    return data;
}

uint8_t CANMessage::CANMessageBuilder::getLength() const {
    return length;
}

CANMessage CANMessage::CANMessageBuilder::build() {
    return {this->priority, this->command, this->response,
            this->data, this->length};
}
/*
CANMessage::CANMessageBuilder4 &CANMessage::CANMessageBuilder::withData(uint8_t index, bool data) {
    return this->withData(index, (uint8_t)(data ? 1 : 0));
}


CANMessage::CANMessageBuilder4 &CANMessage::CANMessageBuilder::withData(uint8_t data) {
    this->withData(this->currentIndex, data);
    this->currentIndex += sizeof(uint8_t);
    return *this;
}

CANMessage::CANMessageBuilder4 &CANMessage::CANMessageBuilder::withData(uint16_t data) {
    this->withData(this->currentIndex, data);
    this->currentIndex += sizeof(uint16_t);
    return *this;
}

CANMessage::CANMessageBuilder4 &CANMessage::CANMessageBuilder::withData(uint32_t data) {
    this->withData(this->currentIndex, data);
    this->currentIndex += sizeof(uint32_t);
    return *this;
}

CANMessage::CANMessageBuilder4 &CANMessage::CANMessageBuilder::withData(uint64_t data) {
    this->withData(this->currentIndex, data);
    this->currentIndex += sizeof(uint32_t);
    return *this;
}

CANMessage::CANMessageBuilder4 &CANMessage::CANMessageBuilder::withData(bool data) {
    this->withData(this->currentIndex, (uint8_t)(data ? 1 : 0));
    this->currentIndex += sizeof(bool);
    return *this;
}

CANMessage::CANMessageBuilder4 &CANMessage::CANMessageBuilder::withData(uint8_t index, bool data) {
    return this->withData(index, (uint8_t)(data ? 1 : 0));
}*/

CANMessage::CANMessageBuilder CANMessage::newBuilder() {
    return {};
}

uint8_t CANMessage::getPriority() const {
    return priority;
}

uint8_t CANMessage::getCommand() const {
    return command;
}

bool CANMessage::isResponse() const {
    return response;
}

const unsigned char *CANMessage::getData() const {
    return data;
}

uint8_t CANMessage::getLength() const {
    return length;
}



void copyCharArray(unsigned char* destination, const unsigned char* source, uint8_t length) {
    for(uint8_t index = 0; index < length; index++) {
        destination[index] = source[index];
    }
}

CANMessage::CANMessage(uint8_t priority, uint8_t command, bool response, unsigned char *data, uint8_t length) {
    this->priority = priority;
    this->command = command;
    this->response = response;
    copyCharArray(this->data, data, sizeof(data[0]) * length);
    this->length = length;
}
