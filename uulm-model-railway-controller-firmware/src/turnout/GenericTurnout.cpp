//
// Created by Falko Alrik Schmidt on 16.10.22.
//

#include "turnout/GenericTurnout.h"
#include "can/MaerklinCANTransceiver.h"

GenericTurnout::GenericTurnout(const uint32_t address, const GenericTurnout::Type type) : Addressable(address), type(type) {
    // Nothing more to do here
}

bool GenericTurnout::begin() const {
    this->setDirection(Direction::STRAIGHT);
    return true;
}

void GenericTurnout::setDirection(GenericTurnout::Direction direction) const {

    maerklinCANTransceiver.writeMessage(
            CANMessage::newBuilder().ofType(CANMessage::ACCESSORY_GENERAL)
            .executeCommand(TURNOUT_STATE_CHANGE_COMMAND)
            .awaitResponse(false)
            .setDataLength(6)
            .withData(this->getAddress())
            .withData((uint8_t)(direction == STRAIGHT ? 0x01 : 0x00))
            .withData((uint8_t)1)   // The time that the turnout will have power in seconds
            .build()
    );
/*
    unsined char data[6];
    data[0] = this->getAddress() >> 24;
    data[1] = this->getAddress() >> 16;
    data[2] = this->getAddress() >> 8;
    data[3] = this->getAddress();
    data[4] = direction == STRAIGHT ? 0x01 : 0x00;
    data[5] = 1;

    // CANMessage(uint8_t prio, uint8_t command, bool response, const unsigned char *data, uint8_t length);
    //CANMessage msg(0x00, SWITCH_STATE_CHANGE_COMMAND, false, data, 6);
    //maerklinCANTransceiver.writeMessage(msg);
    */
}
