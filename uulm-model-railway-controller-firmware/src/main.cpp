//
// Created by Falko Alrik Schmidt on 11.10.22.
//

/*
RAM:   [========= ]  85.5% (used 1751 bytes from 2048 bytes)
Flash: [====      ]  37.6% (used 12114 bytes from 32256 bytes)
 */

#include "Arduino.h"

#include "can/MaerklinCANTransceiver.h"

#include "locomotives/generic/GenericLocomotive.h"
#include "locomotives/LocomotiveShed.h"
#include "turnout/GenericTurnout.h"

#define     ARRAY_SIZE(X)       (sizeof(X)/sizeof(X[0]))

GenericLocomotive* LOCOMOTIVES[3] = {
        LocomotiveShed::parkOut(LocomotiveShed::LocomotiveType::DHG700_BR267_004_9_DB),
        LocomotiveShed::parkOut(LocomotiveShed::LocomotiveType::DHG700_DVM_ML_003),
        LocomotiveShed::parkOut(LocomotiveShed::LocomotiveType::DHG700_GLOBAL_TRANS_16)
};

GenericTurnout* TURNOUTS[2] = {
        new GenericTurnout(0x2FFF + 1, GenericTurnout::LEFT),
        new GenericTurnout(0x2FFF + 2, GenericTurnout::RIGHT)
};

size_t amountLocomotives = ARRAY_SIZE(LOCOMOTIVES);//sizeof(LOCOMOTIVES) / sizeof(LOCOMOTIVES[0]);
size_t amountSwitches = ARRAY_SIZE(TURNOUTS);//sizeof(TURNOUTS) / sizeof(TURNOUTS[0]);

void initLocomotives() {

    // Check if locomotive list is not null
    if(amountLocomotives == 0) {
        return;
    }

    Serial.print("[INIT] Starting init process for ");
    Serial.print(amountLocomotives);
    Serial.println(" locomotives.");


    for(size_t index = 0; index < amountLocomotives; index++) {
        if(LOCOMOTIVES[index]->begin()) {
            Serial.println("[ OK ] Parked out this engine from locomotive shed: ");
            LOCOMOTIVES[index]->printInformation();
        } else {
            Serial.print("[FAIL] Failed to init locomotive '");
            Serial.print(LOCOMOTIVES[index]->getName());
            Serial.println("'.");
        }
    }
}


void initSwitches() {
    if(amountSwitches == 0) {
        return;
    }

    for(size_t index = 0; index < amountSwitches; index++) {
        if(TURNOUTS[index]->begin()) {
            Serial.print("[ OK ] Successfully set switch.");
        } else {
            Serial.print("[FAIL] Failed to init switch.");
        }
    }
}

void setup() {

    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);

    //----------------------------------------------------------------------
    // Start serial interface for debugging
    //----------------------------------------------------------------------
    Serial.begin(115200);
    while(!Serial);


    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);

    Serial.println("[INIT] USB interface set up for communication!");

    //----------------------------------------------------------------------
    // Start CAN bus circuit and power tracks
    //----------------------------------------------------------------------
    maerklinCANTransceiver.begin();
    MaerklinCANTransceiver::setPower(true);

    //----------------------------------------------------------------------
    // Init all devices that we will use the set them into a known state
    //----------------------------------------------------------------------
    initLocomotives();
    Serial.println("[INIT] Locomotive init process done.");

    initSwitches();
    Serial.println("[INIT] Turnout init process done.");

    // Give the CAN bus some time to eventually let other stations
    // send data before sending new commands from this device again.
    delay(500);
}



void loop() {

    for(size_t index = 0; index < amountLocomotives; index++) {
        LOCOMOTIVES[index]->setSpeed(LOCOMOTIVE_HALF_SPEED);
    }
    Serial.println("Half speed driving");
    delay(2000);

    for(size_t index = 0; index < amountLocomotives; index++) {
        LOCOMOTIVES[index]->stop();
    }
    Serial.println("Stopped locos.");
    delay(1000);

    for(size_t index = 0; index < amountLocomotives; index++) {
        LOCOMOTIVES[index]->toggleDirection();
    }
    Serial.println("Direction change.");
}