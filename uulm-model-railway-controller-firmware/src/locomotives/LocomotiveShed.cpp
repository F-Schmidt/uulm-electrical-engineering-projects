//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#include "locomotives/LocomotiveShed.h"
#include "locomotives/dhg700/GlobalTrans16.h"
#include "locomotives/dhg700/BR267_004_9_DB.h"
#include "locomotives/dhg700/DVM_ML_003.h"

GenericLocomotive *LocomotiveShed::parkOut(LocomotiveShed::LocomotiveType type) {
    switch (type) {
        case DHG700_BR267_004_9_DB:     return new BR267_004_9_DB();
        case DHG700_GLOBAL_TRANS_16:    return new GlobalTrans16();
        case DHG700_DVM_ML_003:         return new DVM_ML_003();
        default:                        return nullptr;
    }
}
