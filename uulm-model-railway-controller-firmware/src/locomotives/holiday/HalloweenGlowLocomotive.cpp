//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#include "locomotives/holiday/HalloweenGlowLocomotive.h"

HalloweenGlowLocomotive::HalloweenGlowLocomotive()
        : GenericHolidayLocomotive("Halloween Glow in the Dark Steam Locomotive",
                                   "A black steam locomotive with glow in the dark pattern.",
                                   "Lok Halloween",
                                   42, /* TODO CHECK!!! */
                                   EPOCH_III,
                                   STEAM_LOCOMOTIVE,
                                   36872,
                                   HALLOWEEN) {
    // Nothing more to do here
}

bool HalloweenGlowLocomotive::begin() {
    //this->stop();
   // this->setDirection(Direction::FORWARD);
    return true;
}


