//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#include "locomotives/holiday/GenericHolidayLocomotive.h"

GenericHolidayLocomotive::GenericHolidayLocomotive(const char *name, const char *description, const char *ms2Name,
                                                   const uint32_t &address, GenericLocomotive::Epoch epoch,
                                                   GenericLocomotive::Type type, const uint32_t &articleNumber,
                                                   GenericHolidayLocomotive::Holiday holiday)
       : GenericLocomotive(name, description, ms2Name, address, epoch, type, articleNumber), holiday(holiday) {
    // Nothing more to do here.
}

GenericHolidayLocomotive::Holiday GenericHolidayLocomotive::getHoliday() const {
    return holiday;
}
