//
// Created by Falko Alrik Schmidt on 11.10.22.
//
#include "locomotives/generic/GenericLocomotive.h"
#include "can/MaerklinCANTransceiver.h"

void GenericLocomotive::toggleDirection() const {
    this->setDirection(Direction::TOGGLE);
    // TODO Guter Call wäre:
    /*
     * return CANMessage.ofType(LOCO...(PRIO)
     *      .executeCommand(cmd)
     *      .withDataLength(5)
     *      .withData(LOCO_ADDR, startAtByte)
     *      .withData(direction, 5)
     *      .awaitResponse(default=true)
     *      .send();
     */
}

void GenericLocomotive::setSpeed(uint16_t speed) const {
    // ---------------------------------------------------------------------------
    // Format of the data/payload that we want to transmit:
    // D0    D1    D2    D3    D4    D5    D6    D7    D8
    // MSB    LOC-ID     LSB   MSB  SPEED  LSB   X     X
    // ---------------------------------------------------------------------------
    // D0 - D3: 4 bytes containing the address of the loco that should change
    //          the direction. MSB is located in D0, LSB in D4.
    // D4 - D5: The speed. Minimum is 0. Values to 1023 are allowed, but the
    //          maximum speed is already set for a value of 1000.
    // ---------------------------------------------------------------------------
    maerklinCANTransceiver.writeMessage(
            CANMessage::newBuilder()
            .ofType(CANMessage::LOCO_GENERAL)
            .executeCommand(COMMAND_SET_LOCOMOTIVE_SPEED)
            .awaitResponse(false)
            .setDataLength(6)
            .withData(this->getAddress())
            .withData(speed)
            .build()
    );
    /*
    unsigned char data[6];
    data[0] = this->getAddress() >> 24;
    data[1] = this->getAddress() >> 16;
    data[2] = this->getAddress() >> 8;
    data[3] = this->getAddress();
    data[4] = speed >> 8;
    data[5] = speed;
    //CANMessage msg(0, COMMAND_SET_LOCOMOTIVE_SPEED, false, data, 6);
    //maerklinCANTransceiver.writeMessage(msg);
      */
}

GenericLocomotive::GenericLocomotive(const char *name, const char *description, const char *ms2Name,
                         const uint32_t &address, GenericLocomotive::Epoch epoch, GenericLocomotive::Type type,
                         const uint32_t &articleNumber): Addressable(address), Nameable(name),
                                                         Describable(description),
                         epoch(epoch), articleNumber(articleNumber), ms2Name(ms2Name), type(type) {
    // Nothing to do here.
}

void GenericLocomotive::setDirection(GenericLocomotive::Direction direction) const {
    // ---------------------------------------------------------------------------
    // Format of the data/payload that we want to transmit:
    // D0    D1    D2    D3    D4    D5    D6    D7    D8
    // MSB    LOC-ID    LSB    DIR   X     X     X     X
    // ---------------------------------------------------------------------------
    // D0 - D3: 4 bytes containing the address of the loco that should change
    //          the direction. MSB is located in D0, LSB in D4.
    // D4:      The direction:
    //          0 = Keep current direction
    //          1 = Backward
    //          2 = Forward
    //          3 = Toggle direction
    //      other = Keep current direction / ignored
    // ---------------------------------------------------------------------------
    maerklinCANTransceiver.writeMessage(
            CANMessage::newBuilder().ofType(CANMessage::LOCO_GENERAL)
            .executeCommand(COMMAND_CHANGE_LOCOMOTIVE_DIRECTION)
            .awaitResponse(false)
            .setDataLength(5)
            .withData(this->getAddress())
            .withData(direction)
            .build()
    );
    /*
    unsigned char data[5];
    data[0] = this->getAddress() >> 24;
    data[1] = this->getAddress() >> 16;
    data[2] = this->getAddress() >> 8;
    data[3] = this->getAddress();
    data[4] = direction;*/
    //CANMessage msg(0, COMMAND_CHANGE_LOCOMOTIVE_DIRECTION, false, data, 5);
    //maerklinCANTransceiver.writeMessage(msg);

    //TODO check if direction change was successful
}

void GenericLocomotive::stop() const {
    this->setSpeed(LOCOMOTIVE_NO_SPEED);
}

void GenericLocomotive::setFunction(uint8_t func, bool active) const {

/*    CANMessage::newBuilder()

    CANMessage::CANMessageBuilder().ofType(CANMessage::LOCO_GENERAL)
            ->executeCommand(COMMAND_SET_LOCOMOTIVE_FUNCTION)
            ->setDataLength(6)
            ->withData(this->getAddress())
            ->withData(func)
            ->withData(active ? 0x01 : 0x00);*/

    maerklinCANTransceiver.writeMessage(
            CANMessage::newBuilder()
            .ofType(CANMessage::LOCO_GENERAL)
            .executeCommand(COMMAND_SET_LOCOMOTIVE_FUNCTION)
            .awaitResponse(false)
            .setDataLength(6)
            .withData(this->getAddress())
            .withData(func)
            .withData(active)
            .build()
    );

    /*
    unsigned char data[6];
    data[0] = this->getAddress() >> 24;
    data[1] = this->getAddress() >> 16;
    data[2] = this->getAddress() >> 8;
    data[3] = this->getAddress();
    data[4] = func;
    data[5] = active ? 1 : 0;
    //CANMessage msg(0, COMMAND_SET_LOCOMOTIVE_FUNCTION, false, data, 6);
    //maerklinCANTransceiver.writeMessage(msg);
     */
}

GenericLocomotive::Type GenericLocomotive::getType() const {
    return type;
}

GenericLocomotive::Epoch GenericLocomotive::getEpoch() const {
    return epoch;
}

uint32_t GenericLocomotive::getArticleNumber() const {
    return articleNumber;
}

const char *GenericLocomotive::getMs2Name() const {
    return ms2Name;
}

const char *GenericLocomotive::getTypeString() {
    switch (this->type) {
        case STEAM_LOCOMOTIVE:      return "steam locomotive";
        case DIESEL_LOCOMOTIVE:     return "diesel locomotive";
        case ELECTRIC_LOCOMOTIVE:   return "electric locomotive";
        case POWERED_RAIL_CARS:     return "Powered rail car";
        default:                    return "N/A";
    }
}

const char *GenericLocomotive::getEpochRoman() {
    switch (this->epoch) {
        case EPOCH_I:   return "I";
        case EPOCH_II:  return "II";
        case EPOCH_III: return "III";
        case EPOCH_IV:  return "IV";
        case EPOCH_V:   return "V";
        case EPOCH_VI:  return "VI";
        default:        return "N/A";
    }
}

void GenericLocomotive::setStartStopDelay(bool activate) const {
    // If we want to ACTIVATE the delay, then we must DEACTIVATE this function,
    // because the function turns the delay OFF!
    // So we will invert the given boolean value.
    this->setFunction(GENERIC_LOCOMOTIVE_START_STOP_DELAY_OFF_FUNCTION, !activate);
}

void GenericLocomotive::enableStartStopDelay() const {
    this->setStartStopDelay(true);
}

void GenericLocomotive::disableStartStopDelay() const {
    this->setStartStopDelay(false);
}

bool GenericLocomotive::begin() const {
    // In case that the train 'remembers' any previously set speed,
    // we will stop the locomotive first.
    this->setSpeed(0);

    // Initially we want to drive forward (in the direction of the
    // 'long' part of the locomotive).
    this->setDirection(Direction::FORWARD);

    // We want direct control, so the delay on acceleration must
    // be turned off.
    this->disableStartStopDelay();

    //TODO error detect
    return true;
}


void GenericLocomotive::printInformation(Print *p) {
    if(p == nullptr) {
        return;
    }

    p->print("NAME        : ");     p->println(this->getName());
    p->print("MS2-NAME    : ");     p->println(this->getMs2Name());
    p->print("DESCRIPTION : ");     p->println(this->getDescription());
    p->print("TYPE        : ");     p->println(this->getTypeString());
    p->print("ADDRESS     : 0x");   p->println(this->getAddress(), HEX);
    p->print("EPOCH       : ");     p->println(this->getEpochRoman());
    p->print("ART.-NR.    : ");     p->println(this->getArticleNumber());

}
