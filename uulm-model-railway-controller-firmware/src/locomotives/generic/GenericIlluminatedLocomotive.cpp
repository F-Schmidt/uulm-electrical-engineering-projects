//
// Created by Falko Alrik Schmidt on 18.10.22.
//

#include "locomotives/generic/GenericIlluminatedLocomotive.h"

GenericIlluminatedLocomotive::GenericIlluminatedLocomotive(const char *name, const char *description,
                                                           const char *ms2Name, const uint32_t &address,
                                                           GenericLocomotive::Epoch epoch, GenericLocomotive::Type type,
                                                           const uint32_t &articleNumber, bool hasHeadlight,
                                                           bool hasFlashlight) : GenericLocomotive(name, description,
                                                                                                   ms2Name, address,
                                                                                                   epoch, type,
                                                                                                   articleNumber),
                                                                                 hasHeadlight(hasHeadlight),
                                                                                 hasFlashlight(hasFlashlight) {}

void GenericIlluminatedLocomotive::setHeadlight(bool activate) {
    if(this->hasHeadlight) {
        this->setFunction(GENERIC_LOCOMOTIVE_HEADLIGHT_FUNCTION, activate);
    }
}

void GenericIlluminatedLocomotive::enableHeadlight() {
    this->setHeadlight(true);
}

void GenericIlluminatedLocomotive::disableHeadlight() {
    this->setHeadlight(false);
}

void GenericIlluminatedLocomotive::setFlashlight(bool activate) {
    if(this->hasFlashlight) {
        this->setFunction(GENERIC_LOCOMOTIVE_FLASHING_LIGHT_FUNCTION, activate);
    }
}

void GenericIlluminatedLocomotive::enableFlashlight() {
    this->setFlashlight(true);
}

void GenericIlluminatedLocomotive::disableFlashlight() {
    this->setFlashlight(false);
}

bool GenericIlluminatedLocomotive::begin() {
    // Call method in super class to set direction and speed
    GenericLocomotive::begin();

    // Front light enable
    this->enableHeadlight();

    // Disable the flashing light. This will be used to
    // mark the MOVING train!
    this->disableFlashlight();

    //TODO error detection

    return true;
}
