//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#include "locomotives/dhg700/BR267_004_9_DB.h"

BR267_004_9_DB::BR267_004_9_DB() : GenericDHG700Locomotive("DHG700 BR267 004-9 DB AG",
                       "A small locomotive of type DHG 700 in the colors of the DB AG (red).",
                       "367 004-9 DB AG",
                       0x4005,
                       29469) {
    // Nothing more to do here.
}

void BR267_004_9_DB::setDrivingSound(bool activate) {
    this->setFunction(BR267_004_9_DB_DRIVING_SOUND_FUNCTION, activate);
}

void BR267_004_9_DB::setSignalHorn(bool activate) {
    this->setFunction(BR267_004_9_DB_SIGNAL_HORN_SOUND_FUNCTION, activate);
}

void BR267_004_9_DB::setBreakSqueak(bool activate) {
    this->setFunction(BR267_004_9_DB_BREAK_SQUEAK_SOUND_FUNCTION, activate);
}

void BR267_004_9_DB::setBufferSound(bool activate) {
    this->setFunction(BR267_004_9_DB_BUFFER_SOUND_FUNCTION, activate);
}

void BR267_004_9_DB::setCoupleSound(bool activate) {
    this->setFunction(BR267_004_9_DB_COUPLE_SOUND_FUNCTION, activate);
}

void BR267_004_9_DB::setDecoupleSound(bool activate) {
    this->setFunction(BR267_004_9_DB_DECOUPLE_SOUND_FUNCTION, activate);
}

void BR267_004_9_DB::setShuntingHornSound(bool activate) {
    this->setFunction(BR267_004_9_DB_SHUNTING_HORN_SOUND_FUNCTION, activate);
}

void BR267_004_9_DB::setShuntingMode(bool activate) {
    this->setFunction(BR267_004_9_DB_SHUNTING_MODE_FUNCTION, activate);
}

void BR267_004_9_DB::setConductorWhistleSound(bool activate) {
    this->setFunction(BR267_004_9_DB_CONDUCTOR_WHISTLE_SOUND_FUNCTION, activate);
}

void BR267_004_9_DB::setRandomRailJointSound(bool activate) {
    this->setFunction(BR267_004_9_DB_RANDOM_RAIL_JOINT_SOUND_FUNCTION, activate);
}

void BR267_004_9_DB::setSandSound(bool activate) {
    this->setFunction(BR267_004_9_DB_SAND_SOUND_FUNCTION, activate);
}

void BR267_004_9_DB::disableSound() {
    this->setDrivingSound(false);
    this->setSignalHorn(false);
    this->setBreakSqueak(false);
    this->setBufferSound(false);
    this->setCoupleSound(false);
    this->setDecoupleSound(false);
    this->setShuntingHornSound(false);
    this->setConductorWhistleSound(false);
    this->setRandomRailJointSound(false);
    this->setSandSound(false);
}

bool BR267_004_9_DB::begin() {

    // First, we call the super class function to
    // setup speed, light, direction,...
    if(!GenericIlluminatedLocomotive::begin()) {
        return false;
    }

    // Use normal mode (not shunting mode)
    this->setShuntingMode(false); //TODO CHECK IF THIS IS CORRECT??? or invert?

    // Disable all sounds
    this->disableSound();

    //TODO error check

    return true;
}




