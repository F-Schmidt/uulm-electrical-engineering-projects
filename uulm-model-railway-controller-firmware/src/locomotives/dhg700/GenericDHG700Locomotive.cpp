//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#include "locomotives/dhg700/GenericDHG700Locomotive.h"

GenericDHG700Locomotive::GenericDHG700Locomotive(const char *name, const char *description, const char *ms2Name,
                                                 const uint32_t &address, const uint32_t &articleNumber)
        : GenericIlluminatedLocomotive(name, description, ms2Name, address, Epoch::EPOCH_VI,
                            Type::DIESEL_LOCOMOTIVE, articleNumber, true, true) {
    // Nothing more to do here.
}
