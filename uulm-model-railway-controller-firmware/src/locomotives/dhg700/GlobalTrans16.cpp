//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#include "locomotives/dhg700/GlobalTrans16.h"

GlobalTrans16::GlobalTrans16() : GenericDHG700Locomotive("GlobalTrans 16 (DHG 700, blue)",
                   "A small blue locomotive of type DHG 700 from the starter pack 'container train'",
                   "GlobalTrans 16",
                   0x4006,
                   29453) {
    // Nothing more to set here
}

void GlobalTrans16::setDrivingSound(bool activate) {
    this->setFunction(GLOBAL_TRANS_16_DRIVING_SOUND_FUNCTION, activate);
}

void GlobalTrans16::setSignalHorn(bool activate) {
    this->setFunction(GLOBAL_TRANS_16_SIGNAL_HORN_SOUND_FUNCTION, activate);
}

bool GlobalTrans16::begin() {

    // Init generic stuff like direction and lights.
    if(!GenericDHG700Locomotive::begin()) {
        return false;
    }

    // Deactivate all sound functions.
    this->setDrivingSound(false);
    this->setSignalHorn(false);

    //TODO detect if sounds were deactivated.
    return true;
}
