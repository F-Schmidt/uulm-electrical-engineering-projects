//
// Created by Falko Alrik Schmidt on 15.10.22.
//

#include "locomotives/dhg700/DVM_ML_003.h"

DVM_ML_003::DVM_ML_003() : GenericDHG700Locomotive("DHG 700 DVM ML 003",
           "A red locomotive of type DHG 700 with orange stripe.",
           "ML 003",
           0x4007,
           36700) {
    // Nothing more to do here.
}
