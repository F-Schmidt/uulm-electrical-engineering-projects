# MiniALU-UULM

![MiniALU](./pcb-design/MiniALU_UULM_Top.png)

This project implements a basic circuit, that can add and subtract two 4 bit wide numbers.
Created for several events at the university of Ulm.

Developed by Falko Schmidt
