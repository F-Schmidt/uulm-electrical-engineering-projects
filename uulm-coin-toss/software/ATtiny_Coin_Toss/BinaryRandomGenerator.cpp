//
// Created by Falko Schmidt on 11.02.22.
//

#include "BinaryRandomGenerator.h"


BinaryRandomGenerator::BinaryRandomGenerator(const uint32_t seed) {
    this->seed        = seed;
    this->registerVal = 0; 
}


uint8_t BinaryRandomGenerator::getRandom() {
    this->registerVal = ((this->seed >> 0) ^ (this->seed >> 2) ^ (this->seed >> 3) ^ (this->seed >> 5)) & 1u;
    this->seed = (this->seed >> 1) | (this->registerVal << 15);
    return this->seed & 0x1;
}
