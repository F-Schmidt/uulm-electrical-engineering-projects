//
// Created by Falko Schmidt on 11.02.22.
//

#ifndef ARDUINORANDOMCUBE_RANDOMGENERATOR_H
#define ARDUINORANDOMCUBE_RANDOMGENERATOR_H


#include <stdint.h>

class BinaryRandomGenerator {

private:
    uint32_t seed;
    uint32_t registerVal;

public:
    explicit BinaryRandomGenerator(uint32_t seed);
    uint8_t getRandom();

};


#endif //ARDUINORANDOMCUBE_RANDOMGENERATOR_H
