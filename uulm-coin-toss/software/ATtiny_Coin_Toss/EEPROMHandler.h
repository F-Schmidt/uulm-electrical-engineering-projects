//
// Created by Falko Schmidt on 20.02.22.
//

#ifndef ATTINY_DICE_EEPROMHANDLER_H
#define ATTINY_DICE_EEPROMHANDLER_H

#include <stdint.h>

class EEPROMHandler {

public:
    explicit EEPROMHandler();
    void save(uint64_t val, uint8_t start);
    uint64_t load(uint8_t start);
};


#endif //ATTINY_DICE_EEPROMHANDLER_H
