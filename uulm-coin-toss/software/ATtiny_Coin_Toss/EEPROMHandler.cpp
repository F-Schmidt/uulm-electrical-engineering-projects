//
// Created by Falko Schmidt on 20.02.22.
//

#include <EEPROM.h>
#include "EEPROMHandler.h"

EEPROMHandler::EEPROMHandler() {
    // Nothing to do here
}

void EEPROMHandler::save(const uint64_t val, const uint8_t start) {
    for(uint8_t addr = start; addr < (uint8_t)(start + sizeof(uint64_t)); addr++) {
        EEPROM.update(addr, (uint8_t) (val >> (addr*8)));
    }
}

uint64_t EEPROMHandler::load(const uint8_t start) {
    uint64_t read = 0;
    for(uint8_t addr = start; addr < (uint8_t)(sizeof(uint64_t)); addr++) {
        read |= (uint8_t)(EEPROM.read(addr) << (addr*8));
    }
    return read;
}

