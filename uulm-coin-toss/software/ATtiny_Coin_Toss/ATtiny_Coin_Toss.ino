// ==============================================================================================================
// Pseudo-Random Dice for ATtiny25/45/85 microcontroller
// Created by Falko Schmidt, University of Ulm, GERMANY
// ==============================================================================================================
#include <Arduino.h>                // General header for Arduino devices / Atmel microcontrollers
#include "BinaryRandomGenerator.h"  // Linear congruential generator implementation used in this example
#include "EEPROMHandler.h"          // This class handles store and read operations for the eeprom

// ==============================================================================================================
// Definitions for the button that will be pressed to roll the dice
// PIN_BUTTON_INPUT  = The pin which the button is connected to
// BUTTON_PRESSED    = The state which the button has, when pressed. (Either HIGH or LOW)
// PIN_LED_HEAD      = The pin which the led representing a head toss is connected to
// PIN_LED_TAIL      = The pin which the led representing a tail toss is connected to
// ==============================================================================================================
#define         BUTTON_PRESSED            (LOW)
#define         PIN_BUTTON_INPUT          (PB3)
#define         PIN_LED_HEAD              (PB2)
#define         PIN_LED_TAIL              (PB0)

// ==============================================================================================================
// Definitions for delays
// DELAY_CYCLE       = The delay time between two executions of the loop-function
// ROLL_DISPLAY_TIME = The time that the rolled value is displayed
// ==============================================================================================================
#define         DELAY_CYCLE               (50)
#define         ROLL_DISPLAY_TIME         (4000)

// ==============================================================================================================
// Initialization of all objects needed.
// eepromHandler    -> instance of class handling load and save operations on EEPROM
// randomGenerator  -> the linear congruential generator used for pseudo-random values
// diceDisplay      -> instance of class controlling dice display LEDs
//
// NOTE: EEPROMHandler must be initialized before randomGenerator, because the randomGenerator uses a 
//       seed read from EEPROM
// ==============================================================================================================
EEPROMHandler eepromHandler;
BinaryRandomGenerator randomGenerator(eepromHandler.load(0));

// ==============================================================================================================
// Variables needed for program execution
// ==============================================================================================================
uint8_t shuffleEffectValue = 0;   // The value shown when button is pressed. Start value is 0
uint8_t delayCounter = 0;         // Delay counter which is used when a number was rolled
bool buttonWasPressed = false;    // boolean indicating if button was pressed before. This indicates a roll
bool isFirstRoll = true;          // boolean indicating if the current roll is the first. Used for new seed value

// ==============================================================================================================
// Small inline function for better readability. This function checks if the button is currently pressed
// ==============================================================================================================
inline bool isButtonPressed() {
  return digitalRead(PIN_BUTTON_INPUT) == BUTTON_PRESSED;
}

// ==============================================================================================================
// Small function for better readability. If val is one, then the head LED will light up. Otherwise the tail
// LED will light up.
// ==============================================================================================================
inline void displayToss(const uint8_t val) {
  digitalWrite(PIN_LED_HEAD, val == 1 ? HIGH : LOW);
  digitalWrite(PIN_LED_TAIL, val != 1 ? HIGH : LOW);
}

// ==============================================================================================================
// Deactivate both LEDs.
// ==============================================================================================================
inline void clearToss(void) {
  digitalWrite(PIN_LED_HEAD, LOW);
  digitalWrite(PIN_LED_TAIL, LOW);
}

// ==============================================================================================================
// This function will be executed once at the beginning of the program execution.
// In this case only the pin, where the external push button is connected to, hast to be defined to be an
// interrupt with internal pullup.
// ==============================================================================================================
void setup() {
  // Define button pin to be an input with internal pullup
  pinMode(PIN_BUTTON_INPUT, INPUT_PULLUP);

  // Define LED pins to be outputs
  pinMode(PIN_LED_HEAD, OUTPUT);
  pinMode(PIN_LED_TAIL, OUTPUT);
}

// ==============================================================================================================
// This function will be repeated (forever) after execution of the setup function.
// The status of the button is checked. If pressed, a shuffle effect will be displayed. If released the
// rolled value will be displayed for a certain time.
// ==============================================================================================================
void loop() {
  if(isButtonPressed()) {
    // ----------------------------------------------------------------------------------------------------------
    // Button PRESSED -> shuffle effect displayed
    // ----------------------------------------------------------------------------------------------------------
    
    // Reset values
    delayCounter = 0;
    buttonWasPressed = true;

    // Display shuffle value
    displayToss(shuffleEffectValue);

    // Update shuffle effect value
    shuffleEffectValue = (shuffleEffectValue == 0) ? 1 : 0;
    
  } else {
    // ----------------------------------------------------------------------------------------------------------
    // Button not pressed. We need to work on several cases here.
    // If the button was already pressed, then we need to roll a value and display it.
    // ----------------------------------------------------------------------------------------------------------

    // If it is the first roll, then we need to save a seed for the next startup
    if(isFirstRoll && buttonWasPressed) {
      isFirstRoll = false;
      eepromHandler.save((uint64_t)millis(), 0);
    }

    // Reset shuffle effect
    shuffleEffectValue = 0;

    if(delayCounter == (ROLL_DISPLAY_TIME / DELAY_CYCLE)) {
      delayCounter = 0;
      clearToss();
    } else {
      if(buttonWasPressed) {
        // Show random number
        displayToss(randomGenerator.getRandom());
        buttonWasPressed = false;
      }
      delayCounter++;
    }
  }

  // Wait some time
  delay(DELAY_CYCLE);
}

