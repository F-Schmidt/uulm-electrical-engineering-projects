# uulm-attiny-coin-toss

![Coin Toss](./pcb-design/ATtiny_Coin_Toss_Top.png)

This repository implements a simple coin toss using pseudo random values.

Falko Schmidt, Ulm University
