# uulm-electrical-engineering-projects
This repository contains several electrical projects developed as part of the "WissenSchaffer" of Ulm University.

(c) Falko Schmidt, 2021 - 2023

# Project overview

<table>
<thead>
	<th>Development done?</th>
	<th>Image</th>
	<th>Description</th>
	<th>Further information</th>
</thead>
<tbody>
<tr>
	<td>✅</td>
	<td><img width="800" src="./uulm-tic-tac-toe/pcb-design/Tic_Tac_Toe_Top.png" alt="Tic Tac Toe"></td>
	<td>The typical Tic Tac Toe game, but with electronics.</td>
	<td></td>
</tr>
<tr>
	<td>❌</td>
	<td><img width="800" src="./uulm-snake/pcb-design/Snake.png" alt="Snake"></td>
	<td>A snake game. The playing field is a 8x8 LED matrix. Sadly this project could not be implemented, because the component to control the matrix (HT16K33) is hardly available and other soltions were to expensive.</td>
	<td>Development not done, because the component HT16K33 from Holtek for Matrix control could not be sourced. Alternatives were way too expensive, so this project did not make it =(</td>
</tr>
<tr>
	<td>✅</td>
	<td><img width="800" src="./uulm-dice/pcb-design/ATtiny_Dice_V1_TopView.png" alt="Dice"></td>
	<td>A pseudo-random dice based on th ATtinyX5.</td>
	<td></td>
</td>
</tr>
<tr>
	<td>✅</td>
	<td><img width="800" src="./uulm-coin-toss/pcb-design/ATtiny_Coin_Toss_Top.png" alt="Coin Toss"></td>
	<td>A pseudo-random coin toss based on th ATtinyX5.</td>
	<td></td>
</td>
</tr>
<tr>
	<td>❌</td>
	<td><img width="800" src="./ulmuino/pcb-design/ULMuino.png" alt="Ulmuino"></td>
	<td>A custom version of the well-known Arduino UNO R3.</td>
	<td>The development remains incomplete due to unavailability of components during the development phase. Additionally, the cost analysis revealed that the expenses associated with this project, compared to original Arduinos, were only marginally lower, making it less economically viable.</td>
</td>
</tr>
<tr>
	<td>❌</td>
	<td><img width="800" src="./uulm-pcb-presenter/pcb-design/PCB_Presenter.png" alt="PCB-Presenter"></td>
	<td>PCB-Presenter to show off the student projects using only one Arduino.</td>
	<td>A PCB to show off multiple projects with the use of only one Arduino. You can use an Arduino MEGA, which has 8x the storage space of an Arduino UNO. According to the three levers, the current PCB can be selected. This project was hardly used, because only few Arduino HATs were developed.</td>
</td>
</tr>
<tr>
	<td>✅</td>
	<td><img width="800" src="./uulm-weatherstation/pcb-design/UULM_Weatherstation_Top.png" alt="Arduino Weatherstation"></td>
	<td>A simple circuit to measure temperature and humidity. This PCB works with the Arduino UNO.</td>
	<td>Development done, but not yet used for students.</td>
</td>
</tr>
<tr>
	<td>✅</td>
	<td><img width="800" src="./uulm-arduino-isp/ATtiny_Programmer_Final.jpg" alt="Arduino ISP"></td>
	<td>Programmers for ATtinyX4 and ATtinyX5</td>
	<td></td>
</tr>
<tr>
	<td>✅</td>
	<td><img width="800" src="./uulm-minialu/pcb-design/MiniALU_UULM_Top.png" alt="MiniALU"></td>
	<td>4 bit fulladder with the ability to add and subtract.</td>
	<td></td>
</td>
</tr>
<tr>
	<td>❌</td>
	<td><img width="800" src="./uulm-simon-says/pcb-design/UULM_Simon_Says.png" alt="Simon Says"></td>
	<td>The typical Simon Says.</td>
	<td></td>
</td>
<tr>
	<td>✅</td>
	<td><img width="800" src="./uulm-compass/pcb-design/UULM_Compass_Top.png" alt="Compass"></td>
	<td>A compass showing  North using some LEDs. This project can be used to explain some maths and electrical engineering that we all use in our smartphones today.</td>
	<td>Development done, but not tested. The current PCB is not worth it - there has to be some rework done. Also, PCB is too big for what it is and you need an Arduino to read out this one sensor. All in all it is highly recommended to use a cheaper version with cheaper MCU and an overall smaller PCB.</td>
</td>
</tr>
</tbody>
</table>


## Software projects

<table>
<thead>
	<th>Development done?</th>
	<th>Name</th>
	<th>Description</th>
	<th>Further information</th>
</thead>
<tbody>
<tr>
	<td>❌</td>
	<td>Model railway controller firmware</td>
	<td>This software was implemented to control a small model train to show the use case of Petri nets</td>
	<td>Working prototype exists, but sadly the Petri net was not delivered and therefor the project was not continued. As soon as somebody gets into Petri nets (which is not THAT difficult), this project can be continued and will create a nice result =)</td>
</tr>
<tr>
	<td>✅</td>
	<td>Datalogger framework</td>
	<td>This software was used in the SGUberry weather balloon 2022/23.</td>
	<td>No use anymore. Now we use Arduinos and the according library.</td>
</tr>
<tr>
	<td>✅</td>
	<td>UULM Hello</td>
	<td>Small software framework to present objects and classes in C++.</td>
	<td>No use anymore. It was a nice homework in Seminar NWT 2022/23.</td>
</tr>
</tbody>
</table>















